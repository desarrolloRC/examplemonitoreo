export interface SessionInterface {
  date: string;
  ip: any;
  person_id: number;
  system_id: number;
  url: string;
}
