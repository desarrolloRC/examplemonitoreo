import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {NavigationEnd, Router} from '@angular/router';
import {Angulartics2GoogleGlobalSiteTag} from 'angulartics2/gst';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

import {IpHelper} from './helpers/IpHelper';
import {JwtHelper} from './helpers/JwtHelper';
import {FirebaseHelper} from './helpers/FirebaseHelper';
import {SessionService} from './services/session.service';
import {SessionInterface} from './interfaces/SessionInterface';
import {LoginService} from './modules/system/services/login.service';
import {StorageService} from './modules/shared/services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [SessionService]
})
export class AppComponent implements OnInit {
  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;

  constructor(
    private router: Router,
    private _location: Router,
    private session: SessionService,
    private db: AngularFireDatabase,
    private _loginService: LoginService,
    private translate: TranslateService,
    private storageService: StorageService,
    private angulartics: Angulartics2GoogleGlobalSiteTag,
  ) {
    this.starAnalyticTracking();
    this.starNotifications();
  }

  ngOnInit(): void {
  }

  private starAnalyticTracking() {
    this.angulartics.startTracking();

    this.router.events
      .subscribe(async (event) => {
        if (event instanceof NavigationEnd) {
          if (event['url'] !== '/') {
            await this.saveSession(event['url']);
            this.angulartics.pageTrack(event['url']);
          }
        }
      });
  }

  private starNotifications() {
    // const databaseUrl = FirebaseHelper.getDatabase();
    /*
    if (typeof databaseUrl !== 'undefined' && databaseUrl !== null) {
      this.itemsRef = this.db.list(databaseUrl);
      this.itemsRef.snapshotChanges().pipe(
        map(changes =>
          changes.map(c => ({key: c.payload.key, ...c.payload.val()}))
        )
      ).subscribe(notifications => {
        this.setNotifications(notifications);
      });
    }*/
  }

  private setNotifications(notifications) {
    const sorted = notifications.sort((a, b) => (new Date(a.date).getTime() < new Date(b.date).getTime()) ? 1 : -1);
    // this.storageService.setItem('notifications', JSON.stringify(sorted));
  }

  private async saveSession(url: string) {
    const token = sessionStorage.getItem('token');

    if (typeof token !== 'undefined' && token !== null) {

      IpHelper.getIp().then((ip) => {
        const data: SessionInterface = {
          ip: ip,
          url: url,
          system_id: 1,
          date: new Date().toString(),
          person_id: JwtHelper.getPersonId(token),
        };

        this.session.saveSession(data).subscribe();
      });
    }
  }

}
