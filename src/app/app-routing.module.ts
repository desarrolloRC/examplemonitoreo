import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AuthGuard} from './guards/auth.guard';
import {PageOfflineComponent} from './components/page-offline/page-offline.component';
import {SystemGuard} from './modules/system/guards/system.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: './modules/system/system.module#SystemModule',
    canActivate: [SystemGuard]
  },
  {
    path: 'login',
    loadChildren: './modules/system/system.module#SystemModule',
    canActivate: [SystemGuard]
  },
  {
    path: 'home',
    loadChildren: './modules/shared/shared.module#SharedModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'driver',
    loadChildren: './modules/operation/operation.module#OperationModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'system',
    loadChildren: './modules/system/system.module#SystemModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'risk',
    loadChildren: './modules/risk/risk.module#RiskModule',
  },
  {
    path: 'offline', component: PageOfflineComponent
  },
  {
    path: '**',
    loadChildren: './modules/system/system.module#SystemModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
