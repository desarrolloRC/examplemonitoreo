import * as moment from 'moment';
import * as moment_timezone from 'moment-timezone';

export class TimeHelper {

  public static getNowTime() {
    const now = moment();
    const zone = moment_timezone.tz.guess();
    return moment_timezone.tz(now, zone).format();
  }

  public static getZoneTime(date1: Date) {
    const zone = moment_timezone.tz.guess();
    return moment_timezone.tz(date1, zone).format();
  }
}
