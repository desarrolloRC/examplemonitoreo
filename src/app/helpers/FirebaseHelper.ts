import {JwtHelper} from './JwtHelper';

export class FirebaseHelper {
  public static getDatabase() {
    let database;
    const token = sessionStorage.getItem('token');

    if (token !== null && typeof token !== 'undefined') {
      const enterprise = JwtHelper.getEnterprise(token);
      database = `/notifications/enterprise_${enterprise}`;
    }

    return database;
  }

  public static getUserDatabase(personId: number) {
    let database;
    const token = sessionStorage.getItem('token');

    if (token !== null && typeof token !== 'undefined') {
      const decode = JwtHelper.decodeToken(token);
      database = `/notifications/enterprise_${decode['enterprise']}/person_${personId}`;
    }

    return database;
  }
}
