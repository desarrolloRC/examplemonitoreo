import {TranslateService} from '@ngx-translate/core';

import {I18nHelper} from './I18nHelper';
import {environment} from '../../environments/environment';
import {StorageService} from '../modules/shared/services/storage.service';

export class CharGroupHelper {

  public static getCharGroups(translate: TranslateService, storageService: StorageService) {
    return [
      {
        name: 'Segmentación de deudores',
        permission: 'behaviour-charts',
        charts: [
          {
            value: 'payment_status',
            viewValue: '1.1 Estado de la deuda en días',
            title: 'Distribución de la deuda',
            desc: `<br/>A continucación se muestra como se segmentan los deudores en termino de días. Cada segmento tiene:
            <br/>
            - <strong>Cantidad de deudores</strong><br/>
            - <strong class="alert-warning">Deuda anterior</strong><br/>
            - <strong class="alert-danger">A pagar a fin de mes</strong><br/>
            - <strong class="alert-info">Estimación recomendada</strong><br/>
            `,
            type: 'PieChart',
            endPoint: `${environment.urlScore}segmentation-chart`,
            compareMoney: false,
            category: 'Categoria',
            permission: 'ticket-status-chart',
            table: true
          },
          {
            value: 'payment_status_line',
            viewValue: '1.2 Comportamiento de la deuda',
            title: 'Comportamiento de la deuda',
            desc: `<br/>A continuación se muestra el comportamiento de la deuda en el periodo de tiempo seleccionado
            `,
            type: 'AreaChart',
            endPoint: `${environment.urlScore}segmentation-chart-line`,
            compareMoney: false,
            category: 'Estado',
            permission: 'ticket-status-chart'
          },
          {
            value: 'payment_status_avg',
            viewValue: '1.3 Comportamiento de la deuda montos',
            title: 'Comportamiento de la deuda',
            desc: `<br/>A continuación se muestra el comportamiento de la deuda en el periodo de tiempo seleccionado
            `,
            type: 'LineChart',
            endPoint: `${environment.urlScore}segmentation-chart-line-avg`,
            compareMoney: false,
            category: 'Estado',
            permission: 'ticket-status-chart'
          },
        ]
      },
      {
        name: '¿Cómo se comporta?',
        permission: 'behaviour-charts',
        charts: [
          {
            value: 'tickets_status',
            viewValue: I18nHelper.getTranslate(translate, storageService, 'char.ticket_status_lima'),
            title: I18nHelper.getTranslate(translate, storageService, 'char.ticket_status'),
            desc: I18nHelper.getTranslate(translate, storageService, 'chart.ticket_status'),
            type: 'PieChart',
            endPoint: `${environment.urlRisk}get-data-chart-ticket-status`,
            compareMoney: false,
            category: 'Estado',
            permission: 'ticket-status-chart',
          },
          {
            value: 'tickets_severity',
            viewValue: I18nHelper.getTranslate(translate, storageService, 'char.ticket_severity'),
            title: I18nHelper.getTranslate(translate, storageService, 'char.ticket_severity'),
            desc: I18nHelper.getTranslate(translate, storageService, 'char.ticket_severity_desc'),
            type: 'PieChart',
            endPoint: `${environment.urlRisk}get-data-chart-ticket-severity`,
            compareMoney: true,
            category: 'Gravedad',
            permission: 'ticket-severity-chart',
          },
          {
            value: 'tickets_discount',
            viewValue: I18nHelper.getTranslate(translate, storageService, 'char.ticket_whit_discount'),
            title: I18nHelper.getTranslate(translate, storageService, 'char.ticket_whit_discount_title'),
            desc: I18nHelper.getTranslate(translate, storageService, 'char.ticket_whit_discount_desc'),
            type: 'BarChart',
            endPoint: `${environment.urlRisk}get-data-tickets-discount`,
            compareMoney: false,
            category: 'Descuento',
            permission: 'ticket-discount-chart',
          }
        ]
      },
      {
        name: 'Segmentación de deudores',
        permission: 'behaviour-charts',
        charts: [
          {
            value: 'payment_status',
            viewValue: '1.1 Estado de la deuda en días',
            title: 'Distribución de la deuda',
            desc: `A continucación se muestra como se segmentan los deudores en termino de días. Cada segmento tiene:
            <br/>
            - <strong>Cantidad de deudores</strong><br/>
            - <strong class="alert-warning">Deuda anterior</strong><br/>
            - <strong class="alert-danger">A pagar a fin de mes</strong><br/>
            `,
            type: 'PieChart',
            endPoint: `${environment.urlScore}segmentation-chart`,
            compareMoney: false,
            category: 'estado',
            permission: 'payment-status'
          },
        ]
      },
      {
        name: '¿Cómo es su productividad?',
        permission: 'productivity-charts',
        charts: [
          {
            value: 'license_class',
            viewValue: '2.1 Clase de licencias.',
            title: 'Clase de licencias',
            desc: `Solo aquellas licencias de clase AII son las<br>
          permitidas para prestar servicios de Taxi<br><br>
          + {{VALUE}}% tiene una clase de licencia diferente a AII.`,
            type: 'PieChart',
            endPoint: `${environment.urlRisk}get-documents-driver-behavior`,
            compareMoney: false,
            category: 'Clase',
            permission: 'license-class-chart',
          },
          {
            value: 'license_all_situation',
            viewValue: '2.2 Situación de licencias clase AII.',
            title: 'Situación de licencias clase All',
            desc: `Solo aquellas licencias de Clase AII son las<br>
          permitidas para prestar servicios de Taxi<br><br>
          + Cerca al {{VALUE}}% de los conductores se encuentra<br>
          con la licencia suspendida o vencida.<br>
          + Se recomienda contactar a los conductores para<br>
          explicarles el procedimiento de cambio de clase.`,
            type: 'BarChart',
            endPoint: `${environment.urlRisk}get-data-license-all`,
            compareMoney: false,
            category: 'Observacion',
            permission: 'license-aii-chart',
          },
          {
            value: 'driver_license_points',
            viewValue: '2.3 Punto de licencias de conductores.',
            title: 'Puntos de licencia de conductores',
            desc: `2.2% de los conductores cuentan con más de <br>
          100 puntos en su licencia.<br><br>
          + {{VALUE}}% de conductores se encuentran con más de 40 puntos<br>
          en su licencia, ante cualquier falta Grave sobreparan los 100.<br>
          + Contar con mas de 100 puntos de Licencia implica<br>
          retención vehicular por parte de las autoridades. .`,
            type: 'PieChart',
            endPoint: `${environment.urlScore}get-chart-license-points`,
            compareMoney: false,
            category: 'Puntaje',
            permission: 'driver-license-chart',
          }
        ]
      },
      {
        name: '¿Cómo cuida su vehículo?',
        permission: 'vehicle-charts',
        charts: [
          {
            value: 'capture_orders',
            viewValue: '3.1 Vehiculos con orden de captura.',
            title: 'Vehiculos con orden de captura',
            desc: `
                  + El {{VALUE}}% de los vehículos se encuentra con Orden de Captura
                  `,
            type: 'PieChart',
            endPoint: `${environment.urlDriver}get-capture-orders`,
            compareMoney: false,
            category: 'Estado',
            permission: 'capture-orders-chart',
          },
          {
            value: 'reasons_capture_orders',
            viewValue: '3.2 Distribución de los Motivos de Orden de Captura.',
            title: 'Distribución de los Motivos de Orden de Captura',
            desc: I18nHelper.getTranslate(translate, storageService, 'char.reasons_capture_orders'),
            type: 'BarChart',
            endPoint: `${environment.urlDriver}get-capture-order-reasons`,
            compareMoney: false,
            category: 'Concepto',
            permission: 'reasons-capture-orders-chart',
          },
          {
            value: 'gps',
            viewValue: '3.3 GPS.',
            title: 'GPS',
            desc: `
          Vehículos que presentan AL MENOS UN DÍA de Sobreoperación,<br>
          Infra-operación o No reportan conexión al satélite<br>
          de GPS durante el presente mes:<br><br>
          + 4 vehículos no han reportado conexión al satélite de GPS<br>
          en al menos 10 dÍas.<br>
          + 43 vehículos han infra-operando el 70% de los días del<br>
          presente mes. Esto significa que muy posiblemente no<br>
          hayan producido lo suficiente y sean incapaces de pagar la<br>
          cuota
          `,
            type: 'BarChart',
            endPoint: `${environment.urlScore}get-chart-gps-operation`,
            compareMoney: false,
            category: 'Categoria',
            permission: 'gps-chart',
          },
          {
            value: 'certificate_validity',
            viewValue: '3.4 Vigencia del certificado.',
            title: 'Vigencia del Certificado',
            desc: `
          7.4% tienen la Inspección Vehicular vencida y 3.1%<br>
          tienen menos de 30 días para pasar por una nueva<br>
          inspección.<br><br>
          + Inspecciones vencidas {{VALUE}}%<br>
          + La carencia de una Inspección vehicular vigente y<br>
          aprobada implica una multa MUY GRAVE M27 de s/. 2100.
          `,
            type: 'PieChart',
            endPoint: `${environment.urlRisk}get-data-certificate-validity`,
            compareMoney: false,
            category: 'Estado',
            permission: 'certificate-validity-chart',
          },
          {
            value: 'check_approved_observation',
            viewValue: '3.5 Observaciones de las revisiones aprobadas.',
            title: 'Observaciones en las revisiones aprobadas',
            desc: `{{VALUE}}% de las revisiones técnicas aprobadas,<br>
          cuentan con observaciones.`,
            type: 'PieChart',
            endPoint: `${environment.urlRisk}get-data-certificate-observations`,
            compareMoney: false,
            category: 'Estado',
            permission: 'check-approved-observation-chart',
          },
          {
            value: 'soat_use',
            viewValue: '3.6 Uso del SOAT.',
            title: 'Uso del SOAT',
            desc: I18nHelper.getTranslate(translate, storageService, 'char.soat_text'),
            type: 'PieChart',
            endPoint: `${environment.urlRisk}get-documents-use-soat`,
            compareMoney: false,
            category: 'Uso',
            permission: 'soat-use-chart',
          },
          {
            value: 'soat_validity',
            viewValue: '3.7 Vencimiento de los SOAT tipo taxi.',
            title: 'Vencimiento de los SOAT tipo Taxi',
            desc: I18nHelper.getTranslate(translate, storageService, 'chat.soat_taxi_text'),
            type: 'BarChart',
            endPoint: `${environment.urlRisk}get-documents-expired-soat`,
            compareMoney: false,
            category: 'Estado',
            permission: 'soat-validity-chart',
          },
          {
            value: 'circulation_card_afiliation',
            viewValue: '3.8 Afiliación para la tarjeta de circulación.',
            title: 'Afiliación para la tarjeta de circulación',
            desc: `
          + Se desconoce el tipo de entidad afiliada del {{VALUE}}%<br>
          de los vehículos (SETAME, SECATA o ninguno)<br>
          + El no contar con una Autorización Municipal vigente<br>
          implica que el vehículo no puede prestar el servicio de Taxi.
          `,
            type: 'PieChart',
            endPoint: `${environment.urlRisk}get-documents-circulation-card-affiliation`,
            compareMoney: false,
            category: 'Estado',
            permission: 'circulation-card-afiliation-chart',
          },
          {
            value: 'circulation_card_status',
            viewValue: '3.9 Estado de la tarjeta de circulación.',
            title: 'Estado de la tarjeta de circulación',
            desc: `
          Del total de vehículos en SETAME con Tarjeta de<br>
          Circulación, el {{VALUE}}% NO cuenta con una Tarjeta activa<br><br>
          + La mayoría de los vehículos dados de baja han perdido su<br>
          contrato con la empresa de transportes que lo acogía.
          `,
            type: 'BarChart',
            endPoint: `${environment.urlRisk}get-documents-circulation-card-status`,
            compareMoney: false,
            category: 'Estado',
            permission: 'circulation-card-status-chart',
          },
          {
            value: 'removal_motives',
            viewValue: '3.10 Motivos de baja.',
            title: 'Motivos de baja',
            desc: `
          + La mayoría de los vehículos dados de baja han<br>
          perdido su contrato con la empresa de transportes<br>
          que lo acogía.
          `,
            type: 'BarChart',
            endPoint: `${environment.urlRisk}get-data-circulation-low`,
            compareMoney: false,
            category: 'Motivo',
            permission: 'removal-motives-chart',
          },
          {
            value: 'gas_certificate_validity',
            viewValue: '3.11 Vigencia del certificado de gas.',
            title: 'Vigencia del certificado de Gas',
            desc: `
          {{VALUE}}% tienen un certificado de Gas vencido y 5% cuentan<br>
          con la inspección al Cilindro de Gas expirado.<br><br>
          + Se recomienda contactar a los conductores para explicar<br>
          los riesgos de no contar con una Inspección Técnica vigente<br>
          + El prestar el servicio de taxi con vehículos convertidos a<br>
          Gas Natural Vehicular (GNV) que no cuenten con el<br>
          certificado de conformidad de conversión a GNV o el<br>
          certificado de inspección anual o quinquenal del vehículo<br>
          a GNV vigente implica una multa R14, Grave de S./210.
          `,
            type: 'BarChart',
            endPoint: `${environment.urlRisk}get-data-gas-certificate-validity`,
            compareMoney: false,
            category: 'Estado',
            permission: 'gas-certificate-validity-chart',
          },
          {
            value: 'gas_tank_certificate_validity',
            viewValue: '3.12 Vigencia del certificado del Cilindro de Gas.',
            title: 'Vigencia del certificado del Cilindro de Gas',
            desc: `
          3% Certificados de Gas y 1% Inspecciones al Cilindro<br>
          de Gas estan a menos de 30 dias de su vencimiento.
          `,
            type: 'BarChart',
            endPoint: `${environment.urlRisk}get-data-gas-tank-certificate-validity`,
            compareMoney: false,
            category: 'Estado',
            permission: 'gast-tank-certificate-validity-chart',
          },
          {
            value: 'authorization_afiliation',
            viewValue: '3.13 Afiliación para la autorización.',
            title: 'Afiliación para la autorización',
            desc: `
          Solo {{VALUE}}% de vehículos han podido ser identificados con una<br>
          Autorización Municipal del SETAME, el 96.6% restante podría<br>
          tener una Autorización de SETACA o no contar con ninguna<br>
          autorización.<br><br>
          + Se recomienda contactar a los conductores de los cuales<br>
          se desconoce su situación.<br>
          + El no contar con una Autorización Municipal vigente implica que el<br>
          vehículo no puede prestar el servicio de Taxi y sería llevado al depósito de<br>
          encontrarse trabajando.
          `,
            type: 'PieChart',
            endPoint: `${environment.urlRisk}get-data-authorization-validation-setame`,
            compareMoney: false,
            category: 'Estado',
            permission: 'authorization-afiliation-chart',
          },
          {
            value: 'setame_validity',
            viewValue: '3.14 Vigencia de la afiliación del SETAME.',
            title: 'Vigencia de la afiliación del SETAME',
            desc: `
          Del total de vehículos que se encuentran en SETAME con<br>
          Autorización municipal, el {{VALUE}}% esta con la Autrización vencida.
          `,
            type: 'BarChart',
            endPoint: `${environment.urlRisk}get-data-authorization-validation-validity`,
            compareMoney: false,
            category: 'Estado',
            permission: 'setame-validity-chart',
          }
        ]
      }
    ];
  }
}
