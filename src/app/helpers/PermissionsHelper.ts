import {Injectable} from '@angular/core';

import {JwtHelper} from './JwtHelper';

@Injectable()
export class PermissionsHelper {
  can(permission: string): boolean {
    const token = localStorage.getItem('token');

    if (token !== null && typeof token !== 'undefined') {
      return JwtHelper.getPermissions(token).includes(permission);
    } else {
      return false;
    }
  }
}
