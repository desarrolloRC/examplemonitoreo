import {TranslateService} from '@ngx-translate/core';

import {JwtHelper} from './JwtHelper';
import {StorageService} from '../modules/shared/services/storage.service';

export class I18nHelper {

  public static setUp(translate: TranslateService, storageService: StorageService) {
    const token = sessionStorage.getItem('token');
    let setLanguage = 'es-co';

    if (token) {
      setLanguage = JwtHelper.getCountryCode(token);
      setLanguage = this.setLanguageCode(setLanguage);
    }

    translate.addLangs(['es-co', 'es-mex', 'es-pe']);
    storageService.setItem('language', setLanguage);
    translate.setDefaultLang(setLanguage);
    translate.use(setLanguage);
  }

  private static setLanguageCode(countryCode): string {
    let response = 'es-co';

    switch (countryCode) {
      case 'MX-02':
        response = 'es-mex';
        break;
      case 'PE-03':
        response = 'es-pe';
        break;
    }

    return response;
  }

  public static getTranslate(translate: TranslateService, storageService: StorageService, key: string, value?: string): string {
    let response;
    translate.get(key, {value: value}).subscribe((res: string) => {
      response = res;
    });
    return response;
  }
}
