import * as jwtDecode from 'jwt-decode';

export class JwtHelper {
  public static verifyExpiration(token: string) {
    const decode = jwtDecode(token);
    const date = Math.floor(Date.now() / 1000);

    return decode['exp'] > date;
  }

  public static getPermissions(token: string): Array<any> {
    return jwtDecode(token)['permissions'];
  }

  public static getEnterprise(token: string) {
    return jwtDecode(token)['enterprise'];
  }

  public static getImgUrl(token: string) {
    return jwtDecode(token)['img_url'];
  }

  public static getPersonId(token: string) {
    return jwtDecode(token)['person'];
  }

  public static decodeToken(token: string) {
    return jwtDecode(token);
  }

  public static getCurrency(token: string) {
    return jwtDecode(token)['currency'][0];
  }

  public static getCountryCode(token: string): string {
    return jwtDecode(token)['currency'][0]['code'];
  }
}
