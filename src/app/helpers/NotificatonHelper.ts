import Swal from 'sweetalert2';

export class NotificatonHelper {
  public static errorMessage(message: string) {
    return Swal({
      title: 'Error',
      type: 'error',
      text: message,
      confirmButtonText: 'ok',
    }).then();
  }

  public static recoveryPasswordMessage(message: string) {
    return Swal({
      title: 'Password is changed!',
      type: 'success',
      text: message,
      confirmButtonText: 'ok',
    });
  }
}
