export class TitleHelper {
  public static setTite(name: string) {
    localStorage.setItem('title', name);
  }

  public static getTitle() {
    return localStorage.getItem('title');
  }
}
