import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent, HttpHeaders,
} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import * as JwtDecode from 'jwt-decode';

import {NotificatonHelper} from '../helpers/NotificatonHelper';
import {LoaderService} from '../modules/shared/services/loader.service';
import { JwtHelper } from '../helpers/JwtHelper';
import { LoginService } from '../modules/system/services/login.service';
import {Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class Interceptor implements HttpInterceptor {
  private header = new HttpHeaders();
  private headers;

  constructor(
    private _location: Router,
    private loaderService: LoaderService,
    private _loginService: LoginService) {
    if (sessionStorage.getItem('token') !== null && typeof sessionStorage.getItem('token') !== 'undefined') {
      this.headers = this.header.append('Authorization', 'Bearer ' + sessionStorage.getItem('token'));
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (sessionStorage.getItem('token') !== null && typeof sessionStorage.getItem('token') !== 'undefined') {
      this.headers = this.header.append('Authorization', 'Bearer ' + sessionStorage.getItem('token'));
    }
    
    this.showLoader();
    const request = req.clone({headers: this.headers});

    return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
        this.onEnd();
      },
      (error) => {
        if (sessionStorage.getItem('token') !== null && typeof sessionStorage.getItem('token') !== 'undefined') {
          const token = sessionStorage.getItem('token');
          const valid = JwtHelper.verifyExpiration(token);
          if (!valid) {
            const person_id = {person_id: JwtDecode(token)['person']};
            this._loginService.regenerateToken(person_id).subscribe((response: any) => {
              try {
                sessionStorage.setItem('token', response.token);
                this._location.navigate(['/']).then();
                location.reload();
                return true;
              } catch {
                return false;
              }
            });
          } else {
            this.onEnd();
            if (error.status >= 400 && error.status < 500) {
              NotificatonHelper.errorMessage(error.error.message).then();
            }
    
            console.log(error);
          }
        }
      }));

  }

  private onEnd(): void {
    this.hideLoader();
  }
  private showLoader(): void {
    this.loaderService.show();
  }
  private hideLoader(): void {
    this.loaderService.hide();
  }
}
