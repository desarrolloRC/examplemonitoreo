import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

import * as uuid from 'uuid';
import {MainService} from '../../services/main.service';
import {FirebaseHelper} from '../../../../helpers/FirebaseHelper';
import {AlertComponent} from '../alert/alert.component';

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.css']
})
export class CallComponent implements OnInit {
  public contractId;
  public unpaidDays;
  public submitted = false;
  public eventForm: FormGroup;
  itemsRef: AngularFireList<any>;
  public alertModal: MatDialogRef<AlertComponent>;
  public gData = {driverInfo: 0, Name: '', Phone: '', Email: '', LastName: ''};

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialog,
              private callService: MainService,
              private formBuilder: FormBuilder,
              public snackBar: MatSnackBar,
              public dialogRef: MatDialogRef<CallComponent>,
              private driverServices: MainService,
              private db: AngularFireDatabase) {
    this.contractId = data.ContractId;
  }

  ngOnInit() {
    this.eventForm = this.formBuilder.group({
      notificationText: [null]
    });
  }

  public  async  alert() {
    const param = {
      minWidth: '382px',
      height: '186px',
      data: {text: 'La alerta se ha resuelto.'}
    };
    this.alertModal = this.dialog.open(AlertComponent, param);
  }

  public saveNotificationCall() {
    this.submitted = true;

    if (this.eventForm.valid) {
      const params = this.eventForm.value;

      this.callService.newEvent(params).subscribe((response) => {
        /*
        if (Object.keys(response['data']).length > 0) {
          if (this.gData.driverInfo > 0) {
            const databaseUrl = FirebaseHelper.getUserDatabase(this.gData.driverInfo);
            const paramsFire = {
              date: (new Date()).toString(),
              type: 'accident',
              text: this.eventForm.value
            }
            if (typeof databaseUrl !== 'undefined' && databaseUrl !== null) {
              this.itemsRef = this.db.list(databaseUrl);
              this.itemsRef.set(uuid(), paramsFire);
            }
          }

          this.sendSnack('Evento guardado');
          setTimeout(() => {
            this.dialogRef.close();
          }, 1500);
        }*/
      });

    } else {
      this.sendSnack('Llene los campos requeridos');
    }

    this.submitted = false;
  }

  sendSnack(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

}
