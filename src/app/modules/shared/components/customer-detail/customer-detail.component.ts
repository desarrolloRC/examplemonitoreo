import {Component, Input, Inject, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import {MainService} from '../../services/main.service';
import {environment} from '../../../../../environments/environment';
import {DriverCommonStructure} from '../../../operation/definitions/DriverCommonStructure';
import {TimeHelper} from '../../../../helpers/TimeHelper';
import {FirebaseHelper} from '../../../../helpers/FirebaseHelper';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {map} from 'rxjs/operators';
import {ScoreService} from '../../services/score.service';
import {TranslateService} from '@ngx-translate/core';
import {I18nHelper} from '../../../../helpers/I18nHelper';
import {StorageService} from '../../services/storage.service';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {
  public submitted;
  public info;
  public documents = [];
  public vehicle;
  public tickets = [];
  public attributeLabels;
  public notifications = [];
  public showMaintenanceBtns = true;
  public showBasicBtns = false;
  public itemsRef: AngularFireList<any>;
  public scoreMicroService = environment.urlScore;
  public dataAlerts = [];
  public typesAvailable = [
    {name: 'ticketsPending', label: I18nHelper.getTranslate(this.translate, this.storage, 'ticket.pending'),
      title: I18nHelper.getTranslate(this.translate, this.storage, 'home.tickets_pending')},
    {name: 'paymentFrecuency', label: 'Frecuencia de pago', title: 'Dias promedio'},
    {name: 'paymentPercentage', label: 'Cumplimiento', title: 'Porcentaje de pago promedio'},
    {name: 'debtAmount', label: 'Deuda vencida', title: 'Monto de deuda vencida'},
    {name: 'lowKilometers', label: 'Productividad', title: 'Kilometros recorridos promedio'},
    {name: 'documentsExpired', label: 'Documentos vencidos', title: '# de documentos'},
    {name: 'documentsAboutToExpire', label: 'Documentos en riesgo', title: '# de documentos'},
    {name: 'timesBeyond', label: 'Velocidad alta', title: '# de veces por encima del limite'},
    {name: 'highKilometers', label: 'Kilometraje alto', title: 'Kilometros recorridos promedio'}
  ];

  constructor(private mainService: MainService,
              private storage: StorageService,
              private scoreService: ScoreService,
              @Inject(MAT_DIALOG_DATA) public data,
              private db: AngularFireDatabase,
              private translate: TranslateService) {
  }

  ngOnInit() {
    this.attributeLabels = DriverCommonStructure.AlertsAttributeLabels;
    this.getPersonalInfo();
  }

  async getPersonalInfo() {
    if (typeof this.data.owner_contract_id === 'undefined') {
      this.scoreService.getSummaryByPlate(this.data.plate).subscribe((response) => {
        this.data = response[0];
        const params = {
          'ownerContractId': response[0].owner_contract_id
        };
        this.getPersonalInfoByContract(params);
        this.getDocuments();
        this.getTicketsByVehicle();
        this.getVehicleInfo();
      });
    } else {
      this.getDocuments();
      this.getTicketsByVehicle();
      this.getVehicleInfo();
      const params = {
        'ownerContractId': this.data.owner_contract_id
      };
      this.getPersonalInfoByContract(params);
    }
  }

  private getPersonalInfoByContract(params) {
    /*
    this.mainService.getPersonalInfoByContract(params).subscribe((response) => {
      this.info = response[0];
      this.mainService.getAlertsByContractId(this.data.owner_contract_id).subscribe((result: any) => {
        this.dataAlerts = result;
      });
      const databaseUrl = FirebaseHelper.getUserDatabase(this.info.OwnerId);
      if (typeof databaseUrl !== 'undefined' && databaseUrl !== null) {
        this.itemsRef = this.db.list(databaseUrl);
        this.itemsRef.snapshotChanges().pipe(
          map(changes =>
            changes.map(c => ({key: c.payload.key, ...c.payload.val()}))
          )
        ).subscribe(notifications => {
          this.setNotifications(notifications);
        });
      }
    });*/
  }

  private setNotifications(notifications) {
    this.notifications = notifications.sort((a, b) => (new Date(a.date).getTime() < new Date(b.date).getTime()) ? 1 : -1);
  }


  async getDocuments() {
    const params = {
      'vehicleId': this.data.vehicle_id
    };
    this.mainService.getDocumentsByVehicle(params).subscribe((response: any) => {
      this.documents = [];
      response.map((doc) => {
        const expiration = new Date(doc['expiration_date']);
        if (expiration >= new Date()) {
          doc['expired'] = true;
        } else {
          doc['expired'] = false;
        }
        this.documents.push(doc);
      });
    });
  }

  async getTicketsByVehicle() {
    this.mainService.getTicketsByVehicle(this.data.vehicle_id).subscribe((response: Array<any>) => {
      if (response.length > 0) {
        this.tickets = response;
      }
    });
  }

  async getVehicleInfo() {
    this.mainService.getVehicleInfo(this.data.vehicle_id).subscribe((response: Array<any>) => {
      if (response.length > 0) {
        this.vehicle = response;
      }
    });
  }
}
