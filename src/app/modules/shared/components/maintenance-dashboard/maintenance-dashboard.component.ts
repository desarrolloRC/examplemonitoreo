import {NgxSpinnerService} from 'ngx-spinner';
import {Component, OnInit} from '@angular/core';

import {ParamsAndId} from '../../interfaces/SharedInterface';
import {StorageService} from '../../services/storage.service';
import {environment} from '../../../../../environments/environment';
import {TablesStructure} from '../../../risk/definitions/TablesStructure';
import {OperationDashboardService} from '../../../operation/services/operation-dashboard.service';

@Component({
  selector: 'app-maintenance-dashboard',
  templateUrl: './maintenance-dashboard.component.html',
  styleUrls: ['./maintenance-dashboard.component.css']
})
export class MaintenanceDashboardComponent implements OnInit {
  public pageIndex;
  public pageSize = 2;
  public percentage = 0;
  public percentageTitle = '0%';
  public attributeLabelsMaintenance: object[];
  public maintenanceMicroService = environment.urlMaintenance;
  public titleCard = 'de los vehículos han cumplido su mantenimiento';

  constructor(
    private operationDashboard: OperationDashboardService,
    private spinner: NgxSpinnerService,
    private storageService: StorageService) {
  }

  ngOnInit() {
    this.attributeLabelsMaintenance = TablesStructure.attributeLabelsMaintenance;

    this.getDataGauge();
    let global = this.storageService.getItem('globalSearch');
    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[2] === 'globalSearch' 
      || parts[2] === 'globalSearchStartDate' 
      || parts[2] === 'globalSearchEndDate' 
      || parts[2] === 'chartScatterHarvest'
      ) {
                this.getDataGauge();
      }
      if (parts[2] === 'globalSearch') {
        global = this.storageService.getItem('globalSearch');
        if (global !== null && global !== '') {
          this.titleCard = 'cumplimiento de la ruta de mantenimiento de ese vehículo';
        } else {
          this.titleCard = 'de los vehículos han cumplido su mantenimiento';
        }
      }
    });
    if (global !== null && global !== '') {
      this.titleCard = 'cumplimiento de la ruta de mantenimiento de ese vehículo';
    } else {
      this.titleCard = 'de los vehículos han cumplido su mantenimiento';
    }
  }

  getDataGauge() {
    const startDate = this.storageService.getItem('globalSearchStartDate');
    const endDate = this.storageService.getItem('globalSearchEndDate');
    const search = this.storageService.getItem('globalSearch');

    const params: ParamsAndId = {
      id: null,
      startDate: startDate,
      endDate: endDate,
      search: search,
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };
    this.spinner.show();
    this.operationDashboard.getPreventivesByType(params).subscribe((response: any) => {
      if (response.length > 0) {
        let on_day = 0;
        let no_day = 0;
        response.map((value) => {
          if (value.type === 'Al dia') {
            on_day = on_day + parseInt(value.count, 0);
          } else {
            no_day = no_day + parseInt(value.count, 0);
          }

          this.percentage = Math.round((on_day * 100) / (on_day + no_day));
          this.percentageTitle = this.percentage + '%';
        });
      } else {
        this.percentage = 0;
        this.percentageTitle = '0%';
      }
      this.spinner.hide();
    });
  }
}
