import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Output()
  closeDrawerEmit = new EventEmitter<string>();

  constructor(
    private _location: Router,) {

  }

  public menuList = [
    {
      title: 'Home',
      link: '/home',
      icon: 'home',
      imageLink: 'assets/icons_name/dashboard.svg',
      imageLinkOther: 'assets/icons_name/dashboard-other.svg',
    },
    {
      title: 'Pagos',
      link: 'driver/operation-dashboard',
      icon: 'home',
      imageLink: 'assets/icons_name/comportamiento_1.svg',
      imageLinkOther: 'assets/icons_name/comportamiento-other.svg',
      subItems: [
        {title: 'Recaudo', link: 'driver/collect-dashboard'},
        {title: 'Chequeo del carro', link: 'driver/check-dashboard'},
        {title: 'Preventivo', link: 'driver/maintenance-dashboard'},
      ]
    },
    {
      title: 'Vehículo',
      link: 'risk/risk-dashboard',
      icon: 'home',
      imageLink: 'assets/icons_name/riesgos.svg',
      imageLinkOther: 'assets/icons_name/riesgos-other.svg',
      subItems: [
        {title: 'Accidentes', link: 'risk/accident-dashboard'},
        {title: 'Comparendos', link: 'risk/ticket-dashboard'},
        {title: 'Enfermedad', link: 'risk/sickness-dashboard'},
        {title: 'Vacaciones', link: 'risk/holiday-dashboard'},
      ]
    },
    {
      title: 'Comportamiento',
      link: 'driver/driver-dashboard',
      icon: 'home',
      imageLink: 'assets/icons_name/conduccion.svg',
      imageLinkOther: 'assets/icons_name/conduccion-other.svg',
      subItems: [
        {title: 'Score', link: 'driver/score'},
        {title: 'Kilometros', link: 'driver/kilometers'},
        {title: 'Velocidad', link: 'driver/speed'},
        {title: 'Ubicación', link: 'driver/location'},
      ]
    },
  ];

  ngOnInit(): void {
  }

  private toggleSubMenu(item: any) {
    if (item.expand === false || typeof item.expand === 'undefined') {
      this.menuList.map((value) => {
        value['expand'] = false;
      });
    }
    item.expand = !item.expand;
  }

  logout() {
    sessionStorage.clear();
    this._location.navigate(['']).then();
  }

  closeDrawer(location) {
    this.closeDrawerEmit.emit('close');
    this._location.navigate([location]).then();
  }

}
