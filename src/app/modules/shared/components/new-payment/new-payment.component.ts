import { Component, OnInit } from '@angular/core';
import { DriverService } from '../../services/driver.service';
import { DashboardService } from '../../services/dashboard.service';
import { PermissionsService } from 'src/app/modules/system/services/permissions.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';
import * as PromiseBluebird from 'bluebird';

import * as moment from 'moment';
import * as moment_timezone from 'moment-timezone';
@Component({
  selector: 'app-new-payment',
  templateUrl: './new-payment.component.html',
  styleUrls: ['./new-payment.component.css']
})
export class NewPaymentComponent implements OnInit {
  public total = 0;
  public current = 0;
  public percentage = 0;
  public showProgress = false;
  public paymentHistory = [];
  public interest;

  constructor(
    private _DrivingService: DriverService,
    private _dashboardService: DashboardService,
    public _PermissionService: PermissionsService,
    private spinner: NgxSpinnerService,
    private _location: Router
  ) { }

  data: any;
  dataFinal: object[] = [];
  result: object[] = [];

  ngOnInit() {
    this.getPaymentHistory();
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <any>(XLSX.utils.sheet_to_json(ws, {header: 1}));
      this.proccessFile();

    };
    reader.readAsBinaryString(target.files[0]);
  }

  proccessFile () {
    if (this.data.length <= 1500) {
      this.data.map((payment) => {
        if (typeof payment[2] === 'number') {
          this.dataFinal.push({
            location_code: payment[0],
            location_name: payment[1],
            amount: payment[2],
            transaction_date: this.ExcelDateToJSDate(payment[3]),
            voucher: payment[4],
            contract_number: payment[5],
            currency_code: payment[6],
            register_date: this.ExcelDateToJSDate(payment[7]),
            origin: payment[8],
            interest: payment[9],
            capital: payment[10],
          });
        }
      });
    } else {
      alert('El sistema solo permite cargar hasta 1500 registros a la vez')
    }

  }


  DateToJSDate(dateSent) {
    const date = moment(dateSent,['DD/MM/YYYY', 'YYYY-MM-DD', 'MM-DD-YYYY'],false);
    if (date.isValid()) {
      return date.format('YYYY-MM-DD');
    } else {
      return null;
    }
  }

  ExcelDateToJSDate(serial) {
    if (typeof serial === 'number') {
      const date = new Date((serial - (25567 + 1))*86400*1000);
      return `${date.getFullYear()}-${(date.getMonth() + 1)}-${date.getDate()}`
    } else {
      return this.DateToJSDate(serial);
    }
  }

  async getPaymentHistory() {
    await this._dashboardService.getPaymentHistory().subscribe((response: any) => {
      this.paymentHistory = response;
    });
  }

  async savePayment() {
    
    this.spinner.show();
    
    this.result = [];
    this.current = 0;
    this.total = this.dataFinal.length;
    this.percentage = 0;
    const params = {
      data: this.dataFinal
    };
    this.spinner.hide();
    if (typeof this.interest === 'undefined' || this.interest === false) {
      this._dashboardService.savePaymentCsv(params).subscribe((response: any) => {
        this.result = response['data'];
      });
    } else {
      this._dashboardService.savePaymentCsvInterest(params).subscribe((response: any) => {
        this.result = response['data'];
      });
    }
    alert('Archivo cargado para procesamiento, al terminar aparecera en el historial');
    window.location.reload();
    
   }

}
