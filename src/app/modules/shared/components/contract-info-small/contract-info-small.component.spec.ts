import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractInfoSmallComponent } from './contract-info-small.component';

describe('ContractInfoSmallComponent', () => {
  let component: ContractInfoSmallComponent;
  let fixture: ComponentFixture<ContractInfoSmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractInfoSmallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractInfoSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
