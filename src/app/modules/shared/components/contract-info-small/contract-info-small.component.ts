import {Component, Input, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-contract-info-small',
  templateUrl: './contract-info-small.component.html',
  styleUrls: ['./contract-info-small.component.css']
})
export class ContractInfoSmallComponent implements OnInit {

  constructor(
    private _MainService: MainService,
    @Inject(MAT_DIALOG_DATA) public contract,
  ) { }

  public contractInfo = {
    Number: ''
  };
  public userInfo = {
    Dni: '',
    Name: '',
    LastName: ''
  };

  ngOnInit() {
    this.getInfoContract();
  }
  
  getInfoContract() {
    this._MainService.getContractInfo(this.contract).subscribe((responseContract: any) => {
      this._MainService.getPersonInfoById(responseContract['OwnerId']).subscribe((responsePerson: any) => {
        this.contractInfo.Number = responseContract.Number;
        this.userInfo.Dni = responsePerson.Dni;
        this.userInfo.Name = responsePerson.Name;
        this.userInfo.LastName = responsePerson.LastName;
      });
    });
  }

}
