import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DriverCommonStructure} from '../../../operation/definitions/DriverCommonStructure';
import { PermissionsService } from 'src/app/modules/system/services/permissions.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-payment-home',
  templateUrl: './payment-home.component.html',
  styleUrls: ['./payment-home.component.css']
})
export class PaymentHomeComponent implements OnInit {
  public scoreMicroService = environment.urlScore;
  public attributeLabels;

  constructor(
    public _PermissionService: PermissionsService,
    private _location: Router,) {
  }

  ngOnInit() {
    this.attributeLabels = DriverCommonStructure.PaymentHomeAttributeLabels;
    if (!this._PermissionService.canAccess('menu_payment')) {
      this._location.navigate(['/']).then();
    }
  }

}
