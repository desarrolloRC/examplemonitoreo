import { Component, OnInit } from '@angular/core';
import {DashboardService} from '../../services/dashboard.service';
import {SearchGlobalParams} from '../../interfaces/SharedInterface';
import {OperationDashboardService} from '../../../operation/services/operation-dashboard.service';
import {StorageService} from '../../services/storage.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-average-collect-dashboard',
  templateUrl: './average-collect-dashboard.component.html',
  styleUrls: ['./average-collect-dashboard.component.css']
})
export class AverageCollectDashboardComponent implements OnInit {

  constructor(
    private _dashboardService: DashboardService,
    private spinner: NgxSpinnerService,
    private storageService: StorageService ) { }

  public averageCollect = 0;

  ngOnInit() {

    this.getData();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[2] === 'globalSearch' 
      || parts[2] === 'globalSearchStartDate' 
      || parts[2] === 'globalSearchEndDate' 
      || parts[2] === 'chartScatterHarvest'
      ) {
        this.getData();
      }
    });

  }

  getData() {
    const startDate = this.storageService.getItem('globalSearchStartDate');
    const endDate = this.storageService.getItem('globalSearchEndDate');
    const search = this.storageService.getItem('globalSearch');

    const params: SearchGlobalParams = {
      startDate: startDate,
      endDate: endDate,
      search: search,
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };
    this.spinner.show();
    this._dashboardService.getAverageCollect(params).subscribe((response: any) => {
      if (response.length > 0) {
        let sum  = 0;
        let cont = 0;
        response.map((value) => {
          sum = sum + parseFloat(value['sum']);
          cont ++;
        });
        this.averageCollect = Math.round(sum / cont);
      } else {
        this.averageCollect = 0;
      }
      this.spinner.hide();
    });
  }

}
