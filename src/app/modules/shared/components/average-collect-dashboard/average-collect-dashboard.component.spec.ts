import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AverageCollectDashboardComponent } from './average-collect-dashboard.component';

describe('AverageCollectDashboardComponent', () => {
  let component: AverageCollectDashboardComponent;
  let fixture: ComponentFixture<AverageCollectDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AverageCollectDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AverageCollectDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
