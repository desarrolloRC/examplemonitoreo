import { Component, EventEmitter, Input, OnInit, Output, Inject } from '@angular/core';
import { MainService } from '../../services/main.service';
import { ScoreService } from '../../services/score.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import { AngularFireDatabase } from '@angular/fire/database';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-icon-information',
  templateUrl: './icon-information.component.html',
  styleUrls: ['./icon-information.component.css']
})
export class IconInformationComponent implements OnInit {
  @Input() id: number;
  @Input() contractId: number;

  public imgSrc = 'https://k3-public.s3.amazonaws.com/av2_operations/icons/actividad.svg';
  public data = [];

  constructor(private mainService: MainService,
    private spinner: NgxSpinnerService,
    private scoreService: ScoreService) { }

  ngOnInit() {
  }

  async showModal(id) {
    this.hideAll(id);
    this.getActivityByContractId(id);
    const element = document.getElementById(id);
    if (element.dataset.show === 'false') {
      element.dataset.show = 'true';
      element.className = 'd-block';
      this.imgSrc = 'https://k3-public.s3.amazonaws.com/av2_operations/icons/close_hover.svg';

    } else {
      this.hideModal(id);
    }
  }

  hideAll(id) {
    const elements = document.getElementsByClassName('d-block');
    for(let i = 0; i < elements.length; i++)
    {
      if (id != elements[i].id) {
        const element = document.getElementById(elements[i].id);
        element.className = 'd-none';
        element.dataset.show = 'false';
      }
    }
  }
  hideModal(id) {
    const element = document.getElementById(id);
    element.className = 'd-none';
    element.dataset.show = 'false';
    this.imgSrc = 'https://k3-public.s3.amazonaws.com/av2_operations/icons/actividad.svg';
  }

  getActivityByContractId(contractId) {
    this.mainService.getActivityByContractId(contractId).subscribe((result: any) => {
      this.data = result;
    })
  }

}
