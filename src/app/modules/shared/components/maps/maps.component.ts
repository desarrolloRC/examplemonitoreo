import {HeatmapLayer} from '@ngui/map';
import {Component, OnInit, ViewChild} from '@angular/core';

import {StorageService} from '../../services/storage.service';
import {VehicleGpsService} from '../../services/vehicle-gps.service';
import {MainService} from '../../../system/services/main.service';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css'],
  providers: [VehicleGpsService]
})
export class MapsComponent implements OnInit {
  public mapType = '';
  public points = [];
  public pointsArray = [];
  public lat = 0;
  public lng = 0;
  public showMarkerPlate = false;
  public showMap = false;

  @ViewChild(HeatmapLayer) heatMapLayer: HeatmapLayer;
  public heatMap: google.maps.visualization.HeatmapLayer;
  public map: google.maps.Map;

  constructor(
              private mainService: MainService,
              private vehicleGpsService: VehicleGpsService,
              private storageService: StorageService) {
  }

  ngOnInit() {
    this.initializeMap();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');

      if (parts[2] === 'globalSearch' 
      || parts[2] === 'globalSearchStartDate' 
      || parts[2] === 'globalSearchEndDate' 
      || parts[2] === 'chartScatterHarvest'
      ) {
                this.getPoints();
      }
    });
  }

  public initializeMap() {
    this.heatMapLayer['initialized$'].subscribe((heatMap) => {
      this.heatMap = heatMap;
      this.map = this.heatMap.getMap();
      this.mainService.getEnterprise().subscribe((response) => {
        setTimeout(() => {
          this.lat = response['Latitude'];
          this.lng = response['Longitude'];
          this.getPoints();
        }, 2000);
      });
    });
  }

  public initializeMapStepTwo() {
  }

  public getPoints() {
    this.points = [];
    this.pointsArray = [];
    this.mapType = 'terrain';

    this.vehicleGpsService.getVehiclePoints().subscribe((response) => {
      if (typeof response['data'] !== 'undefined') {
        response['data']['points'].map(item => {
          this.pointsArray.push({
            lat: item['latitude'],
            lng: item['longitude'],
            plate: item['plate']
          });
          this.points.push(new google.maps.LatLng(item['latitude'], item['longitude']));
        });
      }
    });

    setTimeout(() => {
      this.mapType = 'roadmap';
      } , 2000);


  }

  showPoints (event) {
    if (event.target.zoom >= 15) {
      this.showMarkerPlate = true;
    } else {
      this.showMarkerPlate = false;
    }
  }
}
