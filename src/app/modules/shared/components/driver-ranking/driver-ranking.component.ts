import { Component, OnInit } from '@angular/core';

import {JwtHelper} from '../../../../helpers/JwtHelper';
import {environment} from '../../../../../environments/environment';
import {DriverCommonStructure} from '../../../operation/definitions/DriverCommonStructure';

@Component({
  selector: 'app-driver-ranking',
  templateUrl: './driver-ranking.component.html',
  styleUrls: ['./driver-ranking.component.css']
})
export class DriverRankingComponent implements OnInit {
  public attributeLabels;
  public scoreMicroService = environment.urlScore;
  public showTable = false;

  constructor() { }

  ngOnInit() {
    const token = JwtHelper.decodeToken(sessionStorage.getItem('token'));
    this.attributeLabels = token['enterprise'] === 10 ?
      DriverCommonStructure.DrivingRankingFinsusAttributeLabels :
      DriverCommonStructure.DriverRankingAttributeLabels;
    this.showTable = !this.showTable;
  }

}
