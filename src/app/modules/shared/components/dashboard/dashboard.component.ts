import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {I18nHelper} from '../../../../helpers/I18nHelper';
import {StorageService} from '../../services/storage.service';
import {PermissionsService} from '../../../system/services/permissions.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent {
  public typesAvailable = [
    {
      name: 'ticketsPending', label: I18nHelper.getTranslate(this.translate, this.storageService, 'ticket.pending'),
      title: I18nHelper.getTranslate(this.translate, this.storageService, 'home.tickets_pending')
    },
    {name: 'paymentFrecuency', label: 'Frecuencia de pago', title: 'Dias promedio'},
    {name: 'paymentPercentage', label: 'Cumplimiento', title: 'Porcentaje de pago promedio'},
    {name: 'debtAmount', label: 'Deuda vencida', title: 'Monto de deuda vencida'},
    {name: 'lowKilometers', label: 'Productividad', title: 'Kilometros recorridos promedio'},
    {name: 'documentsExpired', label: 'Documentos vencidos', title: '# de documentos'},
    {name: 'documentsAboutToExpire', label: 'Documentos en riesgo', title: '# de documentos'},
    {name: 'timesBeyond', label: 'Velocidad alta', title: '# de veces por encima del limite'},
    {name: 'highKilometers', label: 'Kilometraje alto', title: 'Kilometros recorridos promedio'}
  ];

  constructor(public _PermissionService: PermissionsService,
              private storageService: StorageService,
              private translate: TranslateService) {
  }
}
