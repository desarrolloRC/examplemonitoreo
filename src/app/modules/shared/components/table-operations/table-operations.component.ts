import {Component, EventEmitter, HostListener, Input, OnInit, OnDestroy, Output, ViewChild, AfterViewInit} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {GetById, GlobalSearchParams} from '../../interfaces/SharedInterface';
import {StorageService} from '../../services/storage.service';
import {TableService} from '../../services/table.service';
import {TranslateService} from '@ngx-translate/core';
import {DriverService} from '../../services/driver.service';
import {merge, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import * as XLSX from 'xlsx';
import {AlertUpdateComponent} from '../../../risk/components/alert-update/alert-update.component';

@Component({
  selector: 'app-table-operations',
  templateUrl: './table-operations.component.html',
  styleUrls: ['./table-operations.component.css']
})
export class TableOperationsComponent implements OnInit, OnDestroy {
  @Input() columns = [];
  @Input() url: string;
  @Input() urlExcel: string;
  @Input() pageSize;
  @Input() tableName = '';
  @Input() centerText = 'centerText';
  @Input() hideButtons = true;
  @Input() microService: string;
  @Input() hidePagination = true;
  @Input() refreshOnFilter = false;
  @Input() pageSizeOptions = [5, 10, 15, 25, 50];
  @Input() typeAlert: string;
  @Input() componentName: string;
  @Input() showBasicBtns = true;
  @Input() showMaintenanceBtns = false;
  @Input() classCRow: string;

  @Output() clicked = new EventEmitter<object>();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public globalSearch: GlobalSearchParams = {
    globalInput: '',
    globalInputEndDate: this.currentDate(),
    globalInputStartDate: this.currentDate()
  };

  private alive = true;
  public dataSource = new MatTableDataSource();
  public resultsLength = 0;
  public isLoadingResults = true;
  public isRateLimitReached = false;
  public displayedColumns: string[];
  public exportColumns: string[];

  constructor(private storageService: StorageService,
              private _tableService: TableService,
              private translate: TranslateService,
              private _driverService: DriverService,
              public dialog: MatDialog) {
  }

  ngOnInit(): void {
    if (!this.refreshOnFilter && this.alive) {
      this.storageService.watchStorage().subscribe((data: string) => {
        const parts = data.split('|');

        if (parts[2] === 'globalSearch'
          || parts[2] === 'globalSearchStartDate'
          || parts[2] === 'globalSearchEndDate'
          || parts[2] === 'chartScatterHarvest'
        ) {
          this.globalSearch.globalInput = this.storageService.getItem('globalSearch');
          this.globalSearch.globalInputStartDate = this.storageService.getItem('globalSearchStartDate');
          this.globalSearch.globalInputEndDate = this.storageService.getItem('globalSearchEndDate');
          
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.getData();
        } else if (parts[2] === 'typeInstallment') {

          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.getData();
        }
      });
    }
    this.displayedColumns = this.columns.map(column => column.name);
    this.exportColumns = this.columns.map(column => column.nameExport);
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

  }

  ngAfterViewInit() {
  
    /* now it's okay to set large data source... */

    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.getData();
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  public click(event: any, property: string) {
    this.clicked.emit({event, property});
  }

  public getData() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;

          this.pageSize = typeof this.paginator.pageSize !== 'undefined' ? this.paginator.pageSize : this.pageSize;

          return this._tableService.requestData(
            this.microService,
            this.url,
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex,
            this.pageSize
          );
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;

          this.resultsLength = data['totalElements'];

          return data['content'];
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;

          return observableOf([]);
        })
      ).subscribe(data => {
      /*data.map(value => {
        if (typeof value['city_id'] !== 'undefined') {
          const params: GetById = {
            id: value['city_id']
          };

          value['city_id'] = '';
          this._driverService.getCityById(params).subscribe(response => value['city_id'] = response[0]['label']);
        }

        if (typeof value['location'] !== 'undefined') {
          const id = value['location'];
          const params: Object = {
            plate: value['location']
          };
          value['location'] = '';
          this._driverService.getLastLocation(params).subscribe((responseLocation: any) => {
            if (responseLocation.length > 0) {
              value['location'] = `${responseLocation[0]['date']}`;
            } else {
              value['location'] = `-1`;
            }
          });
        }
        if (typeof value['plate_id'] !== 'undefined') {
          const plate = value['plate_id'];
          const params: Object = {
            plate: value['plate_id']
          };
          value['plate_id'] = '';
          this._driverService.getIdByPlate(params).subscribe(response => value['plate_id'] = `${plate}|${response[0]['id']}|${response[0]['number']}`);
        }
      });*/
      this.dataSource = data;
    });
  }

  currentDate() {
    const date = new Date();
    const day = date.getDate();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    return year + '-' + month + '-' + day;
  }

  colorClass(row, column) {
    let classColor = '';

    if (row[column.name] === 'Al dia') {
      classColor = 'on_day';
    } else if (row[column.name] === 'Sin Info') {
      classColor = 'no_info';
    } else if (row[column.name] === 'No asistio') {
      classColor = 'no_go';
    }

    return classColor;
  }

  downloadGpsdata() {

    this._driverService.getLastLocationReport().subscribe(async (data: any) => {
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);

      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      XLSX.writeFile(wb, 'SheetJS.xlsx');
    });
  }

  downloadExcel() {
    this._tableService.requestDataExcelParams(
      this.microService,
      this.urlExcel,
      this.exportColumns
    ).subscribe(async (data) => {
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data.content);

      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      XLSX.writeFile(wb, 'SheetJS.xlsx');
      console.log(this.displayedColumns);
    });
  }

  @HostListener('click', ['$event'])
  onClick(e) {
    if (e.target.classList.contains('globalActivateSearch')) {
      const arrayData = e.target.getAttribute('data-id').split('|');
      const data: Object = {
        id: arrayData[0],
        label: `${arrayData[2]} - ${arrayData[1]} (Vehículo)`,
        type: 'vehicle',
      };
      this.storageService.setItem('globalSearch', JSON.stringify(data));
    }
  }

  openDialogUpdate() {
    const type = this.typeAlert;
    if (typeof type !== 'undefined') {
      const dialogRef = this.dialog.open(AlertUpdateComponent, {
        panelClass: 'custom-modalbox-update-payment',
        data: {type: type}
      });
    }
  }
}
