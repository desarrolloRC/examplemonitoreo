import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

import * as uuid from 'uuid';
import { QueuingService } from '../../services/queuing.service';
import { FirebaseHelper } from '../../../../helpers/FirebaseHelper';
import {AlertComponent} from '../alert/alert.component';
import {CustomerDetailComponent} from '../customer-detail/customer-detail.component';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  public events;
  public contractId;
  public submitted = false;
  public searchOptions = [];
  public eventForm: FormGroup;
  public gData = { driverInfo: 0 };
  public itemsRef: AngularFireList<any>;
  public alertModal: MatDialogRef<AlertComponent>;
  public gDataDriver = { Name: '', Phone: '', Email: '', LastName: '', Age: 0, PersonId: 0, number: 0, type: '', PhoneCode: 0 };

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog,
    private queuingService: QueuingService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<NotificationsComponent>,
    private formBuilder: FormBuilder,
    private db: AngularFireDatabase) {
    this.contractId = data.ContractId;
  }

  ngOnInit() {
    this.getTypeEvent();
    this.eventForm = this.formBuilder.group({
      eventStatus: [4],
      contractId: [this.contractId],
      notificationType: [null, Validators.compose([Validators.required])],
      notificationText: [null, Validators.compose([Validators.required])]
    });
  }

  public getTypeEvent() {
    this.events = ['email', 'sms'];
  }

  public  async  alert() {
    const param = {
      minWidth: '382px',
      height: '186px',
      data: {text: 'Se ha notificado al conductor'}
    };
    this.alertModal = this.dialog.open(AlertComponent, param);
  }

  public async saveNotification() {
    this.submitted = true;

    if (this.eventForm.valid) {
      const type = this.eventForm.controls['notificationType'].value;
      let params = {};
      let sendNotification = false;
      if (type === 'email') {
        params = {
          template: 'campaing',
          subject: 'Nueva Notificación',
          email: this.gDataDriver.Email,
          emailData: {
            title: `Señor (a) ${this.gDataDriver.Name} ${this.gDataDriver.LastName}`,
            bodyText: this.eventForm.controls['notificationText'].value,
            buttonLink: 'taximo.co',
            buttonText: 'taximo.co'
          }
        };
        if (typeof this.gDataDriver.Email !== 'undefined' && this.gDataDriver.Email !== '') {
          sendNotification = true;
        } else {
          sendNotification = false;
          this.sendSnack('No se puede enviar el email, conductor no tiene email');
        }
      } else {
        params = [{
          toNumber: this.gDataDriver.Phone,
          countryCode: this.gDataDriver.PhoneCode,
          message: this.eventForm.controls['notificationText'].value
        }];
        if (typeof this.gDataDriver.PhoneCode !== 'undefined' && this.gDataDriver.PhoneCode > 0) {
          sendNotification = true;
        } else {
          sendNotification = false;
          this.sendSnack('No se puede enviar el email, conductor no tiene codigo telefonico de el pais');
        }
      }

      if (sendNotification) {
        await this.queuingService.newNotificion(type, params).subscribe((response) => { }, err => {
          console.log(err);
          if (err.status !== 200) {
            this.sendSnack('Error al enviar la notificación, revise los parametros');
          } else if (err.status === 200) {
            this.sendSnack('Notifación realizada');
            setTimeout(() => {
              this.dialogRef.close();
            }, 2000);
          }
        }, () => {
          this.sendSnack('Notifación realizada');
          setTimeout(() => {
            this.dialogRef.close();
          }, 2000);
        });
      }

    } else {
      this.sendSnack('Llene los campos requeridos');
    }

    this.submitted = false;
  }

  sendSnack(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }
}
