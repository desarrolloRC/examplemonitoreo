import {Component, OnInit} from '@angular/core';

import {OperationDashboardService} from '../../../operation/services/operation-dashboard.service';
import {StorageService} from '../../services/storage.service';

@Component({
  selector: 'app-all-vehicles',
  templateUrl: './all-vehicles.component.html',
  styleUrls: ['./all-vehicles.component.css']
})
export class AllVehiclesComponent implements OnInit {
  public count = 0;

  constructor(private operationService: OperationDashboardService, private storageService: StorageService,) {
  }

  ngOnInit() {
    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[2] === 'globalSearch' 
      || parts[2] === 'globalSearchStartDate' 
      || parts[2] === 'globalSearchEndDate'
      || parts[2] === 'chartScatterHarvest'
      ) {
        this.getData();
      }
    });
    this.getData();
  }

  getData() {

    this.operationService.getVehiclesCount().subscribe((response: Array<object>) => {
      if (response !== null && response.length > 0) {
        this.count = response.length;
      } else {
        this.count = 0;
      }
    });

  }
}
