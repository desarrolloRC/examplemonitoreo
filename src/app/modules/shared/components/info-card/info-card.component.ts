import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.css']
})
export class InfoCardComponent implements OnInit {
  @Input() units;
  @Input() link: string;
  @Input() title: string;
  @Input() chartText = 0;
  @Input() chartPercent = 0;
  @Input() showSubtitle = false;
  @Input() chartColor = '#4882c2';
  @Input() chartInnerColor = '#4882c2';
  @Input() gradient = false;
  @Input() gradientColor = '#4882c2';
  @Input() outerStrokeWidth = 10;
  @Input() innerStrokeWidth = 10;
  @Input() background = false;
  @Input() backgroundColor = '';
  @Input() innerStroke = false;
  @Input() space = -10;
  @Input() clockwise = true;

  constructor() {
  }

  ngOnInit() {
  }

}
