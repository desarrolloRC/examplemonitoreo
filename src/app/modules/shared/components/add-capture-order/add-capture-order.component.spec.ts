import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCaptureOrderComponent } from './add-capture-order.component';

describe('AddCaptureOrderComponent', () => {
  let component: AddCaptureOrderComponent;
  let fixture: ComponentFixture<AddCaptureOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCaptureOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCaptureOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
