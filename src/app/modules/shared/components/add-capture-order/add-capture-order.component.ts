import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {DriverService} from '../../services/driver.service';

@Component({
  selector: 'app-add-capure-order',
  templateUrl: './add-capture-order.component.html',
  styleUrls: ['./add-capture-order.component.css']
})
export class AddCaptureOrderComponent implements OnInit {

  data: any;
  dataFinal: object[] = [];
  dataSend: object[] = [];

  constructor(private _location: Router,
              private _snackBar: MatSnackBar,
              private _DrivingService: DriverService) { }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    // if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <any>(XLSX.utils.sheet_to_json(ws, {header: 1}));
      this.proccessFile();

    };
    reader.readAsBinaryString(target.files[0]);
  }

  proccessFile () {
    this.dataFinal = [];
    this.dataSend = [];
    this.data.map((value) => {
      if ((typeof value[0] === 'number' || typeof value[0] === 'string') && value[0] !== 'Placa') {
        if ((typeof value[1] === 'number' || typeof value[1] === 'string') && value[1] !== '' &&
          (typeof value[3] === 'number' || typeof value[3] === 'string') && value[3] !== '' &&
          value[1] !== '' && value [3] !== '') {
          this._DrivingService.getIdByPlate({plate: value[0]}).subscribe((response: Array<any>) => {
            if (response.length > 0) {
              const data = {
                plate: value[0],
                vehicle_id: response[0]['id'],
                document: value[1],
                year: value[2],
                concept: value[3],
                original_plate: value[4],
                reference: value[5],
                amount: value[6],
                status: 'success'
              };
              this.dataFinal.push(data);
              this.dataSend.push(data);
            } else {
              const data = {
                plate: value[0],
                vehicle_id: '',
                document: value[1],
                concept: value[3],
                status: 'danger'
              };
              this.dataFinal.push(data);
            }
          });
        } else {
          const data = {
            plate: value[0],
            vehicle_id: '',
            document: value[1],
            concept: value[3],
            status: 'danger'
          };
          this.dataFinal.push(data);
        }
      }
    });
  }


  saveCapture() {
    const params = {
      data: this.dataSend
    };
    this._DrivingService.saveCaptureOrderExcel(params).subscribe((response) => {
      this._location.navigate(['/']).then();
      this.openSnackBar('archivo cargado', 'Upload');
    }, (err) => {
      this.openSnackBar('Error al cargar las capturas', 'Error');
      console.log(err);
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
