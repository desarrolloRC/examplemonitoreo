import {Component, Input, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {CallComponent} from '../call/call.component';
import {CustomerDetailComponent} from '../customer-detail/customer-detail.component';
import {NotificationsComponent} from '../notifications/notifications.component';
import {ResolveAlertComponent} from '../resolve-alert/resolve-alert.component';
import { PermissionsService } from 'src/app/modules/system/services/permissions.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tablet-actions',
  templateUrl: './tablet-actions.component.html',
  styleUrls: ['./tablet-actions.component.css']
})
export class TabletActionsComponent implements OnInit {
  @Input() data = {};
  @Input() id: number;
  @Input() showBasicBtns = true;
  @Input() showMaintenanceBtns = false;
  @Input() componentName: string;

  public imgSrc = 'https://k3-public.s3.amazonaws.com/av2_operations/icons/mas_opciones.svg';

  public customerDetailModal: MatDialogRef<CustomerDetailComponent>;
  public resolveAlertModal: MatDialogRef<ResolveAlertComponent>;
  public callModal: MatDialogRef<CallComponent>;
  public notificationModal: MatDialogRef<NotificationsComponent>;

  constructor(private dialog: MatDialog,
    public _PermissionService: PermissionsService,
    private _location: Router,) {
  }

  ngOnInit() {
  }

  showModal(id) {
    const element = document.getElementById(id + '_actions' + this.componentName);
    if (element.dataset.show === 'false') {
      element.className = 'd-block';
      element.dataset.show = 'true';
      this.imgSrc = 'https://k3-public.s3.amazonaws.com/av2_operations/icons/close_hover.svg';

    } else {
      element.className = 'd-none';
      element.dataset.show = 'false';
      this.imgSrc = 'https://k3-public.s3.amazonaws.com/av2_operations/icons/mas_opciones.svg';
    }
  }

  showCallModal() {
    const param = {
      width: '400px',
      data: this.data
    };
    this.callModal = this.dialog.open(CallComponent, param);
  }

  showResolveModal() {
    const param = {
      width: '400px',
      data: this.data
    };

    this.resolveAlertModal = this.dialog.open(ResolveAlertComponent, param);
  }

  showNotificationModal() {
    const param = {
      width: '320px',
      data: this.data
    };
    this.notificationModal = this.dialog.open(NotificationsComponent, param);
  }

  showMaintenanceModal() {
    const param = {
      minWidth: '85vw',
      height: '90vh',
      data: this.data,
      backdropClass: 'container-call'
    };
    this.customerDetailModal = this.dialog.open(CustomerDetailComponent, param);
  }
}
