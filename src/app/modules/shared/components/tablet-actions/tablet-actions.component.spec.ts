import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabletActionsComponent } from './tablet-actions.component';

describe('TabletActionsComponent', () => {
  let component: TabletActionsComponent;
  let fixture: ComponentFixture<TabletActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabletActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabletActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
