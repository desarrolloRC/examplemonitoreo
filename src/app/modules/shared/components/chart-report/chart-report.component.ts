import {Component, Input, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import { DriverService } from '../../services/driver.service';
import { StorageService } from '../../services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';
import * as XLSX from 'xlsx';
import { ChartSelectEvent } from 'ng2-google-charts';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatTableDataSource, MatSort} from '@angular/material';
import { InfoChartPartialComponent } from '../info-chart-partial/info-chart-partial.component';
import { PermissionsService } from 'src/app/modules/system/services/permissions.service';
import {CharGroupHelper} from '../../../../helpers/CharGroupHelper';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-chart-report',
  templateUrl: './chart-report.component.html',
  styleUrls: ['./chart-report.component.css']
})
export class ChartReportComponent implements OnInit {

  public chartControl = new FormControl();
  public chartGroups: any[];

  constructor(
    private _DriverService: DriverService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService,
    public _PermissionService: PermissionsService,
    private dialog: MatDialog,
  ) {
  }

  public colors = [
    ['#5574F6', '#9473E9', '#BC76D9', '#D67DC9', '#E78ABB'],
    ['#6776B4', '#628EC8', '#60A6D7', '#66BDE0', '#6CC8E2'],
    ['#FADE70', '#CDD46A', '#A4C96B', '#7FB971', '#5EA978'],
    ['#94D669', '#5BC786', '#51B5A2', '#45A0B3', '#3988B4']
  ];

  public dataDownload;
  public dataDownloadCompare;
  public compare = false;
  public chartTitle = '';
  public chartDescription = '';
  public chartType = '';
  public chartName = '';
  public chartEndpoint = '';
  public compareMoney = false;
  public category = '';

  public valueCurrent = 0;
  public valuePrevious = 0;
  public difference = 0;
  public selected = '';

  public startDatePeriod1 = new FormControl(new Date(this.storageService.getItem('globalSearchStartDate')));
  public endDatePeriod1 = new FormControl(new Date(this.storageService.getItem('globalSearchEndDate')));
  public startDatePeriod2 = new FormControl(new Date(this.storageService.getItem('globalSearchStartDate')));
  public endDatePeriod2 = new FormControl(new Date(this.storageService.getItem('globalSearchEndDate')));

  public table = false;

  public chart: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: []
  };

  public chartCompare: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: []
  };

  public chartModalValues = [];

  public infoChart: MatDialogRef<InfoChartPartialComponent>;

  public ELEMENT_DATA = [];
  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    setTimeout(() => {
      this.chartGroups = CharGroupHelper.getCharGroups(this.translate, this.storageService);
      const chart = this.chartGroups[0].charts[0];
      this.selected = chart.value;
      this.chartTitle = chart.title;
      this.chartDescription = chart.desc;
      this.chartType = chart.type;
      this.chartName = chart.value;
      this.chartEndpoint = chart.endPoint;
      this.compareMoney = chart.compareMoney;
      this.category = chart.category;
      this.table = chart.table;
      setTimeout(() => {
        this.validateCharts();
      }, 2000);
    }, 2000)

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.changeChartData(false);
      } else if (parts[2] === 'globalSearchStartDate') {
        this.changeChartData(false);
      } else if (parts[2] === 'globalSearchEndDate') {
        this.changeChartData(false);
      } else if (parts[2] === 'chartScatterHarvest') {
        this.changeChartData(false);
      }
    });
  }

  validateCharts () {
    const validCharts = [];

    this.chartGroups.map((group, indexGroup) => {
      if (this._PermissionService.canAccess(group.permission)) {
        const groupPartial = group;
        const groupCharts = [];
        group.charts.map((chart, indexChart) => {
          if (this._PermissionService.canAccess(chart.permission)) {
            groupCharts.push(chart);
          }
        });
        group.charts = groupCharts;
        validCharts.push(group);
      }
    });
    this.chartGroups = validCharts;

    const chart = this.chartGroups[0].charts[0];
    this.selected = chart.value;
    this.chartTitle = chart.title;
    this.chartDescription = chart.desc;
    this.chartType = chart.type;
    this.chartName = chart.value;
    this.chartEndpoint = chart.endPoint;
    this.compareMoney = chart.compareMoney;
    this.category = chart.category;
    this.table = chart.table;
    this.changeChartData(false);

  }

  toggleCompare() {
    this.compare = !this.compare;
    this.changeChartData();
  }

  public openAsPNG() {
    const img = this.chart.component.wrapper.getChart().getImageURI();
    const blobData = this.convertBase64ToBlobData(img.replace('data:image/png;base64,', ''));
    if (window.navigator && window.navigator.msSaveOrOpenBlob) { // IE
      window.navigator.msSaveOrOpenBlob(blobData, 'chart.png');
    } else {
      const blob = new Blob([blobData], { type: 'image/png' });
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = 'chart.png';
      link.click();
    }
  }

  public downloadData() {
    console.log(this.dataDownload);
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.dataDownload);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, 'Report.xlsx');
  }

  public downloadDataCompare() {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.dataDownloadCompare);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    XLSX.writeFile(wb, 'Report.xlsx');
  }

  convertBase64ToBlobData(base64Data: string, contentType: string= 'image/png', sliceSize= 512) {
    const byteCharacters = atob(base64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  changeSelector(event) {
    const chartName = event.value;
    let found = false;
    this.chartGroups.map((group) => {
      group.charts.map((chart) => {
        if (chart.value === chartName && !found) {
          found = true;
          this.chartTitle = chart.title;
          this.chartDescription = chart.desc;
          this.chartType = chart.type;
          this.chartName = chartName;
          this.selected = chartName;
          this.chartEndpoint = chart.endPoint;
          this.compareMoney = chart.compareMoney;
          this.category = chart.category;
          this.table = chart.table;
          this.changeChartData();
        }
      });
    });
  }

  changeChartData(showSpinner = true) {
    if (showSpinner) {
      this.spinner.show();
    }
    const random = Math.floor((Math.random() * 4) + 1);

    this.ELEMENT_DATA = [];

    const paramsPeriod1 = {
      startDate: this.startDatePeriod1.value,
      endDate: this.endDatePeriod1.value,
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };
    const paramsPeriod2 = {
      startDate: this.startDatePeriod2.value,
      endDate: this.endDatePeriod2.value,
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    const chartNew: GoogleChartInterface = {
        chartType: this.chartType,
        dataTable: [],
        options: {
          width: 400,
          height: 350,
          chartArea: {width: '100%', height: '50%'},
          pieHole: 0.5,
          legend: {position: 'labeled', textStyle: {fontSize: 10, margin: 0}},
          pointsVisible: true,
          colors: this.colors[(random - 1)],
          pieSliceBorderColor: 'transparent',
          theme: 'material',
          vAxis: {
            textPosition: 'in',
            textStyle: {
              bold: true
            }
          }
        }
      };
      if (typeof this.chartEndpoint !== 'undefined') {
        if (this.table === true) {
          this._DriverService.getChartData(this.chartEndpoint).subscribe(async (response: any ) => {

            this.displayedColumns = response.data[0];

            const data = [];
            response.data.map((value, index) => {
              if (index > 0 ) {
                const dataSingle = {};
                response.data[0].map((header, headerIndex) => {
                  dataSingle[header] = value[headerIndex];
                })
                data.push(dataSingle);
              }
            });
            if (typeof response.dataAll !== 'undefined') {
              this.chartModalValues = response.dataAll;
            }
            this.ELEMENT_DATA = data;
            this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
          });
        } else {
          if (this.compare) {
            this._DriverService.getChartData(this.chartEndpoint, paramsPeriod1).subscribe(async (response: any ) => {
              chartNew.dataTable = response.data;
              this.valuePrevious = response.value === null ? 0 : response.value;
              const ccComponent = this.chartCompare.component;
              this.dataDownloadCompare = response.dataAll;
              if (typeof ccComponent !== 'undefined') {
                this.chart = chartNew;
                const ccWrapper = ccComponent.wrapper;
                ccComponent.draw();
              }
              this._DriverService.getChartData(this.chartEndpoint, paramsPeriod2).subscribe(async (response: any ) => {
                chartNew.dataTable = response.data;
                this.valuePrevious = response.value === null ? 0 : response.value;
                const ccComponent = this.chartCompare.component;
                if (typeof ccComponent !== 'undefined') {
                  this.chartCompare = chartNew;
                  const ccWrapper = ccComponent.wrapper;
                  ccComponent.draw();
                }
                this.spinner.hide();
              });
            });
          } else {
            this._DriverService.getChartData(this.chartEndpoint).subscribe(async (response: any ) => {
              
              if (this.chartType === 'LineChart' || this.chartType === 'AreaChart') {
                chartNew.options['chartArea']['width'] = '80%';
                chartNew.options['hAxis'] = {gridlines:{color: 'transparent', count: 0}};
                chartNew.options['legend'] = {position: 'top', textStyle: {fontSize: 10, margin: 0}};
                chartNew.options['vAxis'] = {
                                              gridlines:{color: 'transparent', count: 0},
                                              textPosition: 'in',
                                              textStyle: {
                                                bold: true
                                              }
                                            };

                response.data.map((value, index) => {
                  if (index > 0) {
                    response.data[index][0] = new Date(response.data[index][0].replace('.000Z', ''))
                  }
                })
              }
              
              chartNew.dataTable = response.data;
              if (typeof response.dataAll !== 'undefined') {
                this.chartModalValues = response.dataAll;
              }
              const value = response.value === null ? 0 : response.value;
              this.chartDescription = this.chartDescription.replace('{{VALUE}}', value.toFixed(1));
              this.dataDownload = response.dataAll;
              const ccComponent = this.chart.component;
              if (typeof ccComponent !== 'undefined') {
                this.chart = chartNew;
                const ccWrapper = ccComponent.wrapper;
                if (typeof ccWrapper !== 'undefined') {
                  ccComponent.draw();
                }
              }
  
              this.spinner.hide();
            });
          }
        }
      }
  }

  public selectTable(category, column) {
    if (column === 'Categoria') {
      if (this.chartModalValues.length > 0) {
        const headers = Object.keys(this.chartModalValues[0]);
        const data = [];
        this.chartModalValues.map((value) => {
          if (value[this.category].indexOf(category) !== -1 ) {
            data.push(value);
          }
        });
        const param = {
          maxWidth: '95vw',
          maxHeight: '80vh',
          data: {
            headers,
            data,
            title: this.chartTitle,
            subtitle: category
          },
          backdropClass: 'info-chart-call'
        };
        this.infoChart = this.dialog.open(InfoChartPartialComponent, param);
      }
    }
  }

  public select(event: ChartSelectEvent) {
    if (this.chartModalValues.length > 0) {
      const headers = Object.keys(this.chartModalValues[0]);
      const data = [];
      if (this.chartType === 'LineChart' || this.chartType === 'AreaChart') {
        const dateCompareValue  = String(event.selectedRowValues[0]).replace('.000Z', '')
        const dateCompare = new Date(dateCompareValue);
        const dateCompareString = `${dateCompare.getFullYear()}-${(dateCompare.getMonth() + 1)}-${dateCompare.getDate()}`;

        this.chartModalValues.map((value) => {
          const date = new Date(value.date.replace('.000Z', ''));
          const dateString = `${date.getFullYear()}-${(date.getMonth() + 1)}-${date.getDate()}`;
          if (value[this.category].indexOf(event.columnLabel) !== -1 && dateString === dateCompareString) {
            data.push(value);
          }
        });
        const param = {
          maxWidth: '95vw',
          maxHeight: '80vh',
          data: {
            headers,
            data,
            title: this.chartTitle,
            subtitle: `${dateCompareString} - ${event.columnLabel}`
          },
          backdropClass: 'info-chart-call'
        };
        this.infoChart = this.dialog.open(InfoChartPartialComponent, param);
      } else {
        this.chartModalValues.map((value) => {
          if (value[this.category].indexOf(event.selectedRowValues[0]) !== -1 ) {
            data.push(value);
          }
        });
        const param = {
          maxWidth: '95vw',
          maxHeight: '80vh',
          data: {
            headers,
            data,
            title: this.chartTitle,
            subtitle: event.selectedRowValues[0]
          },
          backdropClass: 'info-chart-call'
        };
        this.infoChart = this.dialog.open(InfoChartPartialComponent, param);
      }
    }
  }
}
