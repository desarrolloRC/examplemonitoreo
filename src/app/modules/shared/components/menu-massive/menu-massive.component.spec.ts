import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuMassiveComponent } from './menu-massive.component';

describe('MenuMassiveComponent', () => {
  let component: MenuMassiveComponent;
  let fixture: ComponentFixture<MenuMassiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuMassiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuMassiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
