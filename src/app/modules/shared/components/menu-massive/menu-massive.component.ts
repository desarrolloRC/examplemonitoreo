import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {StorageService} from '../../services/storage.service';
import {TranslateService} from '@ngx-translate/core';
import { I18nHelper } from '../../../../helpers/I18nHelper';
import { PermissionsService } from 'src/app/modules/system/services/permissions.service';

@Component({
  selector: 'app-menu-massive',
  templateUrl: './menu-massive.component.html',
  styleUrls: ['./menu-massive.component.css']
})
export class MenuMassiveComponent implements OnInit {

  constructor(
    private translate: TranslateService, private storageService: StorageService,
    private _location: Router,
    public _PermissionService: PermissionsService,
  ) { }

  public options = []

  ngOnInit() {
    setTimeout(() => {

      this.options = [
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.capture_order'),
          link: '/add-capture-order'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.license_points'),
          link: '/system/newLicenseScore'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.tickets'),
          link: '/add-tickets'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.gas_certificate'),
          link: '/system/uploadDocument'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.gas_cylinder_certificate'),
          link: '/system/uploadDocument'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.techinical_inspection'),
          link: '/system/uploadDocument'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.soat'),
          link: '/system/uploadDocument'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.property_card'),
          link: '/system/uploadDocument'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.circulation_card'),
          link: '/system/uploadDocument'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.municipal_authorization'),
          link: '/system/uploadDocument'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.security'),
          link: '/system/uploadDocument'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.credential'),
          link: '/system/uploadDocument'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.drivers_license'),
          link: '/system/uploadDocument'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.debt_points'),
          link: '/new-payment-points'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.massive_load'),
          link: '/add-drivers'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.reestructure_contract'),
          link: '/reestructure-contract'
        },
        {
          label: I18nHelper.getTranslate(this.translate, this.storageService, 'menu.new_estimate'),
          link: '/new-estimate'
        },
      ];
    }, 2000)
  }

  routeTo(link) {
    if (typeof link !== 'undefined' && link !== '') {
      this._location.navigate([link]).then();
    }
  }

}
