import {Component, Input, Inject, OnInit, ViewChild} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort} from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-info-chart-partial',
  templateUrl: './info-chart-partial.component.html',
  styleUrls: ['./info-chart-partial.component.css']
})
export class InfoChartPartialComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public table,
  ) {
  }

  public ELEMENT_DATA = [];
  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public title = 'test';
  public subtitle = 'testSub';
  public numberRecords = 0;
  
  ngOnInit() {
    this.displayedColumns = this.table['headers'];
    this.ELEMENT_DATA = this.table['data'];
    this.numberRecords = this.table['data'].length;
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.title = this.table['title'];
    this.subtitle = this.table['subtitle'];
  }


  public downloadData() {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.table['data']);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    XLSX.writeFile(wb, 'Report.xlsx');
  }

}
