import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoChartPartialComponent } from './info-chart-partial.component';

describe('InfoChartPartialComponent', () => {
  let component: InfoChartPartialComponent;
  let fixture: ComponentFixture<InfoChartPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoChartPartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoChartPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
