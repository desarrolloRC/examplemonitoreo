import {Component, OnInit} from '@angular/core';

import {StorageService} from '../../services/storage.service';
import {OperationDashboardService} from '../../../operation/services/operation-dashboard.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-vehicle-dashboard',
  templateUrl: './vehicle-dashboard.component.html',
  styleUrls: ['./vehicle-dashboard.component.css'],
  providers: [OperationDashboardService]
})
export class VehicleDashboardComponent implements OnInit {
  public vehiclesOn = '0';
  public vehicleTotal = 0;
  public showAll = true;
  public titleCard = 'Número de Vehículos encendidos';

  constructor(private operationDashboardService: OperationDashboardService,
              private spinner: NgxSpinnerService,
              private storageService: StorageService) {
  }

  ngOnInit() {
    this.getVehiclesOn();

    let global = this.storageService.getItem('globalSearch');
    this.storageService.watchStorage().subscribe((data) => {
      this.getVehiclesOn();
      const parts = data.split('|');
      if (parts[2] === 'globalSearch') {
        global = this.storageService.getItem('globalSearch');
        if (global !== null && global !== '') {
          this.titleCard = '¿El vehículo estuvo encendido en esté período de tiempo?';
        } else {
          this.titleCard = 'Número de Vehículos encendidos';
        }
      }
    });
    if (global !== null && global !== '') {
      this.titleCard = '¿El vehículo estuvo encendido en esté período de tiempo?';
    } else {
      this.titleCard = 'Número de Vehículos encendidos';
    }
  }

  public getVehiclesOn() {
    const global = this.storageService.getItem('globalSearch');
    this.spinner.show();
    this.operationDashboardService.getVehiclesOn().subscribe((response) => {
      if (typeof response['data'] !== 'undefined') {
        if (global !== null && global !== '') {
          this.showAll = false;
          this.vehiclesOn = response['data']['vehicles_on'] > 0 ? 'Si' : 'No';
        } else {
          this.showAll = true;
          this.vehiclesOn = response['data']['vehicles_on'];
          this.vehicleTotal = response['data']['total'];
        }
      }
      this.spinner.hide();
    });
  }

}
