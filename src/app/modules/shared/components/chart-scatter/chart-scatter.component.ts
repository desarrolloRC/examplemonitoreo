import * as moment from 'moment';
import {FormControl} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {ChartSelectEvent} from 'ng2-google-charts';
import {GoogleChartInterface} from 'ng2-google-charts/google-charts-interfaces';
import {MatSliderChange, MatSnackBar, MatDialogRef, MatDialog} from '@angular/material';

import {NgxSpinnerService} from 'ngx-spinner';
import {TranslateService} from '@ngx-translate/core';

import {environment} from 'src/environments/environment';
import {I18nHelper} from '../../../../helpers/I18nHelper';
import {DriverService} from '../../services/driver.service';
import {StorageService} from '../../services/storage.service';
import {ConfigurationService} from 'src/app/modules/system/services/configuration.service';
import {ContractInfoSmallComponent} from '../contract-info-small/contract-info-small.component';
import {OperationDashboardService} from '../../../operation/services/operation-dashboard.service';

@Component({
  selector: 'app-chart-scatter',
  templateUrl: './chart-scatter.component.html',
  styleUrls: ['./chart-scatter.component.css']
})
export class ChartScatterComponent implements OnInit {
  public max = 0;
  public compare = false;
  public startDatePeriod1 = new Date(this.storageService.getItem('globalSearchStartDate'));
  public startDatePeriod2 = new Date(this.storageService.getItem('globalSearchEndDate'));
  public chartCompare: GoogleChartInterface = {
    chartType: 'ScatterChart',
    dataTable: []
  };
  public harvest = new FormControl();
  public harvestList = [];
  public count = 0;
  public selected = 0;

  public contractInfoSmall: MatDialogRef<ContractInfoSmallComponent>;

  constructor(
    private _DriverService: DriverService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private _snackBar: MatSnackBar,
    private operationService: OperationDashboardService,
    private translate: TranslateService,
    private configurationService: ConfigurationService,
    private dialog: MatDialog
  ) {
  }

  public paramY = '';
  public paramYLabel = '';

  public productives = [
    {value: 'documents', viewValue: 'Documentos'},
    {value: 'tickets_type_m', viewValue: I18nHelper.getTranslate(this.translate, this.storageService, 'char.tickets_type_m')},
    {value: 'tickets_status', viewValue: I18nHelper.getTranslate(this.translate, this.storageService, 'char.tickets_status')},
    {value: 'tickets_type_g', viewValue: I18nHelper.getTranslate(this.translate, this.storageService, 'char.tickets_type_g')},
    {value: 'tickets_type_m_full', viewValue: I18nHelper.getTranslate(this.translate, this.storageService, 'char.tickets_type_m_full')},
    {value: 'tickets_type_r', viewValue: I18nHelper.getTranslate(this.translate, this.storageService, 'char.tickets_type_r')},
    {value: 'driver_points', viewValue: 'Puntos de Licencia'},
    {value: 'tickets_yesterday', viewValue: I18nHelper.getTranslate(this.translate, this.storageService, 'char.tickets_yesterday')}
  ];

  public behaviours = [
    {value: 'speed_limit_times', viewValue: 'Cantidad de Veces que sobrepaso la velocidad maxima permitida'},
    {value: 'percentage_difference_distance', viewValue: 'Porcentaje de diferencia distancia'},
    {value: 'percentage_difference_speed', viewValue: 'Porcentaje de diferencia velocidad'}
  ];

  public cares = [];

  public productive = '';

  public behaviour = '';

  public care = '';

  public yVar = '';
  public xVar = '';

  public contracts = [];

  ngOnInit() {
    this.getData();
    setTimeout(() => {
      const harvestStorage = this.storageService.getItem('harvest');
      if (harvestStorage !== null) {
        const harvestSplit = this.storageService.getItem('harvest').split(',');
        if (typeof harvestSplit !== 'undefined' && harvestSplit.length > 0 && harvestSplit[0] !== '') {
          const harvestArray = [];
          harvestSplit.map((value) => {
            harvestArray.push(parseInt(value, 10));
          });
          this.harvestList = harvestArray;
        }
      }
      const harvest = this.storageService.getItem('chartScatterHarvest');
      if (harvest !== null) {
        const harvestSplit = this.storageService.getItem('chartScatterHarvest').split(',');
        if (typeof harvestSplit !== 'undefined' && harvestSplit.length > 0 && harvestSplit[0] !== '') {
          const harvestArray = [];
          harvestSplit.map((value) => {
            harvestArray.push(parseInt(value, 10));
          });
          this.harvest.setValue(harvestArray);
        }
      }

      this.resetChart();
    }, 3000);

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.resetChart();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.startDatePeriod1 = new Date(this.storageService.getItem('globalSearchStartDate'));
        this.resetChart();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.startDatePeriod2 = new Date(this.storageService.getItem('globalSearchEndDate'));
        this.resetChart();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.resetChart();
      }
    });
  }

  toggleCompare() {
    this.compare = !this.compare;
    this.dayDifference();
  }

  dayDifference() {
    const days = moment(this.startDatePeriod1['value']);
    const daysb = moment(this.startDatePeriod2['value']);
    this.max = daysb.diff(days, 'days');
    this.resetChart();
  }

  formatLabel(event: MatSliderChange) {
    const aux = event['value'];
    const days = moment(this.startDatePeriod1['value']);
    const startDate = days.add(aux, 'days');
    const endDate = this.startDatePeriod2['value'];
    this.changeChartData(startDate, endDate);
    return event['value'];
  }

  resetChart() {
    const startDate = this.startDatePeriod1['value'];
    let endDate = this.startDatePeriod2['value'];
    if (!this.compare) {
      endDate = this.startDatePeriod1['value'];
    }
    this.storageService.setItem('chartStartDate', startDate);
    this.storageService.setItem('chartEndDate', endDate);

    this.changeChartData(startDate, endDate);
  }

  getData() {

    this.operationService.getVehiclesCount().subscribe((response: Array<object>) => {
      this.count = response.length;
    });

  }

  changeChartData(startDate, endDate) {
    try {
      this.spinner.show();
      const chartNew: GoogleChartInterface = {
        chartType: 'ScatterChart',
        dataTable: [],
        options: {
          height: 400,
          chartArea: {width: '80%', height: '60%'},
          theme: 'material',
          hAxis: {
            title: '', maxValue: 1, gridlines: {
              count: -1
            }
          },
          vAxis: {title: ''},
          crosshair: {trigger: 'both'}
        }
      };
      const harvest = this.storageService.getItem('chartScatterHarvest');
      const params = {
        startDate: this.storageService.getItem('globalSearchStartDate'),
        endDate: this.storageService.getItem('globalSearchEndDate'),
        search: this.storageService.getItem('globalSearch'),
        harvest: harvest !== null && harvest !== '' ? harvest.toString() : '',
        xVar: 'ranking_score',
        yVar: 'payment_score',
        xVarLabel: this.paramYLabel !== '' ? this.paramYLabel : 'Puntaje Operativo',
        yVarLabel: 'Dias en deuda',
        filterY: this.paramY !== '' ? true : false,
        filterYName: this.paramY

      };

      this.yVar = params.yVarLabel;
      this.xVar = params.xVarLabel;

      chartNew.options['hAxis']['title'] = params.xVarLabel;
      chartNew.options['vAxis']['title'] = params.yVarLabel;
      const url = `${environment.urlScore}get-scatter-chart`;
      this._DriverService.getChartData(url, params).subscribe(async (response: any) => {
        this.selected = (response.data.length - 1);
        if (response.data.length === 1) {
          response.data.push([0, 0]);
        }
        chartNew.dataTable = response.data;
        response.data[0][1] = 'Creditos';
        this.contracts = response.resultFormatedContract;
        const ccComponent = this.chartCompare.component;
        if (typeof ccComponent !== 'undefined') {
          this.chartCompare = chartNew;
          ccComponent.draw();
        }
        this.spinner.hide();
      }, (err) => {
        this.spinner.hide();
      });
    } catch (e) {
      this.spinner.hide();
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  setValueParamY(label, value, type) {
    this.paramY = value;
    this.paramYLabel = label;
    if (type === 'productive') {
      this.behaviour = '';
      this.care = '';
    }
    if (type === 'behaviour') {
      this.productive = '';
      this.care = '';
    }
    if (type === 'care') {
      this.behaviour = '';
      this.productive = '';
    }
    this.resetChart();
  }

  reset() {
    this.paramY = '';
    this.paramYLabel = '';
    this.behaviour = '';
    this.productive = '';
    this.care = '';
    this.resetChart();

  }

  fixOption(option) {
    if (option === 'none') {
      this.harvest.setValue([]);
      this.resetChart();
    }
  }

  public select(event: ChartSelectEvent) {
    this.contracts.map((value, index) => {
      if (index === (event.row + 1)) {
        const param = {
          width: '70vw',
          data: value,
          backdropClass: 'contract-call'
        };
        this.contractInfoSmall = this.dialog.open(ContractInfoSmallComponent, param);
      }
    });
  }

}
