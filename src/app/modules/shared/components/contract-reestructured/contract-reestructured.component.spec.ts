import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractReestructuredComponent } from './contract-reestructured.component';

describe('ContractReestructuredComponent', () => {
  let component: ContractReestructuredComponent;
  let fixture: ComponentFixture<ContractReestructuredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractReestructuredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractReestructuredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
