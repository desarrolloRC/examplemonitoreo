import { Component, OnInit } from '@angular/core';
import {DriverCommonStructure} from '../../../operation/definitions/DriverCommonStructure';
import {environment} from '../../../../../environments/environment';
import { PermissionsService } from 'src/app/modules/system/services/permissions.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-vehicles-home',
  templateUrl: './vehicles-home.component.html',
  styleUrls: ['./vehicles-home.component.css']
})
export class VehiclesHomeComponent implements OnInit {
  public scoreMicroService = environment.urlScore;
  public attributeLabels;

  constructor(
    public _PermissionService: PermissionsService,
    private _location: Router,) {
  }

  ngOnInit() {
    this.attributeLabels = DriverCommonStructure.VehiclesHomeAttributeLabels;
    if (!this._PermissionService.canAccess('menu_vehicle')) {
      this._location.navigate(['/']).then();
    }
  }

}
