import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesInRiskComponent } from './vehicles-in-risk.component';

describe('VehiclesInRiskComponent', () => {
  let component: VehiclesInRiskComponent;
  let fixture: ComponentFixture<VehiclesInRiskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiclesInRiskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesInRiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
