import { Component, OnInit } from '@angular/core';

import {DriverService} from '../../services/driver.service';
import {StorageService} from '../../services/storage.service';
import {environment} from '../../../../../environments/environment';
import {DriverCommonStructure} from '../../../operation/definitions/DriverCommonStructure';
import {ScoreService} from '../../services/score.service';

@Component({
  selector: 'app-vehicles-in-risk',
  templateUrl: './vehicles-in-risk.component.html',
  styleUrls: ['./vehicles-in-risk.component.css']
})
export class VehiclesInRiskComponent implements OnInit {
  public attributeLabels: object[];
  public scoreMicroService = environment.urlScore;

  constructor(
    private _DriverService: DriverService,
    private _ScoreService: ScoreService,
    private storageService: StorageService

  ) { }

  public vehiclesInRisk = '00';
  public titleCard = 'Vehículos en alto riesgo de deterioro';
  public showNumber = true;

  ngOnInit() {
      this.attributeLabels = DriverCommonStructure.scoreAttributeLabels;
      this.getVehiclesInRisk();
      this.storageService.watchStorage().subscribe((data: string) => {
        const parts = data.split('|');
        if (parts[2] === 'globalSearch' 
        || parts[2] === 'globalSearchStartDate' 
        || parts[2] === 'globalSearchEndDate' 
        || parts[2] === 'chartScatterHarvest'
        ) {
            this.getVehiclesInRisk();
        }
      });
      this.updateTitle();
  }

  getVehiclesInRisk() {
    this._ScoreService.getVehiclesInRisk().subscribe((result: any) => {
      if (typeof result !== 'undefined' && result.length > 0) {
        const valueStr = String(result[0]['count']);
        this.vehiclesInRisk = parseInt(result[0]['count'].toString(), 0) < 10 ? `0${valueStr}` : valueStr;
      } else {
        this.vehiclesInRisk = '00';
      }
      this.updateTitle();
    });
  }

  updateTitle() {
    const global = this.storageService.getItem('globalSearch');
    if (global !== null && global !== '') {
      this.showNumber = false;
      if (this.vehiclesInRisk === '00') {
        this.titleCard = 'Este vehículo no está en riesgo de deterioro';
      } else {
        this.titleCard = 'Este vehículo está en riesgo de deterioro';
      }
    } else {

      this.showNumber = true;
      this.titleCard = 'Vehículos en alto riesgo de deterioro';
    }
  }


}
