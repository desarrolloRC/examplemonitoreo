import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverAssistAverageComponent } from './driver-assist-average.component';

describe('DriverAssistAverageComponent', () => {
  let component: DriverAssistAverageComponent;
  let fixture: ComponentFixture<DriverAssistAverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverAssistAverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverAssistAverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
