import { Component, OnInit } from '@angular/core';
import {ParamsAndId} from '../../interfaces/SharedInterface';
import {OperationDashboardService} from '../../../operation/services/operation-dashboard.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {StorageService} from '../../services/storage.service';
import {TablesStructure} from '../../../risk/definitions/TablesStructure';

@Component({
  selector: 'app-driver-assist-average',
  templateUrl: './driver-assist-average.component.html',
  styleUrls: ['./driver-assist-average.component.css']
})
export class DriverAssistAverageComponent implements OnInit {

  constructor(
    private operationDashboard: OperationDashboardService,
    private spinner: NgxSpinnerService,
    private storageService: StorageService) { }

  public maintenances = 0;
  public totalMaintenances = 0;

  ngOnInit() {
    this.getDataGauge();
    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[2] === 'globalSearch' 
      || parts[2] === 'globalSearchStartDate' 
      || parts[2] === 'globalSearchEndDate' 
      || parts[2] === 'chartScatterHarvest'
      ) {
          this.getDataGauge();
      }
    });
  }

  getDataGauge() {
    const startDate = this.storageService.getItem('globalSearchStartDate');
    const endDate = this.storageService.getItem('globalSearchEndDate');
    const search = this.storageService.getItem('globalSearch');

    const params: ParamsAndId = {
      id: null,
      startDate: startDate,
      endDate: endDate,
      search: search,
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };
    this.spinner.show();
    this.operationDashboard.getPreventivesByType(params).subscribe((response: any) => {
      if (response.length > 0) {
        let on_day = 0;
        let no_day = 0;
        response.map((value) => {
          if (value.type === 'Al dia') {
            on_day = on_day + parseInt(value.count);
          } else {
            no_day = no_day + parseInt(value.count);
          }
          // this.maintenances = on_day;
          // this.totalMaintenances = on_day + no_day;
          this.maintenances = 0;
          this.totalMaintenances = 0;
        });
      } else {
        this.maintenances = 0;
        this.totalMaintenances = 0;
      }
      this.spinner.hide();
    });
  }

}
