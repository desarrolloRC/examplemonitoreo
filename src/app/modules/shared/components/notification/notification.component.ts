import {Component, Input, OnInit} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {StorageService} from '../../services/storage.service';
import {FirebaseHelper} from '../../../../helpers/FirebaseHelper';
import {CustomerDetailComponent} from '../customer-detail/customer-detail.component';
import {MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  @Input() data = {};
  public customerDetailModal: MatDialogRef<CustomerDetailComponent>;
  public notifications;

  constructor(
    private dialog: MatDialog,
    private storage: StorageService,
    private db: AngularFireDatabase
  ) {}

  ngOnInit() {
    this.getNotifications();

    this.storage.watchStorage().subscribe((data) => {
      const parts = data.split('|');
      if (parts[2] === 'notifications') {
        this.getNotifications();
      }
    });
  }

  public getNotifications() {
    this.notifications = [];
    const notificationsArray: any = JSON.parse(localStorage.getItem('notifications'));
    if (notificationsArray !== null && notificationsArray.length > 0) {
      notificationsArray.forEach((person) => {
        this.notifications.push(person);
        /*for (const i in person) {
          if (typeof person[i] === 'object') {
            this.notifications.push(person[i]);
          }
        }*/
      });
    }
  }

  public deleteNotification(item) {
    const databaseUrl = FirebaseHelper.getDatabase();
    if (typeof databaseUrl !== 'undefined' && databaseUrl !== null) {
      const itemsRef = this.db.list(databaseUrl);
      itemsRef.remove(item);
      const elem = document.getElementById(item);
      elem.parentNode.removeChild(elem);
    }
  }

  public showDetail(item) {
    const param = {
      minWidth: '70vw',
      height: '100vh',
      data: item,
      backdropClass: 'container-call'
    };
    this.customerDetailModal = this.dialog.open(CustomerDetailComponent, param);
  }

}
