import {Component, OnInit} from '@angular/core';

import {StorageService} from '../../services/storage.service';
import {OperationDashboardService} from '../../../operation/services/operation-dashboard.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-travel-dashboard',
  templateUrl: './travel-dashboard.component.html',
  styleUrls: ['./travel-dashboard.component.css']
})
export class TravelDashboardComponent implements OnInit {
  public distanceAverage = 0;

  constructor(private operationDashboardService: OperationDashboardService,
              private spinner: NgxSpinnerService,
              private storageService: StorageService) {
  }
  public titleCard = 'Promedio recorrido por los vehículos';

  ngOnInit() {
    this.getAverage();

    let global = this.storageService.getItem('globalSearch');
    this.storageService.watchStorage().subscribe((data) => {
      this.getAverage();
      const parts = data.split('|');
      if (parts[2] === 'globalSearch') {
        global = this.storageService.getItem('globalSearch');
        if (global !== null && global !== '') {
          this.titleCard = 'Promedio recorrido por ese vehículo';
        } else {
          this.titleCard = 'Promedio recorrido por los vehículos';
        }

      }
    });

    if (global !== null && global !== '') {
      this.titleCard = 'Promedio recorrido por ese vehículo';
    } else {
      this.titleCard = 'Promedio recorrido por los vehículos';
    }

  }

  public getAverage() {
    this.spinner.show();
    this.operationDashboardService.getVehicleDistanceAvg().subscribe((response) => {
      if (typeof response['data'] !== 'undefined') {
        if (response['data']['average'][0]['avg'] !== null) {
          this.distanceAverage = Math.round(response['data']['average'][0]['avg'] / 1000);
        } else {
          this.distanceAverage = 0;
        }
      } else {
        this.distanceAverage = 0;
      }
      this.spinner.hide();
    });
  }
}
