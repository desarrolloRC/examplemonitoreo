import {Component, OnInit, ViewChild} from '@angular/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import * as moment from 'moment';

import {StorageService} from '../../services/storage.service';
import {DashboardService} from '../../services/dashboard.service';
import {GlobalSearch, SimpleValue} from '../../interfaces/SharedInterface';
import {LoginService} from 'src/app/modules/system/services/login.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-form-global',
  templateUrl: './form-global.component.html',
  styleUrls: ['./form-global.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})

export class FormGlobalComponent implements OnInit {
  public globalForm: FormGroup;
  public harvest = new FormControl();
  public harvestList = [];
  public harvestSelect = [];

  public currentSelected = '';
  myControl = new FormControl();
  thirtyDaysBehind = new Date();
  IsothirtyDaysBehind = this.thirtyDaysBehind.setDate(this.thirtyDaysBehind.getDate() - 30);
  startDate = new FormControl(new Date(this.IsothirtyDaysBehind));
  endDate = new FormControl(new Date());
  startDateMax = new Date();
  endDateMin = new Date(2000, 0, 1);
  public minDateContract;

  optionsGlobal: GlobalSearch[];
  optionsGlobalEmpty: GlobalSearch[];

  @ViewChild('optionGlobal') optionGlobal;

  constructor(private _formBuilder: FormBuilder,
              private _dashboardService: DashboardService,
              private _LoginService: LoginService,
              private storageService: StorageService) {

    const harvestStorage = this.storageService.getItem('harvest');

    if (harvestStorage !== null) {
      const harvestSplit = this.storageService.getItem('harvest').split(',');

      if (typeof harvestSplit !== 'undefined' && harvestSplit.length > 0 && harvestSplit[0] !== '') {
        const harvestArray = [];
        harvestSplit.map((value) => {
          harvestArray.push({
            id: parseInt(value, 10),
            value: `${parseInt(value, 10)} - ${moment().subtract(parseInt(value, 10), 'months').format('MM YYYY')}`
          });
        });
        this.harvestList = harvestArray;
      }
    }

    const harvest = this.storageService.getItem('chartScatterHarvest');

    if (harvest !== null) {
      const harvestSplit = this.storageService.getItem('chartScatterHarvest').split(',');

      if (typeof harvestSplit !== 'undefined' && harvestSplit.length > 0 && harvestSplit[0] !== '') {
        const harvestArray = [];
        harvestSplit.map((value) => {
          harvestArray.push(parseInt(value, 10));
        });
        this.harvest.setValue(harvestArray);
      }
    }
  }

  ngOnInit(): void {
    this.getFirstDateContract();
    const dateStorageStart = this.storageService.getItem('globalSearchStartDate');
    const dateStorageEnd = this.storageService.getItem('globalSearchEndDate');
    const dateStorageSearch = this.storageService.getItem('globalSearch');

    if (typeof dateStorageStart === 'undefined' || dateStorageStart === null || dateStorageStart === '') {
      const date = new Date(this.IsothirtyDaysBehind);
      const day = date.getDate();
      const year = date.getFullYear();
      const month = date.getMonth() + 1;
      this.storageService.setItem('globalSearchStartDate', year + '-' + month + '-' + day);
    } else {
      this.startDate = new FormControl(new Date(dateStorageStart));
    }

    if (typeof dateStorageEnd === 'undefined' || dateStorageEnd === null || dateStorageEnd === '') {
      const date = new Date();
      const day = date.getDate();
      const year = date.getFullYear();
      const month = date.getMonth() + 1;
      this.storageService.setItem('globalSearchEndDate', year + '-' + month + '-' + day);
    } else {
      this.endDate = new FormControl(new Date(dateStorageEnd));
    }

    if (typeof dateStorageSearch === 'undefined' || dateStorageSearch === null || dateStorageSearch === '') {
      this.storageService.setItem('globalSearch', '');
    } else {
      this.storageService.setItem('globalSearch', dateStorageSearch);
    }

    this.initGlobalSearch();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');

      if (parts[2] === 'globalSearch') {
        if (this.storageService.getItem('globalSearch') !== '') {
          const object = JSON.parse(this.storageService.getItem('globalSearch'));
          this.currentSelected = object['label'];
        }
      }
    });
  }

  initGlobalSearch() {
    this.globalForm = this._formBuilder.group({
      searchGlobal: [null],
    });
    this.myControl.valueChanges
      .subscribe(
        (value) => {
          if (typeof value !== 'undefined' && value !== '') {
            if (typeof value === 'string') {
              const searchValue: SimpleValue = {value: value};
              this._dashboardService.getPlatesPeople(searchValue).subscribe((response: GlobalSearch[]) => {
                this.optionsGlobal = response;
              });
            } else if (typeof value === 'object') {
              this.storageService.setItem('globalSearch', JSON.stringify(value));
            }
          } else {
            this.storageService.setItem('globalSearch', '');
            this.optionsGlobal = this.optionsGlobalEmpty;
          }
        }
      );

    this.startDate.valueChanges
      .subscribe(
        (value) => {
          const startDate = new Date(this.startDate.value);
          const endDate = new Date(this.endDate.value);
          const diffTime = Math.abs(endDate.getTime() - startDate.getTime());
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

          const date = new Date(value);
          this.endDateMin = value;
          const day = date.getDate();
          const year = date.getFullYear();
          const month = date.getMonth() + 1;
          this.storageService.setItem('globalSearchStartDate', year + '-' + month + '-' + day);

        }
      );
    this.endDate.valueChanges
      .subscribe(
        (value) => {
          const startDate = new Date(this.startDate.value);
          const endDate = new Date(this.endDate.value);
          const diffTime = Math.abs(endDate.getTime() - startDate.getTime());
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

          const date = new Date(value);
          const day = date.getDate();
          const year = date.getFullYear();
          const month = date.getMonth() + 1;
          this.storageService.setItem('globalSearchEndDate', year + '-' + month + '-' + day);


        }
      );
      /*
    this.harvest.valueChanges
      .subscribe(
        (value) => {
          if (value.length > 0) {
            value = this.cleanHarvest(value);
            if (value[0] === '') {
              this.storageService.setItem('chartScatterHarvest', '');
            } else {
              this.storageService.setItem('chartScatterHarvest', value);
            }
          } else {
            this.storageService.setItem('chartScatterHarvest', '');
          }

        }
      );*/

  }

  applyFilter() {
    const startDate = this.startDate.value;
    const endDate = this.endDate.value;
    const myControl = this.myControl.value;
    let harvest = this.harvest.value;

    //StartDate
    const previousStartDate = this.storageService.getItem('globalSearchStartDate');
    let date = new Date(startDate);
    this.endDateMin = startDate;
    let day = date.getDate();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    const newStartDate = year + '-' + month + '-' + day;
    if (previousStartDate !== newStartDate) {
      this.storageService.setItem('globalSearchStartDate', newStartDate);
    }

    //EndDate
    const previousEndDate = this.storageService.getItem('globalSearchEndDate');
    date = new Date(endDate);
    day = date.getDate();
    year = date.getFullYear();
    month = date.getMonth() + 1;
    const newEndDate = year + '-' + month + '-' + day;
    if (previousEndDate !== newEndDate) {
      this.storageService.setItem('globalSearchEndDate', newEndDate);
    }

    //GlobalSearch
    const previousGlobalSearch = this.storageService.getItem('globalSearch');
    let newGlobalSearch = '';
    if (typeof myControl !== 'undefined' && myControl !== '' && myControl !== null) {
      if (typeof myControl === 'string') {
        const searchValue: SimpleValue = {value: myControl};
        this._dashboardService.getPlatesPeople(searchValue).subscribe((response: GlobalSearch[]) => {
          this.optionsGlobal = response;
        });
      } else if (typeof myControl === 'object') {
        newGlobalSearch = JSON.stringify(myControl);
        if (previousGlobalSearch !== newGlobalSearch) {
          this.storageService.setItem('globalSearch', newGlobalSearch);
        }
      }
    } else {
      newGlobalSearch = '';
      if (previousGlobalSearch !== newGlobalSearch) {
        this.storageService.setItem('globalSearch', newGlobalSearch);
      }
      this.optionsGlobal = this.optionsGlobalEmpty;
    }

    //Harvest
    let previousHarvest = this.storageService.getItem('chartScatterHarvest');
    let newHarvest = [];
    let newHarvestString = '';
    if (harvest.length > 0) {
      harvest = this.cleanHarvest(harvest);
      if (harvest[0] === '') {
        newHarvest = [];
        newHarvestString = newHarvest.join(',');
        if (previousHarvest !== newHarvestString) {
          this.storageService.setItem('chartScatterHarvest', newHarvest);
        }
      } else {
        newHarvest = harvest;
        newHarvestString = newHarvest.join(',');
        if (previousHarvest !== newHarvestString) {
          this.storageService.setItem('chartScatterHarvest', newHarvest);
        }
      }
    } else {
      newHarvest = [];
      newHarvestString = newHarvest.join(',');
      if (previousHarvest !== newHarvestString) {
        this.storageService.setItem('chartScatterHarvest', newHarvest);
      }
    }

  }

  displayFn(option?: GlobalSearch): string | undefined {
    return option ? option.label : undefined;
  }

  clearSearch() {
    this.storageService.setItem('globalSearch', '');
    this.optionsGlobal = this.optionsGlobalEmpty;
    this.currentSelected = '';

  }

  cleanHarvest(harvest) {
    const arrayFinal = [];
    harvest.map((value) => {
      if (typeof value === 'number') {
        arrayFinal.push(value);
      }
    });
    return arrayFinal;
  }

  fixOption(event) {
    const option = event.value;
    const temp = [];
    let band = false;
    for (let i = 0; i < option.length; i++) {
      if (this.harvestSelect[i]) {
        if (option[i] !== this.harvestSelect[i]) {
          if (option[i] === '0') {
            this.harvest.patchValue(['0', ...this.harvestList.map(item => item.id), 0]);
            band = false;
            break;
          } else {
            temp[i] = option[i];
            band = true;
          }
        } else {
          if (option[i] !== '0') {
            temp[i] = option[i];
            band = true;
          }
        }
      } else {
        if (option[i] === '0') {
          this.harvest.patchValue(['0', ...this.harvestList.map(item => item.id), 0]);
          band = false;
          break;
        } else {
          temp[i] = option[i];
          band = true;
        }
      }
    }
    if (band) {
      this.harvest.patchValue(temp);
    }
    this.harvestSelect = option;
  }

  getFirstDateContract() {
    this._LoginService.getFirstDateContract().subscribe((response: any) => {
        this.minDateContract = (response.length > 0) ? new Date(response[0]['date_start'].replace('.000Z', '')) : new Date();
    });
  }
}
