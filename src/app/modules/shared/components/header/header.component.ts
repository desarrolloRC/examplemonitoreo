import {FormControl} from '@angular/forms';
import {MediaMatcher} from '@angular/cdk/layout';
import {Component, ChangeDetectorRef, OnInit, ViewChild} from '@angular/core';

import {StorageService} from '../../services/storage.service';
import {DashboardService} from '../../services/dashboard.service';
import {MatDrawer} from '@angular/material';
import {JwtHelper} from '../../../../helpers/JwtHelper';
import {LoginService} from '../../../system/services/login.service';
import {I18nHelper} from '../../../../helpers/I18nHelper';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  public title;
  public img;
  @ViewChild('drawer') drawer: MatDrawer;
  @ViewChild('notifications') notifications: MatDrawer;
  public navBarStatus = 'chevron_left';
  startDate = new FormControl(new Date());
  endDate = new FormControl(new Date());
  public name;

  constructor(
    private loginService: LoginService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private _dashboardService: DashboardService,
    private storageService: StorageService,
    private translate: TranslateService
  ) {
    const token = sessionStorage.getItem('token');
    if (token !== null && typeof token !== 'undefined') {
      const srcImg = JwtHelper.getImgUrl(token);
      if (srcImg !== null && typeof srcImg !== 'undefined') {
        this.img = srcImg;
      } else {
        this.img = '';
      }
    }
  }

  ngOnInit(): void {
    this.getPersonalInfo();
    this.getHarvest();
    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[2] === 'title') {
        this.title = parts[1];
      }
    });

    this.storageService.setItem('globalSearch', '');
  }

  isNotLoggedIn = () => {
    const token = sessionStorage.getItem('token');
    return token === null || typeof token === 'undefined';
  }

  public changeNav(event) {
    this.navBarStatus = event ? 'chevron_right' : 'chevron_left';
  }

  closeDrawerParent() {
    return this.drawer.close();
  }

  getPersonalInfo() {
    if (!this.isNotLoggedIn()) {
      this.loginService.getPersonalInfo().subscribe((response: any) => {
        this.name = `${response['Name']} ${response['LastName']}`;
      });
    }
  }

  getHarvest() {
    if (!this.isNotLoggedIn()) {
      this.loginService.getHarvest().subscribe((response: any) => {
        let harvest = [];
        response.map((value, index) => {
          harvest.push(parseInt(value['harvest'], 0))
        });
        this.storageService.setItem('harvest', harvest);
      });
    }
  }
}
