import * as XLSX from 'xlsx';
import {merge, of as observableOf} from 'rxjs';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Component, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

import {TableService} from '../../services/table.service';
import {DriverService} from '../../services/driver.service';
import {StorageService} from '../../services/storage.service';

import {
  GetById,
  GlobalSearchParams,
} from '../../interfaces/SharedInterface';
import {AlertUpdateComponent} from '../../../risk/components/alert-update/alert-update.component';
import {MatTooltip} from '@angular/material/tooltip';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers: [TableService]
})
export class TableComponent implements OnInit {
  @Input() columns;
  @Input() url: string;
  @Input() urlExcel: string;
  @Input() pageSize;
  @Input() tableName = '';
  @Input() centerText = 'centerText';
  @Input() hideButtons = true;
  @Input() microService: string;
  @Input() hidePagination = true;
  @Input() refreshOnFilter = false;
  @Input() pageSizeOptions = [5, 10, 15, 25, 50];
  @Input() typeAlert: string;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public globalSearch: GlobalSearchParams = {
    globalInput: '',
    globalInputEndDate: this.currentDate(),
    globalInputStartDate: this.currentDate()
  };

  public dataSource;
  public resultsLength = 0;
  public isLoadingResults = true;
  public isRateLimitReached = false;
  public displayedColumns: string[];

  constructor(private storageService: StorageService,
              private _tableService: TableService,
              private translate: TranslateService,
              private _driverService: DriverService,
              public dialog: MatDialog) {

  }

  ngOnInit(): void {
    if (!this.refreshOnFilter) {
      this.storageService.watchStorage().subscribe((data: string) => {
        const parts = data.split('|');

        if (parts[2] === 'globalSearch'
        || parts[2] === 'globalSearchStartDate'
        || parts[2] === 'globalSearchEndDate'
        || parts[2] === 'chartScatterHarvest'
        ) {
          this.globalSearch.globalInput = this.storageService.getItem('globalSearch');
          this.globalSearch.globalInputStartDate = this.storageService.getItem('globalSearchStartDate');
          this.globalSearch.globalInputEndDate = this.storageService.getItem('globalSearchEndDate');
          this.getData();
        } else if (parts[2] === 'typeInstallment') {
          this.getData();
        }
      });
    }
    this.displayedColumns = this.columns.map(column => column.name);
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.getData();
  }

  public getData() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;

          this.pageSize = typeof this.paginator.pageSize !== 'undefined' ? this.paginator.pageSize : this.pageSize;

          return this._tableService.requestData(
            this.microService,
            this.url,
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex,
            this.pageSize
          );
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;

          this.resultsLength = data['totalElements'];

          return data['content'];
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;

          return observableOf([]);
        })
      ).subscribe(data => {
      data.map(value => {
        if (typeof value['city_id'] !== 'undefined') {
          const params: GetById = {
            id: value['city_id']
          };

          value['city_id'] = '';
          this._driverService.getCityById(params).subscribe(response => value['city_id'] = response[0]['label']);
        }
        if (typeof value['vehicle_id'] !== 'undefined') {
          const id = value['vehicle_id'];
          const params: GetById = {
            id: value['vehicle_id']
          };
          value['vehicle_id'] = '';

          this._driverService.getPlateById(params).subscribe(response => value['vehicle_id'] = `${response[0]['plate']}|${id}|${response[0]['number']}`);
        }
        if (typeof value['owner_contract_id'] !== 'undefined') {
          const id = value['owner_contract_id'];
          const params: Object = {
            ownerContractId: value['owner_contract_id']
          };
          value['owner_contract_id'] = '';
          this._driverService.getVehicleByContractId(params).subscribe((response) => {
            value['owner_contract_id'] = `${response[0]['plate']}|${response[0]['id']}|${response[0]['number']}`
            this._driverService.getLastLocation(response[0]['plate']).subscribe((responseLocation: any) => {
              if (responseLocation.length > 0) {
                value['owner_contract_id'] = `${value['owner_contract_id']}|${responseLocation[0]['date']}`
              } else {
                value['owner_contract_id'] = `${value['owner_contract_id']}|-1`
              }
            });
          });
        }
        if (typeof value['plate_id'] !== 'undefined') {
          const plate = value['plate_id'];
          const params: Object = {
            plate: value['plate_id']
          };
          value['plate_id'] = '';
          this._driverService.getIdByPlate(params).subscribe(response => value['plate_id'] = `${plate}|${response[0]['id']}|${response[0]['number']}`);
        }
      });
      this.dataSource = data;
    });
  }

  currentDate() {
    const date = new Date();
    const day = date.getDate();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    return year + '-' + month + '-' + day;
  }

  colorClass(row, column) {
    let classColor = '';

    if (row[column.name] === 'Al dia') {
      classColor = 'on_day';
    } else if (row[column.name] === 'Sin Info') {
      classColor = 'no_info';
    } else if (row[column.name] === 'No asistio') {
      classColor = 'no_go';
    }

    return classColor;
  }

  downloadExcel() {
    this._tableService.requestDataExcel(
      this.microService,
      this.urlExcel
    ).subscribe(data => {

      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      XLSX.writeFile(wb, 'SheetJS.xlsx');
    });
  }

  @HostListener('click', ['$event'])
  onClick(e) {
    if (e.target.classList.contains('globalActivateSearch')) {
      const arrayData = e.target.getAttribute('data-id').split('|');
      const data: Object = {
        id: arrayData[0],
        label: `${arrayData[2]} - ${arrayData[1]} (Vehículo)`,
        type: 'vehicle',
      };
      this.storageService.setItem('globalSearch', JSON.stringify(data));
    }
  }

  openDialogUpdate() {
    const type = this.typeAlert;
    if (typeof type !== 'undefined') {
      const dialogRef = this.dialog.open(AlertUpdateComponent, {
        panelClass: 'custom-modalbox-update-payment',
        data: {type: type}
      });
    }
  }

}
