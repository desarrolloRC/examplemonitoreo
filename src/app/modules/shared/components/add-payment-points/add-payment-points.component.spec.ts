import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPaymentPointsComponent } from './add-payment-points.component';

describe('AddPaymentPointsComponent', () => {
  let component: AddPaymentPointsComponent;
  let fixture: ComponentFixture<AddPaymentPointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPaymentPointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPaymentPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
