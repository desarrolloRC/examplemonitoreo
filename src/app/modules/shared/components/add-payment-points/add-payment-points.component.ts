import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import {Router} from '@angular/router';
import * as momentTZ from 'moment-timezone';
import {NgxSpinnerService} from 'ngx-spinner';

import { DashboardService } from '../../services/dashboard.service';
import {DriverService} from '../../../shared/services/driver.service';
import {PermissionsService} from '../../../system/services/permissions.service';

@Component({
  selector: 'app-add-payment-points',
  templateUrl: './add-payment-points.component.html',
  styleUrls: ['./add-payment-points.component.css']
})
export class AddPaymentPointsComponent implements OnInit {

  constructor(
    private _location: Router,
    private spinner: NgxSpinnerService,
    private _DrivingService: DriverService,
    private _dashboardService: DashboardService,
    public _PermissionService: PermissionsService
  ) { }

  ngOnInit() {
    if (!this._PermissionService.canAccess('new_contract')) {
      this._location.navigate(['/']).then();
    }
  }

  data: any;
  dataFinal: object[] = [];
  timeZone;
  validData = true;

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <any>(XLSX.utils.sheet_to_json(ws, {header: 1, raw: false}));
      this.proccessFile();

    };
    reader.readAsBinaryString(target.files[0]);
  }

  proccessFile () {
    this.dataFinal = [];
    this.timeZone = 'UTC';
    this.data.map((value) => {
      if (value[0] !== 'CREDITO') {
        console.log(value);
        let valid = false;
        const data = {
          contractNumber: value[0],
          paymentScore: value[1],
          date: this.DateToJSDate(value[2]),
        };
        this.dataFinal.push(data);
      }
    });
  }

  DateToJSDate(dateSent) {
    const date = moment(dateSent,
      ['YYYY-MM-DD','D/MM/YYYY', 'DD/MM/YYYY', 'D/M/YYYY'], false);
    if (date.isValid()) {
      return date.format('YYYY-MM-DD');
    } else {
      return null;
    }
  }

  ExcelDateToJSDate(serial) {
    const date = new Date((serial - (25567 + 1))*86400*1000);
    return `${date.getFullYear()}-${(date.getMonth() + 1)}-${date.getDate()}`
 }

  savePoints() {
    this.spinner.show();
    const params = {
      data: this.dataFinal
    };
    this._dashboardService.savePaymentScore(params).subscribe((response) => {
      this.spinner.hide();
      this._location.navigate(['/']).then();
    });
  }

}
