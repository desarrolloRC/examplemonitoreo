import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderButttonsComponent } from './header-butttons.component';

describe('HeaderButttonsComponent', () => {
  let component: HeaderButttonsComponent;
  let fixture: ComponentFixture<HeaderButttonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderButttonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderButttonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
