import {Component, OnInit} from '@angular/core';
import {StorageService} from '../../services/storage.service';
import {PermissionsService} from '../../../system/services/permissions.service';

@Component({
  selector: 'app-header-butttons',
  templateUrl: './header-butttons.component.html',
  styleUrls: ['./header-butttons.component.css']
})
export class HeaderButttonsComponent implements OnInit {
  public notifications = [];

  constructor(
    private storage: StorageService,
    public _PermissionService: PermissionsService,
  ) {
  }

  ngOnInit() {
    this.getNotifications();

    this.storage.watchStorage().subscribe(items => {
      this.getNotifications();
    });
  }

  public getNotifications() {
    const notificationsArray = JSON.parse(localStorage.getItem('notifications'));
    this.notifications = [];
    if (notificationsArray !== null && notificationsArray.length > 0) {
      notificationsArray.forEach((item, index) => {
        if (index < 5) {
          this.notifications.push(item);
        }
      });
    } else {
      this.notifications = [{'title': '', 'date': ''}];
    }
  }
}
