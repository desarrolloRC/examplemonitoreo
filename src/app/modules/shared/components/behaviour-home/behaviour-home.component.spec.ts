import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BehaviourHomeComponent } from './behaviour-home.component';

describe('BehaviourHomeComponent', () => {
  let component: BehaviourHomeComponent;
  let fixture: ComponentFixture<BehaviourHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BehaviourHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BehaviourHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
