import {Component, OnInit} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DriverCommonStructure} from '../../../operation/definitions/DriverCommonStructure';
import {PermissionsService} from 'src/app/modules/system/services/permissions.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {I18nHelper} from '../../../../helpers/I18nHelper';
import {StorageService} from '../../services/storage.service';

@Component({
  selector: 'app-behaviour-home',
  templateUrl: './behaviour-home.component.html',
  styleUrls: ['./behaviour-home.component.css']
})
export class BehaviourHomeComponent implements OnInit {
  public scoreMicroService = environment.urlScore;
  public attributeLabels;
  public typesAvailable = [
    {name: 'ticketsPending', label: I18nHelper.getTranslate(this.translate, this.storage, 'ticket.pending'),
      title: I18nHelper.getTranslate(this.translate, this.storage, 'home.tickets_pending')},
    {name: 'documentsExpired', label: 'Documentos vencidos', title: '# de documentos'},
    {name: 'documentsAboutToExpire', label: 'Documentos en riesgo', title: '# de documentos'},
    {name: 'timesBeyond', label: 'Velocidad alta', title: '# de veces por encima del limite'},
    {name: 'highKilometers', label: 'Kilometraje alto', title: 'Kilometros recorridos promedio'}
  ];

  constructor(
    public _PermissionService: PermissionsService,
    private storage: StorageService,
    private _location: Router,
    private translate: TranslateService) {
  }

  ngOnInit() {
    this.attributeLabels = DriverCommonStructure.BehaviourHomeAttributeLabels;
    if (!this._PermissionService.canAccess('menu_behaviour')) {
      this._location.navigate(['/']).then();
    }
  }

}
