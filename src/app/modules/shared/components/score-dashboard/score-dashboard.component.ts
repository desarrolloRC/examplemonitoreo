import { Component, OnInit } from '@angular/core';
import {StorageService} from '../../services/storage.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-score-dashboard',
  templateUrl: './score-dashboard.component.html',
  styleUrls: ['./score-dashboard.component.css']
})
export class ScoreDashboardComponent implements OnInit {
  public titleCard = 'Promedio de score de los conductores';
  constructor(
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    ) { }

  ngOnInit() {

    let global = this.storageService.getItem('globalSearch');
    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[2] === 'globalSearch') {
        global = this.storageService.getItem('globalSearch');
        if (global !== null && global !== '') {
          this.titleCard = 'Score actual del conductor de ese vehículo';
        } else {
          this.titleCard = 'Promedio del Score de los conductores';
        }
      }
    });

    if (global !== null && global !== '') {
      this.titleCard = 'Score actual del conductor de ese vehículo';
    } else {
      this.titleCard = 'Promedio del Score de los conductores';
    }
  }

}
