import {Router} from '@angular/router';
import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-nav-component-generator',
  templateUrl: './nav-component-generator.component.html',
  styleUrls: ['./nav-component-generator.component.css']
})
export class NavComponentGeneratorComponent implements OnInit {
  @Input() navLinks;

  public active;

  constructor(private _router: Router) {
  }

  ngOnInit() {
    this.active = this._router.url.split('/');
  }

}
