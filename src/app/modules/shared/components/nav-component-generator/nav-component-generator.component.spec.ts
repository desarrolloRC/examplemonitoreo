import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavComponentGeneratorComponent } from './nav-component-generator.component';

describe('NavComponentGeneratorComponent', () => {
  let component: NavComponentGeneratorComponent;
  let fixture: ComponentFixture<NavComponentGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavComponentGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponentGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
