import { Component, OnInit } from '@angular/core';
import { PermissionsService } from 'src/app/modules/system/services/permissions.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-scatter-home',
  templateUrl: './scatter-home.component.html',
  styleUrls: ['./scatter-home.component.css']
})
export class ScatterHomeComponent implements OnInit {

  constructor(
    public _PermissionService: PermissionsService,
    private _location: Router,) {
  }

  ngOnInit() {
    if (!this._PermissionService.canAccess('menu_scatter')) {
      this._location.navigate(['/']).then();
    }
  }

}
