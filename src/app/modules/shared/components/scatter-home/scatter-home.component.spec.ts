import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScatterHomeComponent } from './scatter-home.component';

describe('ScatterHomeComponent', () => {
  let component: ScatterHomeComponent;
  let fixture: ComponentFixture<ScatterHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScatterHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScatterHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
