import * as XLSX from 'xlsx';
import * as moment from 'moment';
import {Router} from '@angular/router';
import * as momentTZ from 'moment-timezone';
import {NgxSpinnerService} from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import * as PromiseBluebird from 'bluebird';

import { DashboardService } from '../../services/dashboard.service';
import {DriverService} from '../../../shared/services/driver.service';
import {PermissionsService} from '../../../system/services/permissions.service';
import { JwtHelper } from 'src/app/helpers/JwtHelper';

@Component({
  selector: 'app-add-ticket-massive',
  templateUrl: './add-ticket-massive.component.html',
  styleUrls: ['./add-ticket-massive.component.css']
})
export class AddTicketMassiveComponent implements OnInit {

  constructor(
    private _location: Router,
    private spinner: NgxSpinnerService,
    private _DrivingService: DriverService,
    private _dashboardService: DashboardService,
    public _PermissionService: PermissionsService
  ) { }

  data: any;
  dataFinal: object[] = [];
  timeZone;
  validData = true;
  public quantity = 0;
  public amount = 0;
  public limitBatch = 500;
  public massiveLoadHistory = [];

  ngOnInit() {
    if (!this._PermissionService.canAccess('new_contract')) {
      this._location.navigate(['/']).then();
    }
    this.getMassiveLoadHistory();
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <any>(XLSX.utils.sheet_to_json(ws, {header: 1, raw: false}));
      this.proccessFile();

    };
    reader.readAsBinaryString(target.files[0]);
  }

  proccessFile () {
    this.spinner.show();
    this.dataFinal = [];
    this.quantity = 0;
    this.amount = 0;
    let dataBatch = [];
    let count = 0;

    this.timeZone = 'UTC';
    this.data.map((value, index) => {
      if (value[0] !== 'FECHA DESCARGA') {
        if ((typeof value[0] !== 'undefined' && value[0] !== '') && value[1] !== '') {
          if (typeof value[11] !== 'undefined') {
            count ++;
            let valid = false;
            const data = {
              vehicle: {},
              contract: {},
              downloadDate: new Date(this.DateToJSDate(value[0])),
              ticket: {
                id: value[2],
                date: new Date(this.DateToJSDate(value[3])),
                status: value[6],
                type: value[5],
                code: value[4],
                received_by: value[1],
                base_amount: value[7],
                interest: value[10],
                total_amount: value[11].replace(/,/g, ''),
              },
              status: 'success'
            };
            dataBatch.push(data);
            this.quantity ++;
            this.amount += parseFloat(value[11].replace(/,/g, ''));
            if (count === this.limitBatch || (index === this.data.length)) {
              this.dataFinal.push(dataBatch);
              count = 0;
              dataBatch = [];
            }
          }
        }
      }
      if (count !== 0 && count < this.limitBatch && (index + 1) === this.data.length) {
        this.dataFinal.push(dataBatch);
        count = 0;
        dataBatch = [];
      }
    });

    this.spinner.hide();
  }

  DateToJSDate(dateSent) {
    const date = moment(dateSent,
      ['YYYY-MM-DD', 'D/MM/YYYY', 'DD/MM/YYYY', 'D/M/YYYY'], false);
    if (date.isValid()) {
      return date.format('YYYY-MM-DD');
    } else {
      return null;
    }
  }

  ExcelDateToJSDate(serial) {
    const date = new Date((serial - (25567 + 1))*86400*1000);
    return `${date.getFullYear()}-${(date.getMonth() + 1)}-${date.getDate()}`
 }

  saveTickets() {
    
    this.spinner.show();
    const params = {
      count: this.dataFinal.length,
      enterpriseId: JwtHelper.getEnterprise(sessionStorage.getItem('token'))

    };
    this._dashboardService.saveTicketsMassiveBatch(params).subscribe(async (response: any) => {
      const id = response.id;
      await PromiseBluebird.mapSeries(this.dataFinal, async (value, index) => {
        const paramsLoad = {
          id: id,
          data: value
        };
        this._dashboardService.saveTicketsMassive(paramsLoad).subscribe((response: any) => {
          console.log(response);
        })

      }, {concurrency: 1})
      .then(async () => {
        this.spinner.hide();
        alert('Archivo cargado para procesamiento, al terminar aparecera en el historial');
        window.location.reload();
      });
      
    });
  }


  async getMassiveLoadHistory() {
    await this._dashboardService.getMassiveLoadHistory().subscribe((response: any) => {
      console.log(response);
      this.massiveLoadHistory = response['data'];
    });
  }

}
