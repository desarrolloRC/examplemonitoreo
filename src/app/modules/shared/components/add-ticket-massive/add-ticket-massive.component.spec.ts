import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTicketMassiveComponent } from './add-ticket-massive.component';

describe('AddTicketMassiveComponent', () => {
  let component: AddTicketMassiveComponent;
  let fixture: ComponentFixture<AddTicketMassiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTicketMassiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTicketMassiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
