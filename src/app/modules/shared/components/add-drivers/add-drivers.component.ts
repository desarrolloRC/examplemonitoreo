import { Component, OnInit } from '@angular/core';
import { DriverService } from '../../services/driver.service';
import { DashboardService } from '../../services/dashboard.service';
import { PermissionsService } from 'src/app/modules/system/services/permissions.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';
import * as PromiseBluebird from 'bluebird';
import * as moment from 'moment';
import * as moment_timezone from 'moment-timezone';

@Component({
  selector: 'app-add-drivers',
  templateUrl: './add-drivers.component.html',
  styleUrls: ['./add-drivers.component.css']
})
export class AddDriversComponent implements OnInit {
  public total = 0;
  public current = 0;
  public percentage = 0;
  public showProgress = false;

  constructor(
    private _DrivingService: DriverService,
    private _dashboardService: DashboardService,
    public _PermissionService: PermissionsService,
    private spinner: NgxSpinnerService,
    private _location: Router
  ) { }

  data: any;
  dataFinal: object[] = [];
  result: object[] = [];

  ngOnInit() {
    if (!this._PermissionService.canAccess('new_contract')) {
      this._location.navigate(['/']).then();
    }
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <any>(XLSX.utils.sheet_to_json(ws, {header: 1}));
      console.log(this.data);

      this.proccessFile();

    };
    reader.readAsBinaryString(target.files[0]);
  }

  proccessFile () {
    this.dataFinal = [];
    this.result = [];
    this.data.map((value) => {

      if (value[0] !== 'Cedula / DNI' && typeof value[0] !== 'undefined') {
        let sequence;
        if (value[27] !== 'Semanal' && value[27] !== 'Mensual') {
          sequence = 'Mensual';
        } else {
          sequence = value[27];
        }
        const mod = String(value[13]).indexOf('-');
        const data = {
          person: {
            dni: String(value[0]).replace(/ /g, ''),
            name: value[1],
            lastname: value[2],
            telephone: String(value[3]).replace(/[^0-9.]/g, ''),
            mobile: String(value[4]).replace(/[^0-9.]/g, ''),
            email: typeof value[5] === 'undefined' ? `${String(value[0]).replace(/ /g, '')}@` : String(value[5]).replace(/ /g, ''),
            dateOfBirth: this.ExcelDateToJSDate(value[6]),
            gender: value[7],
            ocupation: value[8],
            studyLevel: value[9],
            civilState: value[10],
            role: value[11],
          },
          vehicle: {
            plate: String(value[12]).replace(/ /g, ''),
            model: String(value[13]).substring(mod + 1, String(value[13]).length) + ' - ' + String(value[13]).substring(0, mod),
            engineNumber: typeof value[14] === 'undefined' ? '' : String(value[14]).replace(/ /g, ''),
            chasisNumber: typeof value[15] === 'undefined' ? '' : String(value[15]).replace(/ /g, ''),
            enterprise: value[16],
            year: typeof value[17] === 'undefined' ? 0 : String(value[17]).replace(/[^0-9.]/g, '')
          },
          contract: {
            contractNumber: String(value[18]).replace(/ /g, ''),
            startDate: this.ExcelDateToJSDate(value[19]),
            endDate: typeof value[20] === 'undefined' || value[20] === '' ? this.DateToJSDate(new Date()) : this.ExcelDateToJSDate(value[20]),
            paymentDay: String(value[21]).replace(/[^0-9.]/g, ''),
            monthlyPayment: typeof value[22] === 'undefined' ? 0 : String(value[22]).replace(/[^0-9.]/g, ''),
            totalContract: typeof value[23] === 'undefined' || value[23] === '' ? 1 : String(value[23]).replace(/[^0-9.]/g, ''),
            totalInstallments: typeof value[24] === 'undefined' || value[24] === '' ? 2 : String(value[24]).replace(/[^0-9.]/g, ''),
            totalInstallmentsPayed: typeof value[25] === 'undefined' || value[25] === '' ? 1 : String(value[25]).replace(/[^0-9.]/g, ''),
            nextInstallmentToPay: typeof value[26] === 'undefined' || value[26] === '' ?  1 : value[26],
            installmentPaymentSequence: sequence,
          },
          maintenance: {
            currentOdometer: typeof value[28] === 'undefined' ? 1 : String(value[28]).replace(/[^0-9.]/g, ''),
            maintenances: [
              {
                name: 'change_oil',
                lastChangeOdometer: typeof value[29] === 'undefined' ? 1 : parseInt(String(value[29]).replace(/[^0-9.]/g, ''), 0),
                lastChangeDate: typeof value[30] === 'undefined' ? '' : this.ExcelDateToJSDate(value[30])
              },
              {
                name: 'replace_front_tires',
                lastChangeOdometer:  typeof value[31] === 'undefined' ? 1 : parseInt(String(value[31]).replace(/[^0-9.]/g, ''), 0),
                lastChangeDate: typeof value[32] === 'undefined' ? '' : this.ExcelDateToJSDate(value[32])
              },
              {
                name: 'change_clutch_kit',
                lastChangeOdometer: typeof value[33] === 'undefined' ? 1 : parseInt(String(value[33]).replace(/[^0-9.]/g, ''), 0),
                lastChangeDate: typeof value[34] === 'undefined' ? '' : this.ExcelDateToJSDate(value[34])
              },
              {
                name: 'change_distribution_kit',
                lastChangeOdometer: typeof value[35] === 'undefined' ? 1 : parseInt(String(value[35]).replace(/[^0-9.]/g, ''), 0),
                lastChangeDate: typeof value[36] === 'undefined' ? '' : this.ExcelDateToJSDate(value[36])
              },
              {
                name: 'alternator_repair',
                lastChangeOdometer: typeof value[37] === 'undefined' ? 1 : parseInt(String(value[37]).replace(/[^0-9.]/g, ''), 0),
                lastChangeDate: typeof value[38] === 'undefined' ? '' : this.ExcelDateToJSDate(value[38])
              },
              {
                name: 'alignment',
                lastChangeOdometer: typeof value[39] === 'undefined' ? 1 : parseInt(String(value[39]).replace(/[^0-9.]/g, ''), 0),
                lastChangeDate: typeof value[40] === 'undefined' ? '' : this.ExcelDateToJSDate(value[40])
              }
            ]
          }
        };
        console.log(data);
        this.dataFinal.push(data);
      }
    });

  }

  DateToJSDate(dateSent) {
    const date = moment(dateSent, ['DD/MM/YYYY', 'YYYY-MM-DD', 'MM-DD-YYYY'], false);
    if (date.isValid()) {
      return date.format('YYYY-MM-DD');
    } else {
      return null;
    }
  }

  ExcelDateToJSDate(serial) {
    if (typeof serial === 'number') {
      const date = new Date((serial - (25567 + 1)) * 86400 * 1000);
      return `${date.getFullYear()}-${(date.getMonth() + 1)}-${date.getDate()}`;
    } else {
      return this.DateToJSDate(serial);
    }
  }

 async saveDrivers() {

  // this.spinner.show();

  this.result = [];
  this.current = 0;
  this.total = this.dataFinal.length;
  this.percentage = 0;
  this.showProgress = true;
  return await PromiseBluebird.mapSeries(this.dataFinal, async (data, index) => {
    const params = {
      data: [data]
    };

    await this._dashboardService.saveDriversMassive(params).subscribe((response: any) => {
      this.current ++;
      this.percentage = (this.current * 100) / this.total;
      this.result.push(response['result'][0]);
    });
  }, {concurrency: 1})
  .then(async () => {
    // this.showProgress = false;
    // this.dataFinal = [];
    // this.spinner.hide();
  });
 }
}
