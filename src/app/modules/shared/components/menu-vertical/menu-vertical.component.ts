import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';
import {Router} from '@angular/router';
import { PermissionsService } from '../../../system/services/permissions.service';

@Component({
  selector: 'app-menu-vertical',
  templateUrl: './menu-vertical.component.html',
  styleUrls: ['./menu-vertical.component.scss']
})
export class MenuVerticalComponent implements OnInit {
  @Output()
  closeDrawerEmit = new EventEmitter<string>();
  @Input()
    showText: boolean;
  constructor(
    private _location: Router,
    public _PermissionService: PermissionsService) {

  }

  public menuList = [
    {
      title: 'Home',
      link: '/home',
      icon: 'home',
      imageLink: 'assets/icons_name/dashboard_menu.svg',
      imageLinkOther: 'assets/icons_name/dashboard_menu_other.svg',
      permission: false,
    },
    {
      title: 'Pagos',
      link: '/payment-home',
      icon: 'home',
      imageLink: 'assets/icons_name/hand_2.svg',
      imageLinkOther: 'assets/icons_name/hand.svg',
      permission: 'menu_payment',
      subItems: [
        {title: 'Recaudo', link: 'driver/collect-dashboard'},
        {title: 'Chequeo del carro', link: 'driver/check-dashboard'},
        {title: 'Preventivo', link: 'driver/maintenance-dashboard'},
      ]
    },
    {
      title: 'Vehículo',
      link: '/vehicles-home',
      icon: 'home',
      imageLink: 'assets/icons_name/conduccion.svg',
      imageLinkOther: 'assets/icons_name/conduccion_menu_other.svg',
      permission: 'menu_vehicle',
      subItems: [
        {title: 'Accidentes', link: 'risk/accident-dashboard'},
        {title: 'Comparendos', link: 'risk/ticket-dashboard'},
        {title: 'Enfermedad', link: 'risk/sickness-dashboard'},
        {title: 'Vacaciones', link: 'risk/holiday-dashboard'},
      ]
    },
    {
      title: 'Comportamiento',
      link: '/behaviour-home',
      icon: 'home',
      imageLink: 'assets/icons_name/riesgos_menu_other.svg',
      imageLinkOther: 'assets/icons_name/riesgos_menu.svg',
      permission: 'menu_behaviour',
      subItems: [
        {title: 'Score', link: 'driver/score'},
        {title: 'Kilometros', link: 'driver/kilometers'},
        {title: 'Velocidad', link: 'driver/speed'},
        {title: 'Ubicación', link: 'driver/location'},
      ]
    },
    {
      title: 'Dispersión',
      link: '/scatter-home',
      icon: 'home',
      imageLink: 'assets/icons_name/scatter_menu.svg',
      imageLinkOther: 'assets/icons_name/scatter_menu_blue.svg',
      permission: 'menu_scatter',
      subItems: [
        {title: 'Score', link: 'driver/score'},
        {title: 'Kilometros', link: 'driver/kilometers'},
        {title: 'Velocidad', link: 'driver/speed'},
        {title: 'Ubicación', link: 'driver/location'},
      ]
    }
  ];

  ngOnInit(): void {
  }

  private toggleSubMenu(item: any) {
    if (item.expand === false || typeof item.expand === 'undefined') {
      this.menuList.map((value) => {
        value['expand'] = false;
      });
    }
    item.expand = !item.expand;
  }

  logout() {
    sessionStorage.clear();
    localStorage.clear();
    this._location.navigate(['']).then();
  }

  closeDrawer(location) {
    this.closeDrawerEmit.emit('close');
    this._location.navigate([location]).then();
  }

}
