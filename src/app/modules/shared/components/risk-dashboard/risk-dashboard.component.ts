import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {StorageService} from '../../services/storage.service';
import {DashboardService} from '../../services/dashboard.service';
import {ConfigurationService} from '../../../system/services/configuration.service';
import {GlobalSearchParams, RiskData, SearchGlobalParams} from '../../interfaces/SharedInterface';
import {NgxSpinnerService} from 'ngx-spinner';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-risk-dashboard',
  templateUrl: './risk-dashboard.component.html',
  styleUrls: ['./risk-dashboard.component.css']
})
export class RiskDashboardComponent implements OnInit {
  public riskForm: FormGroup;
  public globalSearch: GlobalSearchParams = {
    globalInput: '', globalInputEndDate: this.currentDate(),
    globalInputStartDate: this.currentDate()
  };

  public riskArray = [];

  public riskAccident = '00';
  public riskTicket = '00';
  public riskHoliday = '00';
  public riskDisease = '00';

  public classAccident = 'green-status';
  public classTicket = 'green-status';
  public classHoliday = 'green-status';
  public classSickness = 'green-status';

  public configuration: Object = {
    tickets: {
      red: {id: 0, start: 2},
      yellow: {id: 0, start: 1},
      green: {id: 0, start: 0},
    },
    accidents: {
      red: {id: 0, start: 2},
      yellow: {id: 0, start: 1},
      green: {id: 0, start: 0},
    },
    holidays: {
      red: {id: 0, start: 2},
      yellow: {id: 0, start: 1},
      green: {id: 0, start: 0},
    },
    sickness: {
      red: {id: 0, start: 2},
      yellow: {id: 0, start: 1},
      green: {id: 0, start: 0},
    },
  };

  constructor(
    private _dashboardService: DashboardService,
    private _formBuilder: FormBuilder,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getConfiguration();
    this.storageService.setItem('title', 'Financiera');
    this.initRiskTable();
    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.globalSearch.globalInput = this.storageService.getItem('globalSearch');
        this.updateRisks();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.globalSearch.globalInputStartDate = this.storageService.getItem('globalSearchStartDate');
        this.updateRisks();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.globalSearch.globalInputEndDate = this.storageService.getItem('globalSearchEndDate');
        this.updateRisks();
      }
    });
    this.updateRisks();
    this.buildRiskArray();
  }

  initRiskTable() {
    this.riskForm = this._formBuilder.group({
      noveltyType: [null],
    });
  }

  updateRisks() {
    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };
    this._dashboardService.getNovelties(params).subscribe((response: RiskData[]) => {
      this.spinner.show();
      this.riskAccident = '00';
      this.riskTicket = '00';
      this.riskHoliday = '00';
      this.riskDisease = '00';
      this.classHoliday = 'green-status';
      this.classTicket = 'green-status';
      this.classAccident = 'green-status';
      this.classSickness = 'green-status';

      if (response.length > 0) {
        for (let i = 0; i < response.length; i++) {
          const name = response[i]['name'];
          const count = response[i]['count'];
          if (name === 'holiday') {
            if (count <= this.configuration['holidays']['green']['start']) {
              this.classHoliday = 'green-status';
            } else if (count <= this.configuration['holidays']['yellow']['start'] || count < this.configuration['holidays']['red']['start']) {
              this.classHoliday = 'yellow-status';
            } else {
              this.classHoliday = 'red-status';
            }

            if (count < 10) {
              this.riskHoliday = '0' + count;
            } else {
              this.riskHoliday = count;
            }
          } else if (name === 'ticket') {
            if (count <= this.configuration['tickets']['green']['start']) {
              this.classTicket = 'green-status';
            } else if (count <= this.configuration['tickets']['yellow']['start'] || count < this.configuration['tickets']['red']['start']) {
              this.classTicket = 'yellow-status';
            } else {
              this.classTicket = 'red-status';
            }

            if (count < 10) {
              this.riskTicket = '0' + count;
            } else {
              this.riskTicket = count;
            }
          } else if (name === 'accident') {
            if (count <= this.configuration['accidents']['green']['start']) {
              this.classAccident = 'green-status';
            } else if (count <= this.configuration['accidents']['yellow']['start'] ||
              count < this.configuration['accidents']['red']['start']) {
              this.classAccident = 'yellow-status';
            } else {
              this.classAccident = 'red-status';
            }

            if (count < 10) {
              this.riskAccident = '0' + count;
            } else {
              this.riskAccident = count;
            }
          } else if (name === 'sick') {
            if (count <= this.configuration['sickness']['green']['start']) {
              this.classSickness = 'green-status';
            } else if (count <= this.configuration['sickness']['yellow']['start'] || count < this.configuration['sickness']['red']['start']) {
              this.classSickness = 'yellow-status';
            } else {
              this.classSickness = 'red-status';
            }
            if (count < 10) {
              this.riskDisease = '0' + count;
            } else {
              this.riskDisease = count;
            }
          }
        }
      } else {
          if (parseInt(this.riskHoliday, 0) <= this.configuration['holidays']['green']['start']) {
            this.classHoliday = 'green-status';
          } else if (parseInt(this.riskHoliday, 0) <= this.configuration['holidays']['yellow']['start'] || parseInt(this.riskHoliday, 0) < this.configuration['holidays']['red']['start']) {
            this.classHoliday = 'yellow-status';
          } else {
            this.classHoliday = 'red-status';
          }

          if (parseInt(this.riskTicket, 0) <= this.configuration['tickets']['green']['start']) {
            this.classTicket = 'green-status';
          } else if (parseInt(this.riskTicket, 0) <= this.configuration['tickets']['yellow']['start'] || parseInt(this.riskTicket, 0) < this.configuration['tickets']['red']['start']) {
            this.classTicket = 'yellow-status';
          } else {
            this.classTicket = 'red-status';
          }

          if (parseInt(this.riskAccident, 0) <= this.configuration['accidents']['green']['start']) {
            this.classAccident = 'green-status';
          } else if (parseInt(this.riskAccident, 0) <= this.configuration['accidents']['yellow']['start'] ||
            parseInt(this.riskAccident, 0) < this.configuration['accidents']['red']['start']) {
            this.classAccident = 'yellow-status';
          } else {
            this.classAccident = 'red-status';
          }

          if (parseInt(this.riskDisease, 0) <= this.configuration['sickness']['green']['start']) {
            this.classSickness = 'green-status';
          } else if (parseInt(this.riskDisease, 0) <= this.configuration['sickness']['yellow']['start'] || parseInt(this.riskDisease, 0) < this.configuration['sickness']['red']['start']) {
            this.classSickness = 'yellow-status';
          } else {
            this.classSickness = 'red-status';
          }
      }
      this.spinner.hide();
      this.buildRiskArray();
    });

    this.buildRiskArray();
  }

  currentDate() {
    const date = new Date();
    const day = date.getDate();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    return year + '-' + month + '-' + day;
  }

  getConfiguration() {
    this.configurationService.getConfigurationAlerts().subscribe((response: any) => {
      response.map((value) => {
        if (value['end_value'] === null) {
          if (value['scale_interval'] === 'first_scale') {
            this.configuration[value['scale_type']]['red']['start'] = value['start_value'];
            this.configuration[value['scale_type']]['red']['id'] = value['id'];
          } else if (value['scale_interval'] === 'second_scale') {
            this.configuration[value['scale_type']]['yellow']['start'] = value['start_value'];
            this.configuration[value['scale_type']]['yellow']['id'] = value['id'];
          } else if (value['scale_interval'] === 'third_scale') {
            this.configuration[value['scale_type']]['green']['start'] = value['start_value'];
            this.configuration[value['scale_type']]['green']['id'] = value['id'];
          }
        }
      });
    });
  }

  public buildRiskArray() {
    this.riskArray = [];

    this.riskArray.push({class: this.classAccident, title: 'Accidentes :', number: this.riskAccident, link: '/risk/accident-dashboard'});
    this.riskArray.push({class: this.classTicket, title: 'Comparendos:', number: this.riskTicket, link: '/risk/ticket-dashboard'});
    this.riskArray.push({class: this.classHoliday, title: 'Vacaciones:', number: this.riskHoliday, link: '/risk/holiday-dashboard'});
    this.riskArray.push({class: this.classSickness, title: 'Incapacidades:', number: this.riskDisease, link: '/risk/sickness-dashboard'});
  }

}
