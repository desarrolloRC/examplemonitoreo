import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BehaviourDashboardComponent } from './behaviour-dashboard.component';

describe('BehaviourDashboardComponent', () => {
  let component: BehaviourDashboardComponent;
  let fixture: ComponentFixture<BehaviourDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BehaviourDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BehaviourDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
