import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertComponent} from '../alert/alert.component';
import {MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-resolve-alert',
  templateUrl: './resolve-alert.component.html',
  styleUrls: ['./resolve-alert.component.css']
})
export class ResolveAlertComponent implements OnInit {
  public submitted = false;
  public eventForm: FormGroup;
  public alertModal: MatDialogRef<AlertComponent>;

  constructor( private dialog: MatDialog,
               private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.eventForm = this.formBuilder.group({
      notificationText: [null, Validators.compose([Validators.required])]
    });
  }

  public  async  alert() {
    const param = {
      minWidth: '382px',
      height: '186px',
      data: {text: 'La alerta se ha resuelto'}
    };
    this.alertModal = this.dialog.open(AlertComponent, param);
  }

}
