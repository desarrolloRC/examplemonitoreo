import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeedDashboardComponent } from './speed-dashboard.component';

describe('SpeedDashboardComponent', () => {
  let component: SpeedDashboardComponent;
  let fixture: ComponentFixture<SpeedDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpeedDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeedDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
