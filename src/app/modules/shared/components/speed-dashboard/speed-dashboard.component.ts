import {Component, OnInit} from '@angular/core';

import {StorageService} from '../../services/storage.service';
import {OperationDashboardService} from '../../../operation/services/operation-dashboard.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-speed-dashboard',
  templateUrl: './speed-dashboard.component.html',
  styleUrls: ['./speed-dashboard.component.css'],
  providers: [OperationDashboardService]
})
export class SpeedDashboardComponent implements OnInit {
  public percent = 0;
  public percentText = '0%';

  constructor(private operationDashboardService: OperationDashboardService,
              private spinner: NgxSpinnerService,
              private storageService: StorageService) {
  }

  public titleCard = 'de vehículos que excedieron el limite de velocidad';

  ngOnInit() {
    this.getAvg();
    let global = this.storageService.getItem('globalSearch');
    this.storageService.watchStorage().subscribe((data) => {
      this.getAvg();
      const parts = data.split('|');
      if (parts[2] === 'globalSearch') {
        global = this.storageService.getItem('globalSearch');
        if (global !== null && global !== '') {
          this.titleCard = '¿El vehículo ha excedio el limite de velocidad en esté período de tiempo?';
        } else {
          this.titleCard = 'de vehículos que excedieron el limite de velocidad';
        }
      }
    });
    if (global !== null && global !== '') {
      this.titleCard = '¿El vehículo ha excedio el limite de velocidad en esté período de tiempo?';
    } else {
      this.titleCard = 'de vehículos que excedieron el limite de velocidad';
    }
  }

  public getAvg() {
    const global = this.storageService.getItem('globalSearch');
    this.spinner.show();
    this.operationDashboardService.getVehicleSpeedLimitAvg().subscribe((response) => {
      if (typeof response['data'] !== 'undefined') {
        if (global !== null && global !== '') {
          this.percent = response['data']['average'][0]['percent'];
          this.percentText = response['data']['average'][0]['percent'] > 0  ? 'Si' : 'No';
        } else {
          this.percent = response['data']['average'][0]['percent'];
          this.percentText = response['data']['average'][0]['percent'] + '%';
        }
      }
      this.spinner.hide();
    });
  }

}
