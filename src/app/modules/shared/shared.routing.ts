import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {DashboardComponent} from './components/dashboard/dashboard.component';
import {AuthGuard} from '../../guards/auth.guard';
import { PaymentHomeComponent } from './components/payment-home/payment-home.component';
import { VehiclesHomeComponent } from './components/vehicles-home/vehicles-home.component';
import { BehaviourHomeComponent } from './components/behaviour-home/behaviour-home.component';
import { AddTicketMassiveComponent } from './components/add-ticket-massive/add-ticket-massive.component';
import { AddDriversComponent } from './components/add-drivers/add-drivers.component';
import { NewPaymentComponent } from './components/new-payment/new-payment.component';
import {AddCaptureOrderComponent} from './components/add-capture-order/add-capture-order.component';
import { ScatterHomeComponent } from './components/scatter-home/scatter-home.component';
import { AddPaymentPointsComponent } from './components/add-payment-points/add-payment-points.component';
import { ContractReestructuredComponent } from './components/contract-reestructured/contract-reestructured.component';
import { NewEstimateComponent } from './components/new-estimate/new-estimate.component';

const routes: Routes = [
  {path: 'home', component: DashboardComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'payment-home', component: PaymentHomeComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'vehicles-home', component: VehiclesHomeComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'behaviour-home', component: BehaviourHomeComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'scatter-home', component: ScatterHomeComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'add-tickets', component: AddTicketMassiveComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'add-drivers', component: AddDriversComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'add-capture-order', component: AddCaptureOrderComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'new-payment', component: NewPaymentComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'new-payment-points', component: AddPaymentPointsComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'reestructure-contract', component: ContractReestructuredComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'new-estimate', component: NewEstimateComponent, pathMatch: 'full', canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class SharedRouting {

}
