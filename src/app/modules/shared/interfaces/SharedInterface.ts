export interface BasicType {
  id: number;
  name: string;
  label: string;
}

export interface RiskData {
  id: number;
  plate: string;
  date: string;
  label: string;
}

export interface ParamsAndId {
  id: number;
  startDate: string;
  endDate: string;
  search: string;
  harvest?: string;
}

export interface GlobalSearch {
  id: number;
  label: string;
}

export interface SimpleValue {
  value: string;
}

export interface GlobalParams {
  globalParams: object;
}

export interface GlobalSearchParams {
  globalInput: string;
  globalInputStartDate: string;
  globalInputEndDate: string;
}

export interface GetById {
  id: string;
}

export interface SearchGlobalParams {
  startDate: string;
  endDate: string;
  search: string;
  harvest?: string;
}
