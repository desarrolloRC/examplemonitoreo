import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatSidenavModule,
  MatSliderModule,
  MatToolbarModule,
  MatIconModule,
  MatListModule,
  MatOptionModule,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatDatepickerModule,
  MatTabsModule,
  MatButtonToggleModule,
} from '@angular/material';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

import {MatCheckboxModule} from '@angular/material/checkbox';
import {NgModule} from '@angular/core';
import {NguiMapModule} from '@ngui/map';
import {ChartsModule} from 'ng2-charts';
import {PopoverModule} from 'ngx-popover';
import {NgxSpinnerModule} from 'ngx-spinner';
import {CommonModule} from '@angular/common';
import {NgxGraphModule} from '@swimlane/ngx-graph';
import {NgxChartsModule, TooltipModule} from '@swimlane/ngx-charts';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgCircleProgressModule} from 'ng-circle-progress';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatExpansionModule} from '@angular/material/expansion';

import {SharedRouting} from './shared.routing';
import {SafePipe} from '../risk/pipes/safe.pipe';
import {GetCellValue} from '../risk/pipes/city-pipe';
import {MapsComponent} from './components/maps/maps.component';
import {MenuComponent} from './components/menu/menu.component';
import {MenuVerticalComponent} from './components/menu-vertical/menu-vertical.component';
import {TableComponent} from './components/table/table.component';
import { LoaderComponent } from './components/loader/loader.component';
import {InfoCardComponent} from './components/info-card/info-card.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {FormGlobalComponent} from './components/form-global/form-global.component';
import {NotificationComponent} from './components/notification/notification.component';
import { AllVehiclesComponent } from './components/all-vehicles/all-vehicles.component';
import {AlertUpdateComponent} from '../risk/components/alert-update/alert-update.component';
import {RiskDashboardComponent} from './components/risk-dashboard/risk-dashboard.component';
import {ScoreDashboardComponent} from './components/score-dashboard/score-dashboard.component';
import {HeaderButttonsComponent} from './components/header-butttons/header-butttons.component';
import {SpeedDashboardComponent} from './components/speed-dashboard/speed-dashboard.component';
import {TravelDashboardComponent} from './components/travel-dashboard/travel-dashboard.component';
import { VehiclesInRiskComponent } from './components/vehicles-in-risk/vehicles-in-risk.component';
import {VehicleDashboardComponent} from './components/vehicle-dashboard/vehicle-dashboard.component';
import {CollectDashboardComponent} from './components/collect-dashboard/collect-dashboard.component';
import {BehaviourDashboardComponent} from './components/behaviour-dashboard/behaviour-dashboard.component';
import {MaintenanceDashboardComponent} from './components/maintenance-dashboard/maintenance-dashboard.component';
import { DriverAssistAverageComponent } from './components/driver-assist-average/driver-assist-average.component';
import {NavComponentGeneratorComponent} from './components/nav-component-generator/nav-component-generator.component';
import {AverageCollectDashboardComponent} from './components/average-collect-dashboard/average-collect-dashboard.component';
import {OperationDashboardCheckComponent} from '../operation/components/operation-dashboard-check/operation-dashboard-check.component';
import {OperationDashboardBehaviourComponent} from '../operation/components/operation-dashboard-behaviour/operation-dashboard-behaviour.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';


// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import { DriverRankingComponent } from './components/driver-ranking/driver-ranking.component';
import {TabletActionsComponent} from './components/tablet-actions/tablet-actions.component';
import {IconInformationComponent} from './components/icon-information/icon-information.component';
import {NotificationsComponent} from './components/notifications/notifications.component';
import { CustomerDetailComponent } from './components/customer-detail/customer-detail.component';
import {CallComponent} from './components/call/call.component';
import { TableOperationsComponent } from './components/table-operations/table-operations.component';
import { AlertComponent } from './components/alert/alert.component';
import { ResolveAlertComponent } from './components/resolve-alert/resolve-alert.component';
import { TicketComponent } from '../widget/components/ticket/ticket.component';
import { DocumentComponent } from '../widget/components/document/document.component';
import { DebtComponent } from '../widget/components/debt/debt.component';
import { InspectionAllComponent } from '../widget/components/inspection-all/inspection-all.component';
import { ThousandSuffixesPipe } from '../risk/pipes/currencyCustom.pipe';
import { suffixesPipe } from '../risk/pipes/currencyCustomFull.pipe';
import { PaymentFrecuencyComponent } from '../widget/components/payment-frecuency/payment-frecuency.component';
import { RoundPipe } from '../risk/pipes/round.pipe';
import { FulfillmentComponent } from '../widget/components/fulfillment/fulfillment.component';
import { GpsDistanceComponent } from '../widget/components/gps-distance/gps-distance.component';
import { InspectVariablesComponent } from '../widget/components/inspect-variables/inspect-variables.component';
import { TopSpeedComponent } from '../widget/components/top-speed/top-speed.component';
import { TopDistanceComponent } from '../widget/components/top-distance/top-distance.component';
import { GpsMapComponent } from '../widget/components/gps-map/gps-map.component';
import { VariableGraphComponent } from '../widget/components/variable-graph/variable-graph.component';
import { PaymentHomeComponent } from './components/payment-home/payment-home.component';
import { VehiclesHomeComponent } from './components/vehicles-home/vehicles-home.component';
import { BehaviourHomeComponent } from './components/behaviour-home/behaviour-home.component';
import { InspectionCountComponent } from '../widget/components/inspection-count/inspection-count.component';
import { ConstructionComponent } from '../widget/components/construction/construction.component';
import { AddTicketMassiveComponent } from './components/add-ticket-massive/add-ticket-massive.component';
import { AddDriversComponent } from './components/add-drivers/add-drivers.component';
import { NewPaymentComponent } from './components/new-payment/new-payment.component';
import { ChartReportComponent } from './components/chart-report/chart-report.component';
import { ChartScatterComponent } from './components/chart-scatter/chart-scatter.component';
import { AddCaptureOrderComponent } from './components/add-capture-order/add-capture-order.component';
import { ScatterHomeComponent } from './components/scatter-home/scatter-home.component';
import { ContractInfoSmallComponent } from './components/contract-info-small/contract-info-small.component';
import { InfoChartPartialComponent } from './components/info-chart-partial/info-chart-partial.component';
import { AddPaymentPointsComponent } from './components/add-payment-points/add-payment-points.component';
import {StorageService} from './services/storage.service';
import {I18nHelper} from '../../helpers/I18nHelper';
import { MenuMassiveComponent } from './components/menu-massive/menu-massive.component';
import { ContractReestructuredComponent } from './components/contract-reestructured/contract-reestructured.component';
import { NewEstimateComponent } from './components/new-estimate/new-estimate.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

@NgModule({
  declarations: [
    SafePipe,
    RoundPipe,
    ThousandSuffixesPipe,
    suffixesPipe,
    GetCellValue,
    GetCellValue,
    MenuComponent,
    MapsComponent,
    MapsComponent,
    CallComponent,
    TableComponent,
    InfoCardComponent,
    InfoCardComponent,
    DashboardComponent,
    DashboardComponent,
    FormGlobalComponent,
    FormGlobalComponent,
    MenuVerticalComponent,
    NotificationComponent,
    NotificationComponent,
    RiskDashboardComponent,
    TabletActionsComponent,
    NotificationsComponent,
    CustomerDetailComponent,
    RiskDashboardComponent,
    SpeedDashboardComponent,
    ScoreDashboardComponent,
    HeaderButttonsComponent,
    TravelDashboardComponent,
    IconInformationComponent,
    VehicleDashboardComponent,
    CollectDashboardComponent,
    BehaviourDashboardComponent,
    MaintenanceDashboardComponent,
    NavComponentGeneratorComponent,
    OperationDashboardCheckComponent,
    AverageCollectDashboardComponent,
    OperationDashboardBehaviourComponent,
    AlertUpdateComponent,
    VehiclesInRiskComponent,
    DriverAssistAverageComponent,
    LoaderComponent,
    AllVehiclesComponent,
    DriverRankingComponent,
    CustomerDetailComponent,
    TableOperationsComponent,
    AlertComponent,
    ResolveAlertComponent,
    TicketComponent,
    DocumentComponent,
    DebtComponent,
    InspectionAllComponent,
    PaymentFrecuencyComponent,
    FulfillmentComponent,
    GpsDistanceComponent,
    InspectVariablesComponent,
    TopSpeedComponent,
    TopDistanceComponent,
    GpsMapComponent,
    VariableGraphComponent,
    PaymentHomeComponent,
    VehiclesHomeComponent,
    BehaviourHomeComponent,
    InspectionCountComponent,
    ConstructionComponent,
    AddTicketMassiveComponent,
    AddDriversComponent,
    NewPaymentComponent,
    ChartReportComponent,
    ChartScatterComponent,
    AddCaptureOrderComponent,
    ScatterHomeComponent,
    ContractInfoSmallComponent,
    InfoChartPartialComponent,
    AddPaymentPointsComponent,
    MenuMassiveComponent,
    ContractReestructuredComponent,
    NewEstimateComponent
  ],
  imports: [
    CommonModule,
    SharedRouting,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatToolbarModule,
    FlexLayoutModule,
    AngularFontAwesomeModule,
    MatSidenavModule,
    Ng2GoogleChartsModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatIconModule,
    MatListModule,
    MatOptionModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatDatepickerModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    NgxChartsModule,
    NgxGraphModule,
    ChartsModule,
    TooltipModule,
    MatExpansionModule,
    NgCircleProgressModule.forRoot({
      units: '',
      titleColor: '#000',
      unitsColor: '#000'
    }),
    MatTabsModule,
    PopoverModule,
    AngularMultiSelectModule,
    NgxSpinnerModule,        // configure the imports
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      isolate: false
    }),
    MatCheckboxModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyA3siZL2lQ3RzQTEaK6gDUV8ZHHHgUuW1E&libraries=visualization'})
  ],
  exports: [
    TranslateModule,
    MapsComponent,
    MenuComponent,
    PopoverModule,
    CallComponent,
    TableComponent,
    InfoCardComponent,
    FormGlobalComponent,
    NotificationComponent,
    MenuVerticalComponent,
    NotificationsComponent,
    CustomerDetailComponent,
    TabletActionsComponent,
    HeaderButttonsComponent,
    IconInformationComponent,
    NavComponentGeneratorComponent,
    OperationDashboardCheckComponent,
    OperationDashboardBehaviourComponent,
    MaintenanceDashboardComponent,
    BehaviourDashboardComponent,
    AlertUpdateComponent,
    NgxChartsModule,
    LoaderComponent,
    NgxSpinnerModule,
    MatSlideToggleModule,
    AllVehiclesComponent,
    MatDatepickerModule,
    TranslateModule,
    Ng2GoogleChartsModule,
    MenuMassiveComponent,
    MatCheckboxModule,
    AngularMultiSelectModule
  ],
  entryComponents: [
    AlertUpdateComponent,
    NotificationsComponent,
    CallComponent,
    CustomerDetailComponent,
    AlertComponent,
    ResolveAlertComponent,
    ContractInfoSmallComponent,
    InfoChartPartialComponent
  ],
  providers: [
    {provide: 'googleChartsVersion', useValue: '46'},
  ]
})
export class SharedModule {
  constructor(private translate: TranslateService, storage: StorageService) {
    I18nHelper.setUp(translate, storage);
  }
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
