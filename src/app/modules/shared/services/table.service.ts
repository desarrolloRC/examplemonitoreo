import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {StorageService} from './storage.service';
import {SearchGlobalParams} from '../interfaces/SharedInterface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor(private _http: HttpClient,
              private storageService: StorageService) {
  }

  public requestData(microService: string, url: string, sort: string, order: string, page: number, size: number) {
    const data: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    const typeInstallment = this.storageService.getItem('typeInstallment');

    let globalSearch;
    const startDate = data['startDate'];
    const endDate = data['endDate'];

    if (typeof data['search'] === 'string' && data['search'] !== '') {
      globalSearch = data['search'];
    }

    let requestUrl = `
    ${microService}${url}?page=${page + 1}&size=${size}&indexed=false&startDate=${startDate}&endDate=${endDate}
    `;

    if (typeof globalSearch !== 'undefined') {
      requestUrl = requestUrl + `&globalSearch=${globalSearch}`;
    }

    if (typeof typeInstallment !== 'undefined') {
      requestUrl = requestUrl + `&typeInstallment=${typeInstallment}`;
    }

    if (typeof sort !== 'undefined') {
      requestUrl = requestUrl + `&sort=${sort}:${order}`;
    }

    if (typeof data.harvest !== 'undefined' && data.harvest !== null && data.harvest !== '') {
      requestUrl = requestUrl + `&harvest=${data.harvest}`;
    }

    return this._http.get(requestUrl);
  }

  public requestDataExcel(microService: string, url: string): Observable<any> {
    const data: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    const typeInstallment = this.storageService.getItem('typeInstallment');

    let globalSearch;
    const startDate = data['startDate'];
    const endDate = data['endDate'];

    if (typeof data['search'] === 'string' && data['search'] !== '') {
      globalSearch = data['search'];
    }

    let requestUrl = `
    ${microService}${url}?startDate=${startDate}&endDate=${endDate}
    `;

    if (typeof globalSearch !== 'undefined') {
      requestUrl = requestUrl + `&globalSearch=${globalSearch}`;
    }

    if (typeof typeInstallment !== 'undefined') {
      requestUrl = requestUrl + `&typeInstallment=${typeInstallment}`;
    }

    if (typeof data.harvest !== 'undefined' && data.harvest !== null && data.harvest !== '') {
      requestUrl = requestUrl + `&harvest=${data.harvest}`;
    }

    return this._http.get(requestUrl);
  }

  public requestDataExcelParams(microService: string, url: string, params: Array<any>): Observable<any> {
    const data: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    const typeInstallment = this.storageService.getItem('typeInstallment');

    let globalSearch;
    const startDate = data['startDate'];
    const endDate = data['endDate'];

    if (typeof data['search'] === 'string' && data['search'] !== '') {
      globalSearch = data['search'];
    }

    let requestUrl = `
    ${microService}${url}?startDate=${startDate}&endDate=${endDate}
    `;

    if (typeof globalSearch !== 'undefined') {
      requestUrl = requestUrl + `&globalSearch=${globalSearch}`;
    }

    if (typeof typeInstallment !== 'undefined') {
      requestUrl = requestUrl + `&typeInstallment=${typeInstallment}`;
    }

    if (typeof data.harvest !== 'undefined' && data.harvest !== null && data.harvest !== '') {
      requestUrl = requestUrl + `&harvest=${data.harvest}`;
    }

    return this._http.post(requestUrl, params);
  }
}
