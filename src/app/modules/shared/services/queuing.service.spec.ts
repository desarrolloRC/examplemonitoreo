import { TestBed } from '@angular/core/testing';

import { QueuingService } from './queuing.service';

describe('QueuingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QueuingService = TestBed.get(QueuingService);
    expect(service).toBeTruthy();
  });
});
