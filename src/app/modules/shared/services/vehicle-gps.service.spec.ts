import { TestBed } from '@angular/core/testing';

import { VehicleGpsService } from './vehicle-gps.service';

describe('VehicleGpsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VehicleGpsService = TestBed.get(VehicleGpsService);
    expect(service).toBeTruthy();
  });
});
