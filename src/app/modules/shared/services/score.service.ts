import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';
import {SearchGlobalParams} from '../interfaces/SharedInterface';
import {StorageService} from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {

  constructor(private storageService: StorageService, private http: HttpClient) { }

  public getVehiclesInRisk(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    return this.http.post(environment.urlScore + 'get-vehicles-in-risk', params);
  }

  public getVehiclesInRiskAll(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    return this.http.post(environment.urlScore + 'get-vehicles-in-risk-all', params);
  }

  public getSummaryByPlate(plate): Observable<object> {

    const params = {
      plate: plate
    };

    return this.http.post(environment.urlScore + 'get-summary-by-plate', params);
  }
}
