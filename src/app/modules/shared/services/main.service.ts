import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(private http: HttpClient) { }

  public newEvent(params): Observable<object> {
    return this.http.post(environment.url + 'new-event', params);
  }

  public getPersonalInfoByContract(params): Observable<object> {
    return this.http.post(environment.url + 'get-owner-all-info-by-contract', params);
  }

  public getDocumentsByVehicle(params: Object): Observable<object> {
    return this.http.post(environment.urlRisk + 'get-documents-by-vehicle', params);
  }

  public getTicketsByVehicle(vehicle_id): Observable<object> {
    return this.http.get(environment.urlRisk + `get-tickets-by-vehicle?vehicle_id=${vehicle_id}`);
  }

  public getVehicleInfo(vehicle_id): Observable<object> {
    return this.http.post(environment.urlDriver + `get-basic-info-by-vehicle-id`, {'id': vehicle_id});
  }

  public getPersonInfoByDni(dni): Observable<object> {
    return this.http.post(environment.url + `get-personal-info-by-dni`, {'dni': dni});
  }

  public getPersonInfoById(id): Observable<object> {
    return this.http.post(environment.url + `get-personal-info-by-id`, {'person_id': id});
  }

  public getActivityByContractId(contractId): Observable<object> {
    return this.http.post(environment.url + `get-activity`, {'contractId': contractId});
  }

  public getContractInfo(contractId): Observable<object> {
    return this.http.post(environment.urlDriver + `get-owner-contract-by-id`, {'ownerContractId': contractId});
  }

  public getAlertsByContractId(contractId): Observable<object> {
    return this.http.post(environment.url + `get-alerts`, {'contractId': contractId});
  }
}
