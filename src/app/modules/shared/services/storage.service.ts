import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private storageSub = new Subject<string>();


  watchStorage(): Observable<any> {
    return this.storageSub.asObservable();
  }

  setItem(key: string, data: any) {
    const old = localStorage.getItem(key);
    const string = old + '|' + data + '|' + key;
    if (old !== data) {
      localStorage.setItem(key, data);
      this.storageSub.next(string);
    }
  }

  getItem(key: string) {
    return localStorage.getItem(key);
  }

  removeItem(key) {
    localStorage.removeItem(key);
    this.storageSub.next('changed');
  }
}
