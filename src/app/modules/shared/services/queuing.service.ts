import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QueuingService {

  constructor(private http: HttpClient) {
  }

  public newNotificion(type, params): Observable<object> {
    let url = '';
    if (type === 'email') {
      url = environment.urlQueuing + 'send-email';
    } else {
      url = environment.urlQueuing + 'send-sms';
    }
    return this.http.post(url, params);
  }
}
