import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {StorageService} from './storage.service';
import {SearchGlobalParams} from '../interfaces/SharedInterface';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VehicleGpsService {

  constructor(private _http: HttpClient,
              private storageService: StorageService) {
  }

  public getVehiclePoints(): Observable<object> {
    const data: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };
    let startDateString = data['startDate'];
    const date1 = new Date(data['startDate']);
    const date2 = new Date(data['endDate']);
    const diffTime = Math.abs(date2.getTime() - date1.getTime());
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 

    if (diffDays > 25) {
      const d = new Date(date2);
      d.setDate(d.getDate() - 25);
      startDateString = `${d.getFullYear()}-${(d.getMonth() + 1)}-${d.getDate()}`
    }
    let globalSearch;
    const startDate = startDateString;
    const endDate = data['endDate'];

    if (typeof data['search'] === 'string' && data['search'] !== '') {
      globalSearch = data['search'];
    }

    let requestUrl = `
    ${environment.urlDriver}vehicles-gps-points?startDate=${startDate}&endDate=${endDate}
    `;

    if (typeof globalSearch !== 'undefined') {
      requestUrl = requestUrl + `&globalSearch=${globalSearch}`;
    }

    if (typeof data.harvest !== 'undefined' && data.harvest !== null && data.harvest !== '') {
      requestUrl = requestUrl + `&harvest=${data.harvest}`;
    }

    return this._http.get(requestUrl);
  }
}
