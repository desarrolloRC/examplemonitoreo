import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';
import {SearchGlobalParams} from '../interfaces/SharedInterface';
import {StorageService} from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  constructor(private storageService: StorageService, private http: HttpClient) { }

  public getCityById(params: Object): Observable<object> {

    return this.http.post(environment.url + 'get-city-by-id', params);
  }

  public saveLicenseScoreExcel(params): Observable<object> {
    return this.http.post(environment.urlDriver + 'save-license-score-excel', params);
  }

  public saveCaptureOrderExcel(params): Observable<object> {
    return this.http.post(environment.urlDriver + 'save-capture-order-excel', params);
  }

  public getPlateById(params: Object): Observable<object> {

    return this.http.post(environment.urlDriver + 'get-plate-by-vehicle-id', params);
  }

  public getIdByPlate(params: Object): Observable<object> {

    return this.http.post(environment.urlDriver + 'get-id-by-plate', params);
  }

  public getVehicleByContractId(params: Object): Observable<object> {

    return this.http.post(environment.urlDriver + 'get-vehicle-by-contract', params);
  }

  public getVehicleByOwnerDni(params: Object): Observable<object> {

    return this.http.post(environment.urlDriver + 'get-vehicle-by-owner-dni', params);
  }

  public getExpectedAmount(params: Object): Observable<object> {

    return this.http.post(environment.urlDriver + 'get-expected-amount-drivers', params);
  }

  public getScoreData(params: Object): Observable<object> {

    return this.http.post(environment.urlDriver + 'get-expected-amount-drivers', params);
  }

  public getInstallmentDataByPlate(params: Object): Observable<object> {

    return this.http.post(environment.urlDriver + 'get-installment-and-contract-payment', params);
  }

  public getLastLocation(plate):  Observable<object> {
    const params: Object = {
      plate: plate['plate']
    };
    return this.http.post(environment.urlScore + 'get-last-location', params);
  }

  public getVehiclesInRisk(): Observable<object> {

    const data: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest') || '',
    };

    let globalSearch;
    const startDate = data['startDate'];
    const endDate = data['endDate'];

    if (typeof data['search'] === 'string' && data['search'] !== '') {
      globalSearch = data['search'];
    }

    let requestUrl = `${environment.urlDriver}get-vehicles-in-risk?startDate=${startDate}&endDate=${endDate}`;

    if (typeof globalSearch !== 'undefined') {
      requestUrl = requestUrl + `&globalSearch=${globalSearch}`;
    }

    if (typeof data.harvest !== 'undefined' && data.harvest !== null && data.harvest !== '') {
      requestUrl = requestUrl + `&harvest=${data.harvest}`;
    }

    return this.http.get(requestUrl);
  }
  public getTopSpeedWidget(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest') || '',
    };

    return this.http.post(environment.urlScore + 'get-top-speed-widget', params);
  }


  public getDistanceWidget(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest') || '',
    };

    return this.http.post(environment.urlScore + 'get-distance-widget', params);
  }


  public getDistanceWidgetReport(): Observable<object> {

    const params = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest') || '',
      highDistance: 240
    };

    return this.http.post(environment.urlScore + 'get-distance-widget-report', params);
  }
  public getLowDistanceWidgetReport(): Observable<object> {

    const params = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest') || '',
      lowDistance: 100
    };

    return this.http.post(environment.urlScore + 'get-distance-widget-low-report', params);
  }
  public getGpsWidget(): Observable<object> {

    const d = new Date();
    const endDateString = `${d.getFullYear()}-${(d.getMonth() + 1)}-${d.getDate()}`;
    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: endDateString,
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest') || '',
    };

    return this.http.post(environment.urlScore + 'get-gps-widget', params);
  }
  public calculateValueVariable(type, ownerContract, date = 'day'): Observable<object> {

    const params = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest') || '',
      variable: type,
      ownerContract,
      date
    };

    return this.http.post(environment.urlScore + 'calculate-value-variable', params);
  }
  public getLastLocationReport(): Observable<object> {

    const params = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: new Date(),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest') || '',
    };

    return this.http.post(environment.urlScore + 'get-last-location-report', params);
  }

  public getChartData(endPoint, params?): Observable<object> {
    if (!params) {
      params = {
        startDate: this.storageService.getItem('globalSearchStartDate'),
        endDate: this.storageService.getItem('globalSearchEndDate'),
        search: this.storageService.getItem('globalSearch'),
        harvest: this.storageService.getItem('chartScatterHarvest') || '',
      };
    }

    return this.http.post(endPoint, params);
  }
}
