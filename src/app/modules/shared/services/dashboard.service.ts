import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';
import { SearchGlobalParams } from '../interfaces/SharedInterface';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private storageService: StorageService, private http: HttpClient) { }

  public getNovelties(params: Object): Observable<object> {
    return this.http.post(environment.urlRisk + 'get-novelties', params);
  }
  public uploadDocuments(params: Object): Observable<object> {
    return this.http.post(environment.urlRisk + 'upload-documents', params);
  }
  public getNoveltyTypes(): Observable<object> {
    return this.http.get(environment.urlRisk + 'get-novelty-types');
  }
  public getPlatesPeople(params: Object): Observable<object> {
    return this.http.post(environment.url + 'get-plates-people', params);
  }

  public getAverageCollect(params: Object): Observable<object> {
    return this.http.post(environment.urlCollect + 'get-average-collect', params);
  }

  public getTicketPendingInfoReport(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: '',
    };

    return this.http.post(environment.urlRisk + 'get-tickets-pending-info-report', params);
  }

  public getTicketPendingInfo(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: '',
    };

    return this.http.post(environment.urlRisk + 'get-tickets-pending-info', params);
  }

  public getDocumentInfo(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: '',
    };

    return this.http.post(environment.urlRisk + 'get-documents-info', params);
  }

  public getDocumentInfoReport(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: '',
    };

    return this.http.post(environment.urlRisk + 'get-documents-info-report', params);
  }

  public saveTicketsMassive(params): Observable<object> {

    return this.http.post(environment.urlRisk + 'save-tickets-massive', params);
  }

  public saveTicketsMassiveBatch(params): Observable<object> {

    return this.http.post(environment.urlRisk + 'save-tickets-massive-batch', params);
  }

  public savePaymentScore(params): Observable<object> {

    return this.http.post(environment.urlDriver + 'save-payment-score', params);
  }

  public saveBalanceInterest(params): Observable<object> {

    return this.http.post(environment.urlDriver + 'save-balance-interest', params);
  }

  public saveReestructuredContracts(params): Observable<object> {

    return this.http.post(environment.urlDriver + 'save-reestructured-contracts', params);
  }

  public saveDriversMassive(params): Observable<object> {

    return this.http.post(environment.url + 'save-drivers-massive', params);
  }

  public savePaymentCsv(params): Observable<object> {

    return this.http.post(environment.urlCollect + 'save-payment-csv', params);
  }

  public savePaymentCsvInterest(params): Observable<object> {

    return this.http.post(environment.urlCollect + 'save-payment-csv-with-interest', params);
  }

  public getPaymentHistory(): Observable<object> {

    return this.http.get(environment.urlCollect + 'get-payment-history');
  }

  public getMassiveLoadHistory(): Observable<object> {

    return this.http.post(environment.urlRisk + 'get-massive-load-history', {});
  }
}
