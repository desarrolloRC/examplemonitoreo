interface ContractInterface {
  dni: string;
  name: string;
  lastname: string;
  phone: string;
  mobile: string;
  email: string;
  dateOfBirth: string;
  peopleOnCharge: number;
  gender: number;
  ocupation: number;
  studyLevel: number;
  civilState: number;

  dniDriver: string;
  nameDriver: string;
  lastnameDriver: string;
  phoneDriver: string;
  mobileDriver: string;
  emailDriver: string;
  dateOfBirthDriver: string;
  genderDriver: number;

  contractNumber: string;
  paymentDate: number;
  monthlyPayment: number;
  totalPayment: number;
  installments: number;
  installmentsPayed: number;
  percentagePayed: number;
  vehicleAssigned: number;
  msgInApp: string;
  nextInstallmentToPay: number;
  odometer: number;
  observation: string;

  documents: any[];
}
