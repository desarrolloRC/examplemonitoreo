import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {SystemGuard} from './guards/system.guard';
import {AuthGuard} from '../../guards/auth.guard';
import {LoginComponent} from './components/login/login.component';
import {RecoveryPasswordGuard} from './guards/recovery-password.guard';
import {AccountComponent} from './components/account/account.component';
import {ConfigurationComponent} from './components/configuration/configuration.component';
import {RecoveryPasswordComponent} from './components/recovery-password/recovery-password.component';
import {SystemConfigurationComponent} from './components/system-configuration/system-configuration.component';
import {NewContractComponent} from './components/new-contract/new-contract.component';
import {PaymentComponent} from './components/payment/payment.component';
import { UpdateDocumentsComponent } from './components/update-documents/update-documents.component';
import {LicenseScoreComponent} from './components/license-score/license-score.component';
import {UploadDocumentComponent} from './components/upload-document/upload-document.component';

const routes: Routes = [
  {
    path: '', component: LoginComponent
  },
  {
    path: 'enterprise/:enterprise', component: LoginComponent, canActivate: [SystemGuard]
  },
  {
    path: 'account', component: AccountComponent, canActivate: [AuthGuard]
  },
  {
    path: 'configuration', component: ConfigurationComponent, canActivate: [AuthGuard]
  },
  {
    path: 'newContract', component: NewContractComponent, canActivate: [AuthGuard]
  },
  {
    path: 'newPayment', component: PaymentComponent, canActivate: [AuthGuard]
  },
  {
    path: 'newLicenseScore', component: LicenseScoreComponent, canActivate: [AuthGuard]
  },
  {
    path: 'uploadDocument', component: UploadDocumentComponent, canActivate: [AuthGuard]
  },
  {
    path: 'updateDocuments', component: UpdateDocumentsComponent, canActivate: [AuthGuard]
  },
  {
    path: 'system-configuration', component: SystemConfigurationComponent, canActivate: [AuthGuard]
  },
  {
    path: 'recovery-password/:token', component: RecoveryPasswordComponent, canActivate: [RecoveryPasswordGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemRouting {

}
