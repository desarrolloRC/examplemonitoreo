import { TestBed, async, inject } from '@angular/core/testing';

import { RecoveryPasswordGuard } from './recovery-password.guard';

describe('RecoveryPasswordGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecoveryPasswordGuard]
    });
  });

  it('should ...', inject([RecoveryPasswordGuard], (guard: RecoveryPasswordGuard) => {
    expect(guard).toBeTruthy();
  }));
});
