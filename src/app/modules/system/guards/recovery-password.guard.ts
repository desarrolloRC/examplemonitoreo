import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';

import {RecoveryPasswordService} from '../services/recovery-password.service';

@Injectable({
  providedIn: 'root'
})
export class RecoveryPasswordGuard implements CanActivate {
  constructor(private _recoveryPasswordService: RecoveryPasswordService,
              private _router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const token = state.root.children.shift().children.shift().params['token'];

    return this._recoveryPasswordService
      .checkToken({token: token})
      .toPromise()
      .then((response) => {
        localStorage.setItem('userData', JSON.stringify(response['data'].shift()));
        localStorage.setItem('recoveryToken', token);
        return true;
      }, () => {
        this._router.navigate(['home']);

        return false;
      });
  }
}
