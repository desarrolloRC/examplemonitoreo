import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SystemGuard implements CanActivate {

  constructor(private _router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const token = sessionStorage.getItem('token');
    if (token === null || typeof token === 'undefined') {
      return true;
    } else {
      this._router.navigate(['/home']);

      return false;
    }
  }
}
