import {ActivatedRoute} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {LoginService} from '../../services/login.service';
import {StorageService} from '../../../shared/services/storage.service';
import {ConfigurationService} from '../../services/configuration.service';
import {ConfirmValidParentMatcher, errorMessages} from '../../../../helpers/CommonHelper';
import {I18nHelper} from '../../../../helpers/I18nHelper';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public formError = errorMessages;
  public submitted = false;
  public img = '';
  public validator = new ConfirmValidParentMatcher();
  public state = false;

  constructor(
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _loginService: LoginService,
    private translate: TranslateService,
    private _StorageService: StorageService,
    private _ConfigurationService: ConfigurationService,
  ) {
  }

  ngOnInit() {
    this.getImgUrl(this.route.snapshot.paramMap.get('enterprise'));
    localStorage.removeItem('globalSearch');
    this.loginForm = this._formBuilder.group({
      user: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
    });
  }

  public login() {
    this.submitted = true;

    if (this.loginForm.valid) {
      this._loginService.login(this.loginForm.value).subscribe((response: LoginInterface) => {
        try {
          sessionStorage.setItem('token', response.token);
          location.reload();
        } catch {
          return false;
        }
      });
    } else {
      console.log(this.loginForm.value);
    }

    this.submitted = false;
  }

  public getImgUrl(name) {
    const params = {
      name: name
    };
    this._ConfigurationService.getImgUrlEnterprise(params).subscribe((value) => {
      if (typeof value !== 'undefined' && value !== null) {
        this.img = value['ImgUrl'];
        this._StorageService.setItem('imgEnterprise', value['ImgUrl']);
      } else {
        this.img = '';
        this._StorageService.setItem('imgEnterprise', '');
      }
    });
  }

  isNotLoggedIn = () => {
    const token = sessionStorage.getItem('token');
    return token === null || typeof token === 'undefined';
  }
}
