import { Component, OnInit } from '@angular/core';

import {ConfigurationStructure} from '../../definitions/ConfigurationStructure';
import {ConfigurationService} from '../../services/configuration.service';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {
  public attributeLabels: object[];

  constructor(private configurationService: ConfigurationService) { }

  public configuration: Object = {
    tickets: {
      red: {id: 0, start: 2},
      yellow: {id: 0, start: 1},
      green: {id: 0, start: 0},
    },
    accidents: {
      red: {id: 0, start: 2},
      yellow: {id: 0, start: 1},
      green: {id: 0, start: 0},
    },
    holidays: {
      red: {id: 0, start: 2},
      yellow: {id: 0, start: 1},
      green: {id: 0, start: 0},
    },
    sickness: {
      red: {id: 0, start: 2},
      yellow: {id: 0, start: 1},
      green: {id: 0, start: 0},
    },
    kilometers: {
      red: {id: 0, start: 3000, end: 6000},
      yellow: {id: 0, start: 1000, end: 2999},
      green: {id: 0, start: 0, end: 399},
    },
  };

  ngOnInit() {
    this.attributeLabels = ConfigurationStructure.attributeLabels;
    this.getConfiguration();

  }

  getConfiguration() {
    this.configurationService.getConfigurationAlerts().subscribe((response: any) => {
      response.map((value) => {
        if (value['end_value'] === null) {
          if (value['scale_interval'] === 'first_scale') {
            this.configuration[value['scale_type']]['red']['start'] = value['start_value'];
            this.configuration[value['scale_type']]['red']['id'] = value['id'];
          } else if (value['scale_interval'] === 'second_scale') {
            this.configuration[value['scale_type']]['yellow']['start'] = value['start_value'];
            this.configuration[value['scale_type']]['yellow']['id'] = value['id'];
          } else if (value['scale_interval'] === 'third_scale') {
            this.configuration[value['scale_type']]['green']['start'] = value['start_value'];
            this.configuration[value['scale_type']]['green']['id'] = value['id'];
          }
        } else {
          if (value['scale_interval'] === 'first_scale') {
            this.configuration[value['scale_type']]['red']['start'] = value['start_value'];
            this.configuration[value['scale_type']]['red']['end'] = value['end_value'];
            this.configuration[value['scale_type']]['red']['id'] = value['id'];
          } else if (value['scale_interval'] === 'second_scale') {
            this.configuration[value['scale_type']]['yellow']['start'] = value['start_value'];
            this.configuration[value['scale_type']]['yellow']['end'] = value['end_value'];
            this.configuration[value['scale_type']]['yellow']['id'] = value['id'];
          } else if (value['scale_interval'] === 'third_scale') {
            this.configuration[value['scale_type']]['green']['start'] = value['start_value'];
            this.configuration[value['scale_type']]['green']['end'] = value['end_value'];
            this.configuration[value['scale_type']]['green']['id'] = value['id'];
          }
        }
      });
    });
  }

  changeValue(ev, type, color) {
    const red = this.configuration[type]['red']['start'];
    const yellow = this.configuration[type]['yellow']['start'];
    const green = this.configuration[type]['green']['start'];
    const value = parseInt(ev.target.value);
    if (!isNaN(ev.target.value)) {
      if (color === 'red') {
        if (value <= yellow) {
          ev.target.value = red;
        } else {
          this.configuration[type]['red']['start'] = value;
        }
      } else if (color === 'yellow') {
        if (value <= green || value >= red) {
          ev.target.value = yellow;
        } else {
          this.configuration[type]['yellow']['start'] = value;
        }
      } else if (color === 'green') {
        if (value >= yellow) {
          ev.target.value = green;
        } else {
          this.configuration[type]['green']['start'] = value;
        }
      }
    } else {
      if (color === 'red') {
        ev.target.value = this.configuration[type]['red']['start'];
      } else if (color === 'yellow') {
        ev.target.value = this.configuration[type]['yellow']['start'];
      } else if (color === 'green') {
        ev.target.value = this.configuration[type]['green']['start'];
      }
    }
  }

  changeValueRange(ev, type, color, limit) {
    const red_start = this.configuration[type]['red']['start'];
    const yellow_start = this.configuration[type]['yellow']['start'];
    const green_start = this.configuration[type]['green']['start'];

    const red_end = this.configuration[type]['red']['end'];
    const yellow_end = this.configuration[type]['yellow']['end'];
    const green_end = this.configuration[type]['green']['end'];

    const value = parseInt(ev.target.value);

    if (!isNaN(ev.target.value)) {
      if (limit === 'start') {
        if (color === 'red') {
          if (value <= yellow_end || value > red_end) {
            ev.target.value = red_start;
          } else {
            this.configuration[type]['red']['start'] = value;
          }
        } else if (color === 'yellow') {
          if (value <= green_end || value >= red_start || value > yellow_end) {
            ev.target.value = yellow_start;
          } else {
            this.configuration[type]['yellow']['start'] = value;
          }
        } else if (color === 'green') {
          if (value >= yellow_start || value > green_end || value < 0) {
            ev.target.value = green_start;
          } else {
            this.configuration[type]['green']['start'] = value;
          }
        }
      } else if (limit === 'end') {
        if (color === 'red') {
          if (value < red_start) {
            ev.target.value = red_end;
          } else {
            this.configuration[type]['red']['end'] = value;
          }
        } else if (color === 'yellow') {
          if (value > red_start  || value < yellow_start) {
            ev.target.value = yellow_end;
          } else {
            this.configuration[type]['yellow']['end'] = value;
          }
        } else if (color === 'green') {
          if (value > yellow_start || value < green_start) {
            ev.target.value = green_end;
          } else {
            this.configuration[type]['green']['end'] = value;
          }
        }
      }
    } else {
      if (limit === 'start') {
        if (color === 'red') {
          ev.target.value = this.configuration[type]['red']['start'];
        } else if (color === 'yellow') {
          ev.target.value = this.configuration[type]['yellow']['start'];
        } else if (color === 'green') {
          ev.target.value = this.configuration[type]['green']['start'];
        }
      } else if (limit === 'end') {
        if (color === 'red') {
          ev.target.value = this.configuration[type]['red']['end'];
        } else if (color === 'yellow') {
          ev.target.value = this.configuration[type]['yellow']['end'];
        } else if (color === 'green') {
          ev.target.value = this.configuration[type]['green']['end'];
        }
      }
    }
  }

  saveAlerts() {
    this.configurationService.saveAlerts(this.configuration).subscribe((response: any) => {
      console.log(response);
    });
  }

}
