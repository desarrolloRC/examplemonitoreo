import { Component, OnInit } from '@angular/core';
import {MainService} from '../../services/main.service';
import {MatSnackBar} from '@angular/material';
import {UploadService} from '../../services/upload.service';
import {environment} from '../../../../../environments/environment';
import {PermissionsService} from '../../services/permissions.service';
import {Router} from '@angular/router';
import {JwtHelper} from '../../../../helpers/JwtHelper';
import {OperationDashboardService} from '../../../operation/services/operation-dashboard.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-new-contract',
  templateUrl: './new-contract.component.html',
  styleUrls: ['./new-contract.component.css']
})

export class NewContractComponent implements OnInit {
  public configuration = {
    gender: [],
    civilState: [],
    ocupation: [],
    studyLevel: [],
    plates: [],
    vehicleModel: [],
    maintenanceJobs: [],
    enterprises: [],
    documents: [],
    roles: []
  };

  public plate;
  public newVehicleChecked = false;

  public infoContract = {
    dni: '',
    name: '',
    lastname: '',
    phone: '',
    mobile: '',
    email: '',
    dateOfBirth: '',
    peopleOnCharge: 0,
    gender: 0,
    ocupation: 0,
    studyLevel: 0,
    civilState: 0,
    role: 0,

    dniDriver: '',
    nameDriver: '',
    lastnameDriver: '',
    phoneDriver: '',
    mobileDriver: '',
    emailDriver: '',
    dateOfBirthDriver: '',
    genderDriver: 0,

    contractNumber: '',
    paymentDate: 0,
    monthlyPayment: 0,
    totalPayment: 0,
    installments: 0,
    installmentsPayed: 0,
    percentagePayed: 0,
    vehicleAssigned: 0,
    msgInApp: '',
    nextInstallmentToPay: 0,
    odometer: 0,
    observation: '',
    timeInstallment: 0,

    plate: '',
    propertyCard: '677863343',
    year: 0,
    engineNumber: '',
    chasisNumber: '',
    isNew: true,
    vehicleModel: 0,
    vehicleFuelType: 1,
    enterpriseId: 0,

    maintenanceJobs: [],

    documents: [],

  };

  public documentsSelected = {};

  constructor(
    private _MainService: MainService,
    private snackBar: MatSnackBar,
    private _uploadService: UploadService,
    public _PermissionService: PermissionsService,
    private spinner: NgxSpinnerService,
    private _location: Router,
  ) { }

  ngOnInit() {
    this.getConfiguration();
    if (!this._PermissionService.canAccess('new_contract')) {
      this._location.navigate(['/']).then();
    }
 }

  openSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

  getConfiguration() {
    this._MainService.getConfigurationContract().subscribe((response) => {
      if (typeof response !== 'undefined') {
        this.configuration.gender = response['gender'];
        this.configuration.civilState = response['civilState'];
        this.configuration.documents = response['documents'];
        this.configuration.ocupation = response['ocupation'];
        this.configuration.plates = response['plates'];
        this.configuration.studyLevel = response['studyLevel'];
        this.configuration.vehicleModel = response['vehicleModels'];
        this.configuration.maintenanceJobs = response['maintenanceJobs'];
        this.infoContract.maintenanceJobs = response['maintenanceJobs'];
        this.configuration.enterprises = response['enterprises'];
        this.configuration.roles = response['roles'];

        this.configuration.documents.map((doc) => {
          this.documentsSelected[doc['Name']] = {};
        });
      }
    });
  }

  validateInfo() {
    let result = true;
    let message = '';
    const ic = this.infoContract;
    // Validate owner info
    if (ic.dni === '') {
      result = false;
      message = `Debe indicar la cedula del propietario`;
    } else if (ic.name === '') {
      result = false;
      message = `Debe indicar el nombre del propietario`;
    } else if (ic.lastname === '') {
      result = false;
      message = `Debe indicar el apellido del propietario`;
    } else if (ic.mobile === '') {
      result = false;
      message = `Debe indicar el celular del propietario`;
    } else if (ic.email === '') {
      result = false;
      message = `Debe indicar el correo del propietario`;
    } else if (ic.dateOfBirth === '') {
      result = false;
      message = `Debe indicar la fecha de nacimiento del propietario`;
    } else if (ic.gender === 0) {
      result = false;
      message = `Debe indicar el genero del propietario`;
    } else if (ic.ocupation === 0) {
      result = false;
      message = `Debe indicar la ocupación del propietario`;
    } else if (ic.studyLevel === 0) {
      result = false;
      message = `Debe indicar el nivel de estudios del propietario`;
    } else if (ic.civilState === 0) {
      result = false;
      message = `Debe indicar el estado civil del propietario`;
    } else if (ic.role === 0) {
      result = false;
      message = `Debe indicar el rol`;
    } else if (ic.dniDriver !== '') {
      if (ic.nameDriver === '') {
        result = false;
        message = `Debe indicar el nombre del conductor`;
      } else if (ic.lastnameDriver === '') {
        result = false;
        message = `Debe indicar el apelido del conductor`;
      } else if (ic.mobileDriver === '') {
        result = false;
        message = `Debe indicar el celular del conductor`;
      } else if (ic.dateOfBirthDriver === '') {
        result = false;
        message = `Debe indicar la fecha de nacimiento del conductor`;
      } else if (ic.emailDriver === '') {
        result = false;
        message = `Debe indicar el correo del conductor`;
      } else if (ic.genderDriver === 0) {
        result = false;
        message = `Debe indicar el genero del conductor`;
      }
    } else if (ic.contractNumber === '') {
      result = false;
      message = `Debe indicar el número del contrato`;
    } else if (ic.paymentDate === 0) {
      result = false;
      message = `Debe indicar el dia de pago mensual`;
    } else if (ic.monthlyPayment === 0) {
      result = false;
      message = `Debe indicar el monto de la cuota`;
    } else if (ic.totalPayment === 0) {
      result = false;
      message = `Debe indicar el pago total`;
    } else if (ic.installments === 0) {
      result = false;
      message = `Debe indicar el número de cuotas`;
    } else if (ic.nextInstallmentToPay === 0) {
      result = false;
      message = `Debe indicar el mes del proximo pago`;
    } else if (ic.odometer === 0) {
      result = false;
      message = `Debe indicar el kilometraje del vehículo`;
    } else if (ic.vehicleAssigned === 0 && !this.newVehicleChecked) {
      result = false;
      message = `Debe indicar el vehículo del contrato`;
    } else if (this.newVehicleChecked) {
      if (ic.plate === '') {
        result = false;
        message = `Debe indicar la placa del vehiculo`;
      } else if (ic.vehicleModel === 0) {
        result = false;
        message = `Debe indicar el módelo del vehículo`;
      } else if (ic.year === 0) {
        result = false;
        message = `Debe indicar el año del vehículo`;
      } else if (ic.engineNumber === '') {
        result = false;
        message = `Debe indicar el número del motor`;
      } else if (ic.chasisNumber === '') {
        result = false;
        message = `Debe indicar el número de chasis`;
      } else if (ic.enterpriseId === 0) {
        result = false;
        message = `Debe indicar la empresa`;
      }
    }
    return {result: result, message: message};
  }

  async saveAll() {
    if (this.infoContract.enterpriseId === 0) {
      const token = sessionStorage.getItem('token');
      this.infoContract.enterpriseId = JwtHelper.getEnterprise(token);
    }

    const valid = this.validateInfo();
    if (valid.result === false) {
      this.openSnackBar(valid.message);
    } else {
      if (this.infoContract.documents.length > 0) {
        await this.uploadFiles();
      } else {
        this.spinner.show();
        this._MainService.sendInfoContract(this.infoContract).subscribe((result: any) => {
          this.spinner.hide();
          if (result['result'] === 'Ok') {
            this.openSnackBar('Contrato registrado');
            this._location.navigate(['']);
          } else {
            if (typeof result['message'] === 'undefined') {
              this.openSnackBar('Ha ocurrido un error, verifique los datos');
            } else {
              this.openSnackBar(result['message']);
            }
          }
        });
      }
    }
  }

  uploadFiles() {

    this.spinner.show();
    const requests = this.infoContract.documents.map(async (document, index) => {
      return new Promise(async (resolve) => {
        if (document['file']['size'] < 100097152) {
          this.infoContract.documents[index]['expirationDate'] = this.documentsSelected[document['name']]['expirationDate'];
          const resultUpload = await this._uploadService.uploadFile(document);
          this.infoContract.documents[index]['url'] = `${environment.s3_folder_url}${document['plate']}/${document['name']}-${document['file']['name']}`;
          resolve();
        } else {
          window.alert('El archivo es demasiado grande');
          resolve();
        }
      });
    });

    Promise.all(requests).then((resultPromise) => {
      this._MainService.sendInfoContract(this.infoContract).subscribe((result: any) => {
        this.spinner.hide();
        if (result['result'] === 'Ok') {
          this.openSnackBar('Contrato registrado');
          this._location.navigate(['']);
        } else {
          if (typeof result['message'] === 'undefined') {
            this.openSnackBar('Ha ocurrido un error, verifique los datos');
          } else {
            this.openSnackBar(result['message']);
          }
        }
      });
    });
  }

  assignVehicle(event) {
    if (event.target.value !== '0') {
      this.plate = event.target.value.split('|')[1];
    } else {
      this.plate = '';
    }
  }

  assignPlate(event) {
    if (event.target.value !== '') {
      this.plate = event.target.value[1];
    } else {
      this.plate = '';
    }
  }

  assignExpirationDate(event, name) {
    this.documentsSelected[name]['expirationDate'] = event.target.value;
  }

  async onFileSelected(event) {
    const file = event.target.files[0];
    const name = event.target.getAttribute('data-name');
    this.infoContract.documents.push({file: file, name: name, plate: this.plate});
    this.documentsSelected[name]['file'] = file;
    this.documentsSelected[name]['name'] = name;
    this.documentsSelected[name]['plate'] = this.plate;
  }

  newVehicle(event) {
    this.infoContract.vehicleAssigned = 0;
    this.newVehicleChecked = event.checked;
  }

  assignJobInfo(event, name, type) {
    const value = event.target.value;

    this.infoContract.maintenanceJobs.map((job, index) => {
      if (job['Job']['Name'] === name) {
        this.infoContract.maintenanceJobs[index][type] = value;
      }
    });
  }

}
