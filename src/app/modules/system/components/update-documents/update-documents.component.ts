import { Component, OnInit } from '@angular/core';
import {MainService} from '../../services/main.service';
import {MatSnackBar} from '@angular/material';
import {UploadService} from '../../services/upload.service';
import {environment} from '../../../../../environments/environment';
import {PermissionsService} from '../../services/permissions.service';
import {Router} from '@angular/router';
import {JwtHelper} from '../../../../helpers/JwtHelper';
import {OperationDashboardService} from '../../../operation/services/operation-dashboard.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-update-documents',
  templateUrl: './update-documents.component.html',
  styleUrls: ['./update-documents.component.css']
})
export class UpdateDocumentsComponent implements OnInit {

  public configuration = {
    gender: [],
    civilState: [],
    ocupation: [],
    studyLevel: [],
    plates: [],
    vehicleModel: [],
    maintenanceJobs: [],
    enterprises: [],
    documents: [],
    roles: []
  };

  public plate;
  public newVehicleChecked = false;

  public infoContract = {
    vehicleAssigned: 0,

    documents: [],

  };

  public documentsSelected = {};

  constructor(
    private _MainService: MainService,
    private snackBar: MatSnackBar,
    private _uploadService: UploadService,
    public _PermissionService: PermissionsService,
    private spinner: NgxSpinnerService,
    private _location: Router,
  ) { }

  ngOnInit() {
    this.getConfiguration();
    if (!this._PermissionService.canAccess('new_contract')) {
      this._location.navigate(['/']).then();
    }
 }

  openSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

  getConfiguration() {
    this._MainService.getConfigurationContract().subscribe((response) => {
      console.log(response);
      if (typeof response !== 'undefined') {
        this.configuration.documents = response['documents'];
        this.configuration.plates = response['platesWithContract'];

        this.configuration.documents.map((doc) => {
          this.documentsSelected[doc['Name']] = {};
        });
      }
    });
  }

  validateInfo() {
    let result = true;
    let message = '';
    const ic = this.infoContract;
    // Validate owner info
    if (ic.vehicleAssigned === 0 && !this.newVehicleChecked) {
      result = false;
      message = `Debe indicar el vehículo`;
    }
    return {result: result, message: message};
  }

  async saveAll() {

    const valid = this.validateInfo();
    if (valid.result === false) {
      this.openSnackBar(valid.message);
    } else {
      if (this.infoContract.documents.length > 0) {
        await this.uploadFiles();
      }
    }
  }

  assignVehicle(event) {
    if (event.target.value !== '0') {
      this.plate = event.target.value.split('|')[1];
    } else {
      this.plate = '';
    }
  }

  uploadFiles() {

    this.spinner.show();
    const requests = this.infoContract.documents.map(async (document, index) => {
      return new Promise(async (resolve) => {
        if (document['file']['size'] < 100097152) {
          this.infoContract.documents[index]['expirationDate'] = this.documentsSelected[document['name']]['expirationDate'];
          const resultUpload = await this._uploadService.uploadFile(document);
          this.infoContract.documents[index]['url'] = `${environment.s3_folder_url}${document['plate']}/${document['name']}-${document['file']['name']}`;
          resolve();
        } else {
          window.alert('El archivo es demasiado grande');
          resolve();
        }
      });
    });

    Promise.all(requests).then((resultPromise) => {
      this._MainService.sendInfoContractDocuments(this.infoContract).subscribe((result: any) => {
        this.spinner.hide();
        if (result['result'] === 'Ok') {
          this.openSnackBar('Contrato registrado');
          this._location.navigate(['']);
        } else {
          if (typeof result['message'] === 'undefined') {
            this.openSnackBar('Ha ocurrido un error, verifique los datos');
          } else {
            this.openSnackBar(result['message']);
          }
        }
      });
    });
  }

  assignExpirationDate(event, name) {
    this.documentsSelected[name]['expirationDate'] = event.target.value;
  }

  async onFileSelected(event) {
    const file = event.target.files[0];
    const name = event.target.getAttribute('data-name');
    this.infoContract.documents.push({file: file, name: name, plate: this.plate});
    this.documentsSelected[name]['file'] = file;
    this.documentsSelected[name]['name'] = name;
    this.documentsSelected[name]['plate'] = this.plate;
  }


}
