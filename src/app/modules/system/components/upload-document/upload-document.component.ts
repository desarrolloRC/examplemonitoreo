import * as XLSX from 'xlsx';
import * as moment from 'moment';
import {Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {MainService} from '../../../shared/services/main.service';
import {DriverService} from '../../../shared/services/driver.service';
import {DashboardService} from '../../../shared/services/dashboard.service';

@Component({
  selector: 'app-upload-document',
  templateUrl: './upload-document.component.html',
  styleUrls: ['./upload-document.component.css']
})
export class UploadDocumentComponent implements OnInit {
  data: any;
  dataFinal: object[] = [];
  dataSend: object[] = [];

  constructor(private _location: Router,
              private dashboardService: DashboardService,
              private _snackBar: MatSnackBar,
              private driverService: DriverService,
              private mainService: MainService) { }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    this.data = [];
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    // if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <any>(XLSX.utils.sheet_to_json(ws, {header: 1, raw: false}));
      this.proccessFile();

    };
    reader.readAsBinaryString(target.files[0]);
  }

  proccessFile () {
    this.dataSend = [];

    this.data.map((value, index) => {
      let band = false;
      if (value[0] && value[1] && value[5]
        && value[0] !== 'placa/dni'
        && typeof value[0] !== 'undefined'
        && typeof value[1] !== 'undefined'
        && typeof value[5] !== 'undefined'
        && index > 1) {
        value[2] = this.DateToJSDate(value[2]);
        value[3] = this.DateToJSDate(value[3]);
        if ((value[2] || value[2] === '')
          && (value[3] || value[3] === '')) {
          if (value[5] === 'driver_license') {
            value[7] = this.DateToJSDate(value[7]);

            if ((value[7] || value[7] === '')) {
              band = true;
              value[8] = String(value[8]).replace(/ /g, '');
              value[9] = String(value[9]).replace(/ /g, '');
              value[10] = String(value[10]).replace(/ /g, '');
            }
          } else if (value[5] === 'credential') {
            value[6] = this.DateToJSDate(value[6]);
            value[7] = this.DateToJSDate(value[7]);

            if ((value[6] || value[6] === '') && (value[7] || value[7] === '')) {
              band = true;
            }
          } else if (value[5] === 'road_safety') {
            value[10] = this.DateToJSDate(value[10]);
            if ((value[10] || value[10] === '')) {
              band = true;
            }
          } else if (value[5] === 'municipal_authorization') {
            value[7] = this.DateToJSDate(value[7]);
            value[9] = this.DateToJSDate(value[9]);
            if ((value[7] || value[7] === '') && (value[9] || value[9] === '')) {
              band = true;
            }
          } else if (value[5] === 'circulation_card') {
            value[6] = this.DateToJSDate(value[6]);
            value[8] = this.DateToJSDate(value[8]);
            value[9] = this.DateToJSDate(value[9]);
            if ((value[6] || value[6] === '') && (value[8] || value[8] === '') && (value[9] || value[9] === '')) {
              band = true;
              value[7] = String(value[7]).replace(/ /g, '');
            }
          } else if (value[5] === 'property_card') {
            band = true;
          } else if (value[5] === 'soat') {
            value[9] = this.DateToJSDate(value[9]);
            value[11] = this.DateToJSDate(value[11]);
            value[12] = this.DateToJSDate(value[12]);
            if ((value[9] || value[9] === '') && (value[11] || value[11] === '') && (value[12] || value[12] === '')) {
              band = true;
            }
          } else if (value[5] === 'technical_inspection') {
            value[9] = this.DateToJSDate(value[9]);
            value[10] = this.DateToJSDate(value[10]);
            if ((value[9] || value[9] === '') && (value[10] || value[10] === '')) {
              band = true;
            }
          } else if (value[5] === 'gas_certificate') {
            value[7] = this.DateToJSDate(value[7]);
            if ((value[7] || value[7] === '')) {
              band = true;
            }
          } else if (value[5] === 'gas_tank_certificate') {
            value[6] = this.DateToJSDate(value[6]);
            if ((value[6] || value[6] === '')) {
              band = true;
            }
          }

          if (band) {
            if (value[1] === 'vehicle') {
              this.proccessVehicle(value, index);
            } else {
              this.proccessPerson(value, index);
            }
          }
        }
        if (!band) {
          const data = {
            vehicle: value[0].trim(),
            dni: value[0].trim(),
            type: value[1],
            expedition_date: value[2],
            expiration_date: value[3],
            document_type: value[5],
            status: 'danger'
          };
          console.log(data);
          this.dataSend.push(data);
        }
      }
    });
    console.log(this.dataSend);
  }
  proccessVehicle(value, index) {
    const vehicle = value[0];
    const data = {
      vehicle: value[0].trim(),
      dni: vehicle.trim(),
      type: value[1],
      expedition_date: value[2],
      expiration_date: value[3],
      document_type: value[5],
      status: 'success'
    };
    this.dataSend.push(data);
    this.dataFinal.push(this.data[index]);
  }

  proccessPerson(value, index) {
    const vehicle = value[0];
    const data = {
      vehicle: value[0].trim(),
      dni: vehicle.trim(),
      type: value[1],
      expedition_date: value[2],
      expiration_date: value[3],
      document_type: value[5],
      status: 'success'
    };
    this.dataSend.push(data);
    this.dataFinal.push(this.data[index]);
  }

  DateToJSDate(dateSent) {
    if (typeof dateSent === 'undefined' || dateSent === '') {
      return '';
    } else {
      dateSent.replace(/ /g, '');
      const date = moment(dateSent,
        ['D/MM/YYYY', 'DD/MM/YYYY', 'D/M/YYYY'], false);
      if (date.isValid()) {
        return date.format('YYYY-MM-DD');
      } else {
        return false;
      }
    }
  }

  savePayments() {
    const params = {
      data: this.dataFinal
    };
    this.dashboardService.uploadDocuments(params).subscribe((response: Array<any>) => {
      this._location.navigate(['/']).then();
      this.openSnackBar('archivo cargado', 'Upload');
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
