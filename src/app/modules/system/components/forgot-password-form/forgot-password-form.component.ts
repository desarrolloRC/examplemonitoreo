import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {ConfirmValidParentMatcher, errorMessages} from '../../../../helpers/CommonHelper';
import {ForgotPasswordService} from '../../services/forgot-password.service';
import {NotificatonHelper} from '../../../../helpers/NotificatonHelper';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-forgot-password-form',
  templateUrl: './forgot-password-form.component.html',
  styleUrls: ['./forgot-password-form.component.css'],
  providers: [ForgotPasswordService]
})
export class ForgotPasswordFormComponent implements OnInit {

  public forgotPasswordForm: FormGroup;
  public formError = errorMessages;
  public submitted = false;

  public validator = new ConfirmValidParentMatcher();

  constructor(
    private _formBuilder: FormBuilder,
    private _forgotPasswordService: ForgotPasswordService,
    public dialogRef: MatDialogRef<ConfirmValidParentMatcher>
  ) {
  }

  ngOnInit() {
    this.forgotPasswordForm = this._formBuilder.group({
      email: [null, Validators.compose([Validators.required])],
    });
  }

  resetPassword() {
    this.submitted = true;

    if (this.forgotPasswordForm.valid) {
      return this._forgotPasswordService
        .forgotPassword(this.forgotPasswordForm.value)
        .subscribe(async (response) => {
          await NotificatonHelper.recoveryPasswordMessage(response['message']);

          this.dialogRef.close();
        }).add(() => {
          this.submitted = false;
        });
    } else {
      this.submitted = false;
      console.log(this.forgotPasswordForm.value);
    }
  }
}
