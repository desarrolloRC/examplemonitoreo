import {Router} from '@angular/router';
import {FormGroup} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {RxwebValidators, RxFormBuilder} from '@rxweb/reactive-form-validators';

import {NotificatonHelper} from '../../../../helpers/NotificatonHelper';
import {RecoveryPasswordService} from '../../services/recovery-password.service';

@Component({
  selector: 'app-recovery-password',
  templateUrl: './recovery-password.component.html',
  styleUrls: ['./recovery-password.component.css'],
  providers: [RecoveryPasswordService]
})
export class RecoveryPasswordComponent implements OnInit {

  public changePasswordForm: FormGroup;
  public submitted = false;

  constructor(
    private _formBuilder: RxFormBuilder,
    private _recoveryPasswordService: RecoveryPasswordService,
    private _router: Router,
  ) {
  }

  ngOnInit() {
    this.changePasswordForm = this._formBuilder.group({
      password: ['', RxwebValidators.compose({
        validators: [
          RxwebValidators.required(),
          RxwebValidators.minLength({value: 6}),
        ]
      })],
      confirmPassword: ['', RxwebValidators.compare({fieldName: 'password'})],
      recoveryToken: [localStorage.getItem('recoveryToken')]
    });
  }

  changePassword() {
    this.submitted = true;

    if (this.changePasswordForm.valid) {
      this._recoveryPasswordService.changePassword(this.changePasswordForm.value)
        .subscribe((response) => {
          localStorage.clear();
          NotificatonHelper.recoveryPasswordMessage(response['message']).then(
            () => {
              this._router.navigate(['']).then();
            }
          );
        });
    } else {
      this.submitted = false;
      console.log(this.changePasswordForm.value);
    }
  }
}
