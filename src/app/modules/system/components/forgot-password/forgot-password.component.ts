import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {ForgotPasswordFormComponent} from '../forgot-password-form/forgot-password-form.component';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  fileNameDialogRef: MatDialogRef<ForgotPasswordFormComponent>;

  constructor(private _dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openForgotPasswordDialog() {
    this.fileNameDialogRef = this._dialog.open(ForgotPasswordFormComponent, {
      panelClass: 'custom-modalbox-forgot-password',
      width: '40vw',
    });
  }
}
