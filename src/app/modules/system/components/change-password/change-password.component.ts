import {FormGroup} from '@angular/forms';
import {Component, Inject, OnInit} from '@angular/core';
import {RxFormBuilder, RxwebValidators} from '@rxweb/reactive-form-validators';

import {NotificatonHelper} from '../../../../helpers/NotificatonHelper';
import {RecoveryPasswordService} from '../../services/recovery-password.service';
import {ChangePasswordService} from '../../services/change-password.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  public submitted = false;
  public changePasswordForm: FormGroup;

  constructor(
    private _formBuilder: RxFormBuilder,
    private _changePasswordService: ChangePasswordService,
    public dialogRef: MatDialogRef<ChangePasswordComponent>,
  ) {
  }

  ngOnInit() {
    this.changePasswordForm = this._formBuilder.group({
      oldPassword: ['', RxwebValidators.compose({
        validators: [
          RxwebValidators.required(),
        ]
      })],
      password: ['', RxwebValidators.compose({
        validators: [
          RxwebValidators.required(),
          RxwebValidators.minLength({value: 6}),
        ]
      })],
      confirmPassword: ['', RxwebValidators.compare({fieldName: 'password'})]
    });
  }

  changePassword() {
    this.submitted = true;

    if (this.changePasswordForm.valid) {
      this._changePasswordService.changePassword(this.changePasswordForm.value)
        .subscribe(async (response) => {
          await NotificatonHelper.recoveryPasswordMessage(response['message']);
        });
      this.dialogRef.close();
    } else {
      this.submitted = false;
    }
  }
}
