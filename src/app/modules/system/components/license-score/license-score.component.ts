import * as XLSX from 'xlsx';
import {Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DriverService} from '../../../shared/services/driver.service';

@Component({
  selector: 'app-license-score',
  templateUrl: './license-score.component.html',
  styleUrls: ['./license-score.component.css']
})
export class LicenseScoreComponent implements OnInit {

  data: any;
  dataFinal: object[] = [];
  dataSend: object[] = [];

  constructor(private _location: Router,
              private _snackBar: MatSnackBar,
              private _DrivingService: DriverService) { }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    // if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <any>(XLSX.utils.sheet_to_json(ws, {header: 1}));
      this.proccessFile();

    };
    reader.readAsBinaryString(target.files[0]);
  }

  proccessFile () {
    this.dataFinal = [];
    this.dataSend = [];
    this.data.map((value) => {
      const data = {
        dni: value[0],
        license_score: value[3],
        very_serious_faults: value[1],
        serious_misconduct: value[2],
        strong_points: value[3],
        status: 'success'
      };
      this.dataFinal.push(data);
      this.dataSend.push(data);
    });
  }


  savePayments() {
    const params = {
      data: this.dataSend
    };
    this._DrivingService.saveLicenseScoreExcel(params).subscribe((response) => {
      this._location.navigate(['/']).then();
      this.openSnackBar('archivo cargado', 'Upload');
    });
    alert('Archivo cargado para procesamiento');
    window.location.reload();
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
