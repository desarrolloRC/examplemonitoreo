import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseScoreComponent } from './license-score.component';

describe('LicenseScoreComponent', () => {
  let component: LicenseScoreComponent;
  let fixture: ComponentFixture<LicenseScoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
