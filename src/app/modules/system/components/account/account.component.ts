import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../services/login.service';
import {MatDialog, MatDialogRef} from '@angular/material';

import {ChangePasswordComponent} from '../change-password/change-password.component';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  public email;

  fileNameDialogRef: MatDialogRef<ChangePasswordComponent>;

  constructor(private loginService: LoginService,
              private _dialog: MatDialog) {
  }

  ngOnInit() {
    this.getPersonalInfo();
  }

  getPersonalInfo() {
    this.loginService.getPersonalInfo().subscribe((response: any) => {
      this.email = response['Email'];
    });
  }

  openChangeDialog() {
    this.fileNameDialogRef = this._dialog.open(ChangePasswordComponent, {
      panelClass: 'custom-modalbox-forgot-password',
      width: '40vw',
      height: '450px'
    });
  }

}
