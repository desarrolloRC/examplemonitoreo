import { Component, OnInit } from '@angular/core';
import {MainService} from '../../services/main.service';
import {MatSnackBar} from '@angular/material';
import {UploadService} from '../../services/upload.service';
import {PermissionsService} from '../../services/permissions.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import * as XLSX from 'xlsx';
import {DriverService} from '../../../shared/services/driver.service';
import {CollectService} from '../../../operation/services/collect.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  constructor(
    private _MainService: MainService,
    private _CollectService: CollectService,
    private _DrivingService: DriverService,
    private snackBar: MatSnackBar,
    private _uploadService: UploadService,
    public _PermissionService: PermissionsService,
    private spinner: NgxSpinnerService,
    private _location: Router
  ) { }

  data: any;
  dataFinal: object[] = [];

  ngOnInit() {
    if (!this._PermissionService.canAccess('new_contract')) {
      this._location.navigate(['/']).then();
    }
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <any>(XLSX.utils.sheet_to_json(ws, {header: 1}));
      this.proccessFile();

    };
    reader.readAsBinaryString(target.files[0]);
  }

  proccessFile () {
    this.dataFinal = [];
    this.data.map((value) => {
      if (value[1] !== 'PLACA') {
        if (value[7] > 0) {
          this._DrivingService.getInstallmentDataByPlate({plate: value[1].replace(/ /g, '')}).subscribe((response: any) => {
            if (response !== -1) {
              let valid = false;
              response['installments'].map((installment) => {
                if (installment['status'] === 'ACTUAL' || installment['status'] === 'VENCIDA' || installment['status'] === 'PENDIENTE') {
                  if (valid === false) {
                    valid = true;
                    const amount = parseFloat(value[7]);
                    const amountToPay = installment['amount'] - installment['sum'];
                    const payed = amount >= amountToPay;
                    const date = new Date(installment['payment_date'].replace('.000Z', ''));
                    const data = {
                      vehicle: response['vehicle'],
                      contract: response['contract'],
                      installment: installment,
                      paymentDate: date,
                      month: date.getMonth() + 1,
                      year: date.getFullYear(),
                      amount: value[7],
                      payed: value[7] >= (installment['amount'] - installment['sum']) ? true : false,
                      installmentId: installment['id']

                    };
                    console.log(data);
                    this.dataFinal.push(data);
                  }
                }
              });
            }
          });
        }
      }
    });

  }

  savePayments() {
    const params = {
      data: this.dataFinal
    };
    this._CollectService.savePaymentsExcel(params).subscribe((response) => {
      this._location.navigate(['/']).then();
    });
  }

}
