import { TestBed } from '@angular/core/testing';

import { RecoveryPasswordService } from './recovery-password.service';

describe('RecoveryPasswordService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecoveryPasswordService = TestBed.get(RecoveryPasswordService);
    expect(service).toBeTruthy();
  });
});
