import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecoveryPasswordService {

  constructor(private http: HttpClient) { }

  public checkToken(params: Object): Observable<object> {

    return this.http.post(environment.urlPublic + 'check-token', params);
  }

  public changePassword(params: Object): Observable<object> {
    const personId = JSON.parse(localStorage.getItem('userData'));

    return this.http.post(environment.urlPublic + 'change-password/' + personId['id'], params);
  }
}
