import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';
import {JwtHelper} from '../../../helpers/JwtHelper';

@Injectable({
  providedIn: 'root'
})
export class MainService {


  constructor(private http: HttpClient) { }

  public getConfigurationContract(): Observable<object> {
    const token = sessionStorage.getItem('token');
    const params = {
      enterpriseId: JwtHelper.getEnterprise(token)
    };
    return this.http.post(environment.url + 'get-configuration-contract', params);
  }
  public sendInfoContract(params): Observable<object> {
    return this.http.post(environment.url + 'send-info-contract', params);
  }
  public sendInfoContractDocuments(params): Observable<object> {
    return this.http.post(environment.url + 'send-info-contract-documents', params);
  }

  public getEnterprise(): Observable<object> {
    const token = sessionStorage.getItem('token');
    const params = {
      enterpriseId: JwtHelper.getEnterprise(token)
    };
    return this.http.post(environment.url + 'get-enterprise', params);
  }
}
