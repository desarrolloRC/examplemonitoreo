import * as S3 from 'aws-sdk/clients/s3';
import {Injectable} from '@angular/core';


import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UploadService {
  public folder = environment.s3_folder;

  public bucket = new S3(
    {
      accessKeyId: environment.s3_access_key,
      secretAccessKey: environment.s3_private_key,
      region: environment.s3_region,
    }
  );

  constructor() {
    const token = sessionStorage.getItem('token');
    this.folder = `${environment.s3_folder}`;
  }

  public async uploadFile(file) {
    console.log(file);
    const params = {
      Bucket: environment.s3_bucket,
      Key: file['plate'] + `/` + file['name'] + '-' + file['file']['name'],
      Body: file['file']
    };

    return await this.bucket.upload(params, async (error, data) => {
      if (error !== null) {
        console.log('error ' + error);

        return false;
      }
      return `${environment.s3_folder_url}${file['plate']}/${file['name']}-${file['file']['name']}`;
    });
  }

  deleteUpload(key: string) {
    return this.bucket.deleteObject({Bucket: environment.s3_bucket, Key: this.folder + key}, (error, data) => {
      if (error !== null) {
        console.log('error ' + error);

        return false;
      }

      return data;
    });
  }
}
