import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {

  constructor(private http: HttpClient) { }

  public forgotPassword(params: Object): Observable<object> {
    
    return this.http.post(environment.urlPublic + 'forgot-password', params);
  }
}
