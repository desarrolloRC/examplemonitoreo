import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordService {

  constructor(private _http: HttpClient) { }

  public changePassword(params): Observable<any> {
    return this._http.post(environment.url + 'user-change-password', params);
  }
}
