import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(private http: HttpClient) { }

  public getConfigurationAlerts(): Observable<object> {

    return this.http.get(environment.url + 'get-configuration-alerts');
  }

  public getConfigurationAlertsByType(params): Observable<object> {

    return this.http.post(environment.url + 'get-configuration-alerts-by-type', params);
  }

  public saveAlerts(params): Observable<object> {

    return this.http.post(environment.url + 'save-alerts', params);
  }

  public saveAlertsAll(params): Observable<object> {

    return this.http.post(environment.url + 'save-alerts-all', params);
  }

  public getImgUrlEnterprise(params): Observable<object> {

    return this.http.post(environment.urlPublic + 'get-enterprise-by-name', params);
  }
}
