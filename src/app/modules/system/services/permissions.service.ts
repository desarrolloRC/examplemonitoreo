import {Injectable} from '@angular/core';

import {JwtHelper} from '../../../helpers/JwtHelper';

@Injectable({
    providedIn: 'root'
})
export class PermissionsService {

    constructor() {
    }

    public permissions = [];

    public getPermissions() {
        this.permissions = [];

        const token = sessionStorage.getItem('tokenDriver');

        if (token !== null && typeof token !== 'undefined') {
            const permissions = JwtHelper.getPermissions(token);
            if (permissions.length > 0) {
                return permissions;
            } else {
                return this.permissions;
            }
        } else {
            return this.permissions;
        }
    }

    public canAccess(permission) {
        let result = false;

        const token = sessionStorage.getItem('token');

        const permissions = JwtHelper.getPermissions(token);

        permissions.forEach((value) => {
            if (value['operation'] === permission) {
                result = true;
            }
        });
        return result;
    }
}
