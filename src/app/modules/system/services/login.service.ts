import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  public login(params: Object): Observable<object> {

    return this.http.post(environment.urlPublic + 'login', params);
  }

  public regenerateToken(params: Object): Observable<object> {

    return this.http.post(environment.urlPublic + 'regenerate-token', params);
  }

  public getPersonalInfo(): Observable<object> {

    return this.http.get(environment.url + 'get-personal-info');
  }

  public getHarvest(): Observable<object> {

    return this.http.post(environment.urlDriver + 'get-harvest-by-enterprise', {});
  }

  public getFirstDateContract(): Observable<object> {

    return this.http.post(environment.urlDriver + 'get-first-date-contract', {});
  }

}
