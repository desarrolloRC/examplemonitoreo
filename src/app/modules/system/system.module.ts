import {
  ErrorStateMatcher,
  MatButtonModule,
  MatCardModule, MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule, MatIconModule,
  MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule, MatSortModule, MatTableModule,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RxReactiveFormsModule} from '@rxweb/reactive-form-validators';

import {SystemRouting} from './system.routing';
import {SharedModule} from '../shared/shared.module';
import {LoginComponent} from './components/login/login.component';
import {AccountComponent} from './components/account/account.component';
import {ConfigurationComponent} from './components/configuration/configuration.component';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';
import {ChangePasswordComponent} from './components/change-password/change-password.component';
import {RecoveryPasswordComponent} from './components/recovery-password/recovery-password.component';
import {ForgotPasswordFormComponent} from './components/forgot-password-form/forgot-password-form.component';
import {SystemConfigurationComponent} from './components/system-configuration/system-configuration.component';
import { NewContractComponent } from './components/new-contract/new-contract.component';
import { PaymentComponent } from './components/payment/payment.component';
import { UpdateDocumentsComponent } from './components/update-documents/update-documents.component';
import { LicenseScoreComponent } from './components/license-score/license-score.component';
import { UploadDocumentComponent } from './components/upload-document/upload-document.component';
import {HttpClient} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {StorageService} from '../shared/services/storage.service';
import {I18nHelper} from '../../helpers/I18nHelper';

@NgModule({
  declarations: [
    LoginComponent,
    AccountComponent,
    ConfigurationComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent,
    RecoveryPasswordComponent,
    ForgotPasswordFormComponent,
    SystemConfigurationComponent,
    NewContractComponent,
    PaymentComponent,
    UpdateDocumentsComponent,
    LicenseScoreComponent,
    UploadDocumentComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    SystemRouting,
    MatCardModule,
    MatIconModule,
    MatSortModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatCheckboxModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      isolate: false
    }),
  ],
  exports: [
    TranslateModule
  ],
  entryComponents: [
    ChangePasswordComponent,
    ForgotPasswordFormComponent,
  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
  ]
})
export class SystemModule {
  constructor(private translate: TranslateService, storage: StorageService) {
    I18nHelper.setUp(translate, storage);
  }
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
