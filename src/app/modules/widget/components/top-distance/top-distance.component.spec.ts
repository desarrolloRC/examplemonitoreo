import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopDistanceComponent } from './top-distance.component';

describe('TopDistanceComponent', () => {
  let component: TopDistanceComponent;
  let fixture: ComponentFixture<TopDistanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopDistanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopDistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
