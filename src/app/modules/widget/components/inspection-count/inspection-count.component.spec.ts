import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionCountComponent } from './inspection-count.component';

describe('InspectionCountComponent', () => {
  let component: InspectionCountComponent;
  let fixture: ComponentFixture<InspectionCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
