import { Component, OnInit } from '@angular/core';
import { CheckService } from 'src/app/modules/operation/services/check.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';

@Component({
  selector: 'app-inspection-count',
  templateUrl: './inspection-count.component.html',
  styleUrls: ['./../ticket/ticket.component.css', './inspection-count.component.css']
})
export class InspectionCountComponent implements OnInit {

  public lowInspections = 0;
  public totalInspections = 0;
  constructor(
    private _CheckService: CheckService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getCheckInfo();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getCheckInfo();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getCheckInfo();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getCheckInfo();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getCheckInfo();
      }
    });
  }

  getCheckInfo() {
    this._CheckService.getCheckInfo().subscribe((response: any) => {
      this.totalInspections = response.length;
      this.lowInspections = 0;
      let count = 0;
      response.map((inspection) => {
        if ((inspection['score'] * 100) <= 40) {
          this.lowInspections ++;
        }
      })
    });
  }

}
