import { Component, OnInit } from '@angular/core';
import { CollectService } from 'src/app/modules/operation/services/collect.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';

@Component({
  selector: 'app-fulfillment',
  templateUrl: './fulfillment.component.html',
  styleUrls: ['./../ticket/ticket.component.css','./fulfillment.component.css']
})
export class FulfillmentComponent implements OnInit {
  public ideal = 0;
  public payed = 0;
  public percentagePayed = 0;

  constructor(
    private _CollectService: CollectService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getPaymentIdeal();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getPaymentIdeal();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getPaymentIdeal();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getPaymentIdeal();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getPaymentIdeal();
      }
    });
  }

  getPaymentIdeal() {
    this._CollectService.getPaymentIdeal().subscribe(async (response: any) => {
      this.ideal = 0;
      this.payed = 0;
      this.percentagePayed = 0;
      response.map((installment) => {
        this.ideal += parseFloat(installment['installment_amount']);
        this.payed += parseFloat(installment['payed']);
      });

    });
  }

}
