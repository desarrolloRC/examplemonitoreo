import { Component, OnInit } from '@angular/core';
import { DriverService } from 'src/app/modules/shared/services/driver.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';
import * as PromiseBluebird from 'bluebird';

@Component({
  selector: 'app-top-speed',
  templateUrl: './top-speed.component.html',
  styleUrls: ['./../ticket/ticket.component.css','./top-speed.component.css']
})
export class TopSpeedComponent implements OnInit {
  public countVehicles;
  public countTopVehicles;
  public countLimitVehicles;

  constructor(
    private _DriverService: DriverService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }



  ngOnInit() {
    this.getTopSpeedWidget();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getTopSpeedWidget();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getTopSpeedWidget();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getTopSpeedWidget();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getTopSpeedWidget();
      }
    });
  }

  getTopSpeedWidget() {
    this._DriverService.getTopSpeedWidget().subscribe(async (response: any) => {
      const vehiclesDates = {};
      return await PromiseBluebird.mapSeries(response, async function (topSpeed, index) {
        if (typeof vehiclesDates[topSpeed['plate']] === 'undefined') {
          vehiclesDates[topSpeed['plate']] = [];
        }
        vehiclesDates[topSpeed['plate']].push({
          topSpeed
        });
      }, {concurrency: 1})
      .then(async () => {
          for (const property in vehiclesDates) {
            if (vehiclesDates.hasOwnProperty(property)) {
              vehiclesDates[property]['summary'] = await this.getSummaryFromArray(vehiclesDates[property]);
            }
          }
          this.calculateWidget(vehiclesDates);
      });
    });
  }

  private async getSummaryFromArray(array) {

    let countTop = 0;
    let countMinus = 0;
    let distance = 0;
    return await PromiseBluebird.mapSeries(array, async function (topSpeedObject, index) {
        if (topSpeedObject['topSpeed']['times_beyond_limit'] > 0) {
          countMinus ++;
        }
        if (topSpeedObject['topSpeed']['top_speed'] > 100) {
          countTop ++;
        }
    }, {concurrency: 1})
    .then(function() {
        return {
          countTop,
          countMinus
        };
    });
  }

  private async calculateWidget(vehiclesDates) {
    this.countVehicles = 0;
    this.countLimitVehicles = 0;

    for (const property in vehiclesDates) {
      if (vehiclesDates.hasOwnProperty(property)) {
        if (vehiclesDates[property]['summary']['countTop'] > 0) {
          this.countVehicles ++;
        }
        if (vehiclesDates[property]['summary']['countMinus'] > 0) {
          this.countLimitVehicles ++;
        }
      }
    }

  }

}
