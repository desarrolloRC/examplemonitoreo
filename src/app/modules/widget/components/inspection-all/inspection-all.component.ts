import { Component, OnInit } from '@angular/core';
import { CheckService } from 'src/app/modules/operation/services/check.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-inspection-all',
  templateUrl: './inspection-all.component.html',
  styleUrls: ['./../ticket/ticket.component.css','./inspection-all.component.css']
})
export class InspectionAllComponent implements OnInit {
  public percentage = 0;
  public totalInspections = 0;
  constructor(
    private _CheckService: CheckService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getCheckInfo();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getCheckInfo();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getCheckInfo();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getCheckInfo();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getCheckInfo();
      }
    });
  }

  getCheckInfo() {
    this._CheckService.getCheckInfo().subscribe((response: any) => {
      this.totalInspections = response.length;
      let count = 0;
      response.map((inspection) => {
        if ((inspection['score'] * 100) <= 40) {
          count ++;
        }
      })
      if (response.length > 0) {
        this.percentage = (count * 100) / response.length;
      } else {
        this.percentage = 0;
      }
    });
  }

  downloadReport() {
    this._CheckService.getCheckInfoReport().subscribe((data: any) => {
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      XLSX.writeFile(wb, 'Report.xlsx');
    });
  }

}
