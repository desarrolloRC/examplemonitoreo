import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentFrecuencyComponent } from './payment-frecuency.component';

describe('PaymentFrecuencyComponent', () => {
  let component: PaymentFrecuencyComponent;
  let fixture: ComponentFixture<PaymentFrecuencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentFrecuencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentFrecuencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
