import { Component, OnInit } from '@angular/core';
import { CollectService } from 'src/app/modules/operation/services/collect.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';
import * as PromiseBluebird from 'bluebird';

@Component({
  selector: 'app-payment-frecuency',
  templateUrl: './payment-frecuency.component.html',
  styleUrls: ['./../ticket/ticket.component.css','./payment-frecuency.component.css']
})
export class PaymentFrecuencyComponent implements OnInit {
  public count = 0;
  public sum = 0;
  public countMinus = 0;
  constructor(
    private _CollectService: CollectService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getPaymentData();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getPaymentData();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getPaymentData();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getPaymentData();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getPaymentData();
      }
    });
  }

  public calculatePaymentFrecuency(contractsInfo) {
    this.count = 0;
    this.sum = 0;
    this.countMinus = 0;
    for (const property in contractsInfo) {
      if (contractsInfo.hasOwnProperty(property)) {
        this.count ++;
        this.sum += contractsInfo[property]['paymentFrecuency'];
        if (contractsInfo[property]['paymentFrecuency'] >= 40) {
          this.countMinus ++;
        }
      }
    }
  }

  public async getPaymentFrecuency(totalPayments) {
    let count = 0;
    let sum = 0;
    let firstDate;
    return await PromiseBluebird.mapSeries(totalPayments, async function (installment, index) {
        if (index === 0) {
            firstDate = new Date(installment['installment']['provider_date']);
        } else {
            const endDate = new Date(installment['installment']['provider_date']);

            const diffTime = Math.abs(endDate.getTime() - firstDate.getTime());
            sum += Math.ceil(diffTime / (1000 * 60 * 60 * 24));

            firstDate = new Date(installment['installment']['provider_date']);
        }
        count ++;
    }, {concurrency: 1})
    .then(function() {
        return Math.round(sum / count);
    });
  }

  getPaymentData() {
    this._CollectService.getPaymentData().subscribe(async (response: any) => {
      const contractsInfo = {};
      return await PromiseBluebird.mapSeries(response, async function (installment) {
        if (typeof contractsInfo[installment['owner_contract_id']] === 'undefined') {
          contractsInfo[installment['owner_contract_id']] = [];
        }
        contractsInfo[installment['owner_contract_id']].push({
          installment
        });
      }, {concurrency: 1})
      .then(async () => {
        for (const property in contractsInfo) {
          if (contractsInfo.hasOwnProperty(property)) {
            contractsInfo[property]['paymentFrecuency'] = await this.getPaymentFrecuency(contractsInfo[property]);
          }
        }
        this.calculatePaymentFrecuency(contractsInfo);
      });
    });
  }

}
