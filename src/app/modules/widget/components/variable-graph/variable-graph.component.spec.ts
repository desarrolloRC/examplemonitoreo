import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariableGraphComponent } from './variable-graph.component';

describe('VariableGraphComponent', () => {
  let component: VariableGraphComponent;
  let fixture: ComponentFixture<VariableGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariableGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariableGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
