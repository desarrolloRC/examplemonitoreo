import * as PromiseBluebird from 'bluebird';
import { Component, OnInit, Input } from '@angular/core';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';

import { DriverService } from 'src/app/modules/shared/services/driver.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';

declare var $: any;
declare var google: any;

@Component({
  selector: 'app-variable-graph',
  templateUrl: './variable-graph.component.html',
  styleUrls: ['./variable-graph.component.css']
})
export class VariableGraphComponent implements OnInit {
  @Input() typesAvailable: any[];
  @Input() ownerContractId: number;
  public selectedType = '';
  public selectedValue = '';
  public date = 'month';
  public columnChart: GoogleChartInterface = {
    chartType: 'ColumnChart',
    dataTable: [
    ],
    options: {
      title: '',
      colors: ['#5B9BF0', 'blue'],
      height: 250,
      hAxis: {title: 'Fecha'},
      vAxis: {
        gridlines: {
            color: 'transparent'
        },
        title: ''
    }}
  };

  constructor(
    private _DriverService: DriverService,
    private storageService: StorageService,
  ) {

  }

  ngOnInit() {
    if (this.typesAvailable.length > 0) {
      setTimeout(() => {
        this.changeSelectedType(null);
      }, 2000);

    }
    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getData();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getData();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getData();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getData();
      }
    });
  }

  changeSelectedType(event) {
    this.selectedType = '';
    this.selectedValue = '';
    let value;
    if (typeof event !== 'undefined' && event !== null) {
      value = event.target.value;
    } else {
      value = this.typesAvailable[0]['name'];
    }
    this.typesAvailable.map((type) => {
      if (type['name'] === value) {
        this.selectedType = type['label'];
        this.selectedValue = type['name'];
        this.columnChart.options['title'] = type['label'];
        this.columnChart.options['vAxis']['title'] = type['title'];

        const ccComponent = this.columnChart.component;
        if (typeof ccComponent !== 'undefined') {
          const ccWrapper = ccComponent.wrapper;
          if (typeof ccWrapper !== 'undefined') {
            ccComponent.draw();
          }
        }
        this.getData();
      }
    });
  }

  getData() {
    if (this.selectedValue !== '') {
      this._DriverService.calculateValueVariable(this.selectedValue, this.ownerContractId, this.date).subscribe(async (response: any ) => {
        this.columnChart.dataTable = [];
        const array = [];
        array.push(['Fecha', 'Valor', { role: 'style'}]);
        if (response.length > 0) {
          return await PromiseBluebird.mapSeries(response, async (value, index) => {
            if (this.date === 'month') {
              array.push([await this.getMonthName(value['date']), parseFloat(value['value']), '#5B9BF0']);
            } else {
              array.push([await this.getJustDate(value['date']), parseFloat(value['value']), '#5B9BF0']);
            }
          }, {concurrency: 1})
          .then(async () => {
            this.columnChart.dataTable = array;
            const ccComponent = this.columnChart.component;
            if (typeof ccComponent !== 'undefined') {
              const ccWrapper = ccComponent.wrapper;
              if (typeof ccWrapper !== 'undefined') {
                ccComponent.draw();
              }
            }
          });
        } else {
          this.selectedType = '';
        }
      });
    }
  }

  getJustDate(date) {
    const dateFormatted = new Date(date);

    return `${dateFormatted.getDate()}/${(dateFormatted.getMonth() + 1)}/${dateFormatted.getFullYear().toString().substr(-2)}`;
  }

  getMonthName(month) {
    switch (month) {
      case 1:
        return 'Ene';
        break;
      case 2:
        return 'Feb';
        break;
      case 3:
        return 'Mar';
        break;
      case 4:
        return 'Abr';
        break;
      case 5:
        return 'May';
        break;
      case 6:
        return 'Jun';
        break;
      case 7:
        return 'Jul';
        break;
      case 8:
        return 'Ago';
        break;
      case 9:
        return 'Sept';
        break;
      case 10:
        return 'Oct';
        break;
      case 11:
        return 'Nov';
        break;
      case 12:
        return 'Dic';
        break;
    }
  }

  changeDateType(type) {
    this.date = type;
    this.getData();
  }

}
