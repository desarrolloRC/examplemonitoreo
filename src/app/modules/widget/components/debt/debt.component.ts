import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/modules/shared/services/dashboard.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';
import { CollectService } from 'src/app/modules/operation/services/collect.service';

@Component({
  selector: 'app-debt',
  templateUrl: './debt.component.html',
  styleUrls: ['./../ticket/ticket.component.css','./debt.component.css']
})
export class DebtComponent implements OnInit {
  public daysInDebt = 0;
  public totalDebt = 0;

  constructor(
    private _CollectService: CollectService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getDebtInfo();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getDebtInfo();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getDebtInfo();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getDebtInfo();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getDebtInfo();
      }
    });
  }

  getDebtInfo() {
    this._CollectService.getDebtInfo().subscribe((response: any) => {
      this.daysInDebt = response['daysInDebt'];
      this.totalDebt = response['totalDebt'];
    });
  }

}
