import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/modules/shared/services/dashboard.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';
import * as XLSX from 'xlsx';
import {merge, of as observableOf} from 'rxjs';
@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./../ticket/ticket.component.css','./document.component.css']
})
export class DocumentComponent implements OnInit {
  public aboutToExpire = 0;
  public expired = 0;

  constructor(
    private _dashboardService: DashboardService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getDocumentInfo();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getDocumentInfo();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getDocumentInfo();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getDocumentInfo();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getDocumentInfo();
      }
    });
  }

  getDocumentInfo() {
    this._dashboardService.getDocumentInfo().subscribe((response: any) => {
      this.aboutToExpire = response['aboutToExpire'];
      this.expired = response['expired'];
    });
  }

  downloadReport() {
    this._dashboardService.getDocumentInfoReport().subscribe((data: any) => {
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      XLSX.writeFile(wb, 'Report.xlsx');
    });
  }


}
