import { Component, OnInit } from '@angular/core';
import { CheckService } from 'src/app/modules/operation/services/check.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';
import { DriverService } from 'src/app/modules/shared/services/driver.service';
import * as PromiseBluebird from 'bluebird';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-gps-distance',
  templateUrl: './gps-distance.component.html',
  styleUrls: ['./../ticket/ticket.component.css','./gps-distance.component.css']
})
export class GpsDistanceComponent implements OnInit {
  public countVehicles = 0;
  public countMinusVehicles = 0;
  public distance = 0;
  constructor(
    private _DriverService: DriverService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getDistanceWidget();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getDistanceWidget();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getDistanceWidget();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getDistanceWidget();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getDistanceWidget();
      }
    });
  }

  getDistanceWidget() {
    this._DriverService.getDistanceWidget().subscribe(async (response: any) => {
      const vehiclesDates = {};
      return await PromiseBluebird.mapSeries(response, async function (distance, index) {
        if (typeof vehiclesDates[distance['plate']] === 'undefined') {
          vehiclesDates[distance['plate']] = [];
        }
        vehiclesDates[distance['plate']].push({
          distance
        });
      }, {concurrency: 1})
      .then(async () => {
          for (const property in vehiclesDates) {
            if (vehiclesDates.hasOwnProperty(property)) {
              vehiclesDates[property]['summary'] = await this.getSummaryFromArray(vehiclesDates[property]);
            }
          }
          this.calculateWidget(vehiclesDates);
      });
    });
  }

  private async getSummaryFromArray(array) {
    let count = 0;
    let countMinus = 0;
    let distance = 0;
    return await PromiseBluebird.mapSeries(array, async function (distanceObject, index) {
        count ++;
        distance += distanceObject['distance']['kilometers'];
        if (distanceObject['distance']['kilometers'] < 150) {
          countMinus ++;
        }
    }, {concurrency: 1})
    .then(function() {
        return {
          countMinus,
          count,
          avg: Math.round(distance / count)
        };
    });
  }

  private async calculateWidget(vehiclesDates) {
    this.countVehicles = 0;
    this.countMinusVehicles = 0;
    this.distance = 0;

    for (const property in vehiclesDates) {
      if (vehiclesDates.hasOwnProperty(property)) {
        this.countVehicles ++;
        this.distance += vehiclesDates[property]['summary']['avg'];
        if (vehiclesDates[property]['summary']['countMinus'] > 0) {
          this.countMinusVehicles ++;
        }
      }
    }

  }

  downloadReport() {
    this._DriverService.getLowDistanceWidgetReport().subscribe((data: any) => {
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      XLSX.writeFile(wb, 'Report.xlsx');
    });
  }

}
