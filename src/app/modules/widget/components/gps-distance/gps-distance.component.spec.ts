import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpsDistanceComponent } from './gps-distance.component';

describe('GpsDistanceComponent', () => {
  let component: GpsDistanceComponent;
  let fixture: ComponentFixture<GpsDistanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpsDistanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpsDistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
