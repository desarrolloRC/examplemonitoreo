import { Component, OnInit } from '@angular/core';
import { DriverService } from 'src/app/modules/shared/services/driver.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';
import * as PromiseBluebird from 'bluebird';

@Component({
  selector: 'app-gps-map',
  templateUrl: './gps-map.component.html',
  styleUrls: ['./gps-map.component.css']
})
export class GpsMapComponent implements OnInit {

  public totalVehicles = 0;
  public vehiclesNotReporting = 0;
  public vehiclesShutdown = 0;

  constructor(
    private _DriverService: DriverService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getGpsWidget();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getGpsWidget();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getGpsWidget();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getGpsWidget();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getGpsWidget();
      }
    });
  }

  getGpsWidget() {
    this._DriverService.getGpsWidget().subscribe(async (response: any) => {
      const vehicleObjects = {};
      this.vehiclesNotReporting = 0;
      this.vehiclesShutdown = 0;
      this.totalVehicles = response.vehicles.split(',').length;

      return await PromiseBluebird.mapSeries(response.result, async  (gps, index) => {
        if (gps['last_date_gps'] !== 'unknown') {
          const date = new Date(gps['last_date_gps']);
          const diff = Math.abs(date.getTime() - new Date().getTime()) / 3600000;
          if (Math.round(diff) > 8) {
            this.vehiclesNotReporting ++;
          }
        }

        if (gps['kilometers'] <= 0) {
          this.vehiclesShutdown ++;
        }
      });
    });
  }
}
