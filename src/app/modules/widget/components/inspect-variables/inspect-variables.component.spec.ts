import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectVariablesComponent } from './inspect-variables.component';

describe('InspectVariablesComponent', () => {
  let component: InspectVariablesComponent;
  let fixture: ComponentFixture<InspectVariablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectVariablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectVariablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
