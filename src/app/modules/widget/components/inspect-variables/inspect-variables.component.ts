import { Component, OnInit } from '@angular/core';
import { CheckService } from 'src/app/modules/operation/services/check.service';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';

@Component({
  selector: 'app-inspect-variables',
  templateUrl: './inspect-variables.component.html',
  styleUrls: ['./../ticket/ticket.component.css','./inspect-variables.component.css']
})
export class InspectVariablesComponent implements OnInit {
  public variables = {
    first: '',
    second: '',
    third: '',
    fourth: ''

  }
  public firstVariable;
  public secondVariable;
  public thirdVariable;
  public fourthVariable;

  constructor(
    private _CheckService: CheckService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getVariablesInfo();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getVariablesInfo();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getVariablesInfo();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getVariablesInfo();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getVariablesInfo();
      }
    });
  }

  async getVariablesInfo() {
    this.firstVariable = await this.getVariableInfo('Aceite', 'first');
    this.secondVariable = await this.getVariableInfo('Líquido de frenos', 'second');
    this.thirdVariable = await this.getVariableInfo('Radiador', 'third');
    this.fourthVariable = await this.getVariableInfo('Líquido refrigerante', 'fourth');
  }

  async getVariableInfo(variable, variableValue) {
    this._CheckService.getVariableInfo(variable).subscribe((response: any) => {
      if (response.length > 0) {
        console.log(response[0]['answer']);
        this.variables[variableValue] = response[0]['answer'];
      } else {
        this.variables[variableValue] = 'Sin info';
      }
    });

  }

}
