import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/modules/shared/services/storage.service';
import { DashboardService } from 'src/app/modules/shared/services/dashboard.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/modules/system/services/configuration.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {
  public ticketCount = 0;
  public ticketAmount = 0;
  public ticketAvg = 0;

  constructor(
    private _dashboardService: DashboardService,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private configurationService: ConfigurationService
  ) {

  }

  ngOnInit() {
    this.getTicketPendingInfo();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.getTicketPendingInfo();
      } else if (parts[2] === 'globalSearchStartDate') {
        this.getTicketPendingInfo();
      } else if (parts[2] === 'globalSearchEndDate') {
        this.getTicketPendingInfo();
      } else if (parts[2] === 'chartScatterHarvest') {
        this.getTicketPendingInfo();
      }
    });
  }

  getTicketPendingInfo() {
    this._dashboardService.getTicketPendingInfo().subscribe((response: any) => {
      if (response.length > 0) {
        this.ticketCount = response[0]['count'];
        this.ticketAmount = response[0]['sum'] === null ? 0 : response[0]['sum'];
        this.ticketAvg = response[0]['avg'] === null ? 0 : response[0]['avg'];
      }
    });
  }

  downloadReport() {
    this._dashboardService.getTicketPendingInfoReport().subscribe((data: any) => {
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      XLSX.writeFile(wb, 'Report.xlsx');
    });
  }
}
