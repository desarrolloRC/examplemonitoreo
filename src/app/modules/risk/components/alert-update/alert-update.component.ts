import {Component, Inject, OnInit} from '@angular/core';
import {ConfigurationService} from '../../../system/services/configuration.service';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';

@Component({
  selector: 'app-alert-update',
  templateUrl: './alert-update.component.html',
  styleUrls: ['./alert-update.component.css']
})
export class AlertUpdateComponent implements OnInit {

  constructor(
    private configurationService: ConfigurationService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AlertUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  public labelAlert = '';
  public configuration: Object = {
    normal: {
      red: {id: 0, start: 2},
      yellow: {id: 0, start: 1},
      green: {id: 0, start: 0},
    },
    limits: {
      red: {id: 0, start: 3000, end: 6000},
      yellow: {id: 0, start: 1000, end: 2999},
      green: {id: 0, start: 0, end: 399},
    },
  };

  ngOnInit() {
    this.getConfiguration(this.data['type']);
  }

  getConfiguration(type) {
    const params: Object = {
      type: type
    };
    this.configurationService.getConfigurationAlertsByType(params).subscribe((response: any) => {
      response.map((value) => {
        this.labelAlert = value['scale_label'];
        if (value['end_value'] === null) {
          if (value['scale_interval'] === 'first_scale') {
            this.configuration['normal']['red']['start'] = value['start_value'];
            this.configuration['normal']['red']['id'] = value['id'];
          } else if (value['scale_interval'] === 'second_scale') {
            this.configuration['normal']['yellow']['start'] = value['start_value'];
            this.configuration['normal']['yellow']['id'] = value['id'];
          } else if (value['scale_interval'] === 'third_scale') {
            this.configuration['normal']['green']['start'] = value['start_value'];
            this.configuration['normal']['green']['id'] = value['id'];
          }
        }
      });
    });
  }

  changeValue(ev, color) {
    const type = 'normal';
    const red = this.configuration[type]['red']['start'];
    const yellow = this.configuration[type]['yellow']['start'];
    const green = this.configuration[type]['green']['start'];
    const value = parseInt(ev.target.value);
    if (!isNaN(ev.target.value)) {
      if (color === 'red') {
        if (value <= yellow) {
          ev.target.value = red;
        } else {
          this.configuration[type]['red']['start'] = value;
        }
      } else if (color === 'yellow') {
        if (value <= green || value >= red) {
          ev.target.value = yellow;
        } else {
          this.configuration[type]['yellow']['start'] = value;
        }
      } else if (color === 'green') {
        if (value >= yellow) {
          ev.target.value = green;
        } else {
          this.configuration[type]['green']['start'] = value;
        }
      }
    } else {
      if (color === 'red') {
        ev.target.value = this.configuration[type]['red']['start'];
      } else if (color === 'yellow') {
        ev.target.value = this.configuration[type]['yellow']['start'];
      } else if (color === 'green') {
        ev.target.value = this.configuration[type]['green']['start'];
      }
    }
  }

  saveAlerts() {
    this.configurationService.saveAlertsAll(this.configuration).subscribe((response: any) => {
      this.dialogRef.close();
    });
  }

}
