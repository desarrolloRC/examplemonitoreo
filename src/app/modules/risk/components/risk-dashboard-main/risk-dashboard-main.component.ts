import {Component, OnInit, ViewChild} from '@angular/core';
import {DriverCommonStructure} from "../../../operation/definitions/DriverCommonStructure";
import {RiskCommonStructure} from "../../definitions/RiskCommonStructure";
import {StorageService} from "../../../shared/services/storage.service";
import {GlobalSearchParams} from "../../../shared/interfaces/SharedInterface";
import {environment} from '../../../../../environments/environment';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-risk-dashboard-main',
  templateUrl: './risk-dashboard-main.component.html',
  styleUrls: ['./risk-dashboard-main.component.css'],
})
export class RiskDashboardMainComponent implements OnInit {
  public globalSearch: GlobalSearchParams = {globalInput: '', globalInputEndDate: this.currentDate(), globalInputStartDate: this.currentDate()};
  public pageIndex;
  public pageSize = 2;
  public pageSizeOptions = [5, 10, 15, 20];
  public attributeLabels: object[];
  public attributeLabelsAccidents: object[];
  public attributeLabelsTickets: object[];
  public attributeLabelsHolidaySick: object[];
  public attributeLabelsDocuments: object[];
  public riskMicroService = environment.urlRisk;
  @ViewChild('tabGroup') tabGroup;

  constructor(private storageService: StorageService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[0] !== parts[1] && parts[2] === 'globalSearch') {
        this.globalSearch.globalInput = this.storageService.getItem('globalSearch');
      } else if (parts[2] === 'globalSearchStartDate'){
        this.globalSearch.globalInputStartDate = this.storageService.getItem('globalSearchStartDate');
      } else if (parts[2] === 'globalSearchEndDate'){
        this.globalSearch.globalInputEndDate = this.storageService.getItem('globalSearchEndDate');
      }
    });

    this.attributeLabels = RiskCommonStructure.attributeLabelsNovelty;
    this.attributeLabelsAccidents = RiskCommonStructure.attributeLabelsNoveltyAccident;
    this.attributeLabelsTickets = RiskCommonStructure.attributeLabelsNoveltyTickets;
    this.attributeLabelsHolidaySick = RiskCommonStructure.attributeLabelsHolidaySick;
    this.attributeLabelsDocuments = RiskCommonStructure.attributeLabelsDocuments;

    this.route
      .data
      .subscribe((data) => {
        this.tabGroup.selectedIndex = data.index;
      });
  }

  currentDate() {
    const date = new Date();
    const day = date.getDate();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    return year + '-' + month + '-' + day;
  }

}
