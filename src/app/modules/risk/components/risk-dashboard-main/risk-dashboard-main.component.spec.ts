import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskDashboardMainComponent } from './risk-dashboard-main.component';

describe('RiskDashboardMainComponent', () => {
  let component: RiskDashboardMainComponent;
  let fixture: ComponentFixture<RiskDashboardMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskDashboardMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskDashboardMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
