export class TablesStructure {
  public static attributeLabelsMaintenance: object[] = [
    {
      name: 'vehicle_id',
      displayName: 'Placa',
      type: 'plateGlobal'
    },
    {
      name: 'label',
      displayName: 'Item',
      type: 'string'
    },
    {
      name: 'date_scheduled',
      displayName: 'Fecha',
      type: 'date'
    },
    {
      name: 'type_status',
      displayName: 'Estado',
      type: 'type_status'
    },
  ];

  public static attributeLabelsCheck: object[] = [
    {
      name: 'date',
      displayName: 'Fecha',
      type: 'date'
    },
    {
      name: 'vehicle_id',
      displayName: '# Crédito',
      type: 'plateGlobal'
    },
    {
      name: 'check_score',
      displayName: 'Puntaje inspección',
      type: 'number'
    },
  ];
}
