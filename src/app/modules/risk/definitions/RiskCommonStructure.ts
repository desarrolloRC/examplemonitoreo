export class RiskCommonStructure {
  public static attributeLabelsPayments: object[] = [
    {
      name: 'owner_contract_id',
      displayName: 'Contrato',
      type: 'location'
    },
    {
      name: 'amount',
      displayName: 'Valor',
      type: 'number'
    },
    {
      name: 'provider_date',
      displayName: 'Fecha del pago',
      type: 'date'
    },
  ];
  public static attributeLabelsInstallments: object[] = [
    {
      name: 'owner_contract_id',
      displayName: 'Contrato',
      type: 'plateGlobal'
    },
    {
      name: 'number_installment',
      displayName: 'Número cuota',
      type: 'string'
    },
    {
      name: 'amount',
      displayName: 'Monto',
      type: 'number'
    },
    {
      name: 'payed',
      displayName: 'Pagado',
      type: 'number'
    },
    {
      name: 'payment_date',
      displayName: 'Fecha de vencimiento',
      type: 'date'
    },
    {
      name: 'status',
      displayName: 'Estado',
      type: 'string'
    },
  ];
  public static attributeLabelsNovelty: object[] = [
    {
      name: 'Novelty_created_at',
      displayName: 'Fecha Creacion',
      type: 'date'
    },
    {
      name: 'Novelty_date',
      displayName: 'Fecha',
      type: 'date'
    },
    {
      name: 'Novelty_driver_id',
      displayName: 'Id Conductor',
      type: 'number'
    },
    {
      name: 'Novelty_id',
      displayName: 'Numero Novedad',
      type: 'number'
    },
    {
      name: 'Novelty_novelty_type_id',
      displayName: 'Numero Tipo Novedad',
      type: 'number'
    },
    {
      name: 'Novelty_observation',
      displayName: 'Observacion',
      type: 'string'
    }
  ];
  public static attributeLabelsNoveltyAccident: object[] = [
    {
      name: 'origin',
      displayName: 'Origen',
      type: 'string'
    },
    {
      name: 'vehicle_id',
      displayName: 'Contrato',
      type: 'plateGlobal'
    },
    {
      name: 'accident_number',
      displayName: 'Croquis',
      type: 'string'
    },
    {
      name: 'created_at',
      displayName: 'Fecha',
      type: 'date'
    },
    {
      name: 'city_id',
      displayName: 'Ciudad',
      type: 'city'
    },
    {
      name: 'status',
      displayName: 'Estado',
      type: 'string'
    },
    {
      name: 'type',
      displayName: 'Tipo de accidente',
      type: 'string'
    },
    {
      name: 'responsability',
      displayName: 'Responsabilidad',
      type: 'string'
    },
    {
      name: 'damage_type',
      displayName: 'Daños',
      type: 'string'
    }
  ];
  public static attributeLabelsNoveltyTickets: object[] = [
    {
      name: 'id',
      displayName: 'No.',
      type: 'string'
    },
    {
      name: 'vehicle_id',
      displayName: 'Contrato',
      type: 'plateGlobal'
    },
    {
      name: 'status',
      displayName: 'Estado',
      type: 'string'
    },
    {
      name: 'created_at',
      displayName: 'Fecha',
      type: 'date'
    },
    {
      name: 'origin',
      displayName: 'Origen',
      type: 'string'
    },
    {
      name: 'type',
      displayName: 'Tipo',
      type: 'string'
    },
    {
      name: 'amount',
      displayName: 'Monto',
      type: 'number'
    }
  ];
  public static attributeLabelsHolidaySick: object[] = [
    {
      name: 'vehicle_id',
      displayName: 'Contrato',
      type: 'plateGlobal'
    },
    {
      name: 'created_at',
      displayName: 'Fecha',
      type: 'date'
    },
    {
      name: 'number_days',
      displayName: 'Días sin trabajar',
      type: 'number'
    }
  ];
  public static attributeLabelsDocuments: object[] = [
    {
      name: 'label',
      displayName: 'Documento',
      type: 'string'
    },
    {
      name: 'vehicle_id',
      displayName: 'Contrato',
      type: 'plateGlobal'
    },
    {
      name: 'expiration_date',
      displayName: 'Vencimiento',
      type: 'date'
    },
    {
      name: 'download',
      displayName: 'Descargar',
      type: 'download'
    }
  ];

}
