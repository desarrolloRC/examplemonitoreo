import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AuthGuard} from '../../guards/auth.guard';
import {RiskDashboardMainComponent} from './components/risk-dashboard-main/risk-dashboard-main.component';

const routes: Routes = [
  {
    path: 'risk-dashboard', component: RiskDashboardMainComponent, canActivate: [AuthGuard], data: {index : 0},
  },
  {
    path: 'accident-dashboard', component: RiskDashboardMainComponent, canActivate: [AuthGuard], data: {index : 1},
  },
  {
    path: 'ticket-dashboard', component: RiskDashboardMainComponent, canActivate: [AuthGuard], data: {index : 2},
  },
  {
    path: 'holiday-dashboard', component: RiskDashboardMainComponent, canActivate: [AuthGuard], data: {index : 4},
  },
  {
    path: 'sickness-dashboard', component: RiskDashboardMainComponent, canActivate: [AuthGuard], data: {index : 3},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class RiskRouting {

}
