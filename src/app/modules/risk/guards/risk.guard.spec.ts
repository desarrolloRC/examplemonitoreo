import { TestBed, async, inject } from '@angular/core/testing';

import { RiskGuard } from './risk.guard';

describe('RiskGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RiskGuard]
    });
  });

  it('should ...', inject([RiskGuard], (guard: RiskGuard) => {
    expect(guard).toBeTruthy();
  }));
});
