import {Pipe, PipeTransform} from '@angular/core';
import {DriverService} from '../../shared/services/driver.service';
import {DomSanitizer} from '@angular/platform-browser';
import {TimeHelper} from '../../../helpers/TimeHelper';
import { suffixesPipe } from './currencyCustomFull.pipe';
import { JwtHelper } from 'src/app/helpers/JwtHelper';

@Pipe({
  name: 'safe'
})

export class SafePipe implements PipeTransform {
  private result;

  constructor(private _driverService: DriverService, protected sanitizer: DomSanitizer) {
  }

  transform(value: any, row, column): Promise<any> {
    if (column.type === 'date') {
      const date = new Date(row[column.name].replace('.000Z', ''));
      const year = date.getFullYear();
      const day = date.getDate();
      let month = date.getMonth();
      month = month + 1;

      this.result = year + '-' + month + '-' + day;
    } else if (column.type === 'dateZone') {
      const date = new Date(value.replace('.000Z', ''));
      const year = date.getFullYear();
      const day = date.getDate();
      let month = date.getMonth();
      month = month + 1;
      this.result = year + '-' + month + '-' + day;
      // this.result = TimeHelper.getZoneTime(value);
      console.log(this.result);
    } else if (column.type === 'number') {
      this.result = Math.round(row[column.name]).toLocaleString();
    } else if (column.type === 'percent') {
      this.result = Math.round(row[column.name]).toLocaleString() + '%';
    } else if (column.type === 'km') {
      if (typeof row[column.name] === 'undefined' || row[column.name] === null) {
        row[column.name] = 0;
      }
      this.result = row[column.name] + ' Km';
    } else if (column.type === 'percentage') {
      this.result = Math.round(row[column.name] * 100).toLocaleString() + '%';
    } else if (column.type === 'percentScore') {
      const valueScore = row[column.name];
      if (valueScore <= 35) {
        this.result = `<span class="down-score">${Math.round(row[column.name]).toLocaleString()}%</span>`;
      } else {
        this.result = `<span class="">${Math.round(row[column.name]).toLocaleString()}%</span>`;
      }
      this.result = this.sanitizer.bypassSecurityTrustHtml(this.result);
    } else if (column.type === 'download') {
      this.result = `<a target="_blank" href="${row[column.name]}"><img class="img-download" src="assets/icons_name/descargas.svg"></a>`;
      this.result = this.sanitizer.bypassSecurityTrustHtml(this.result);
    } else if (column.type === 'currency') {
      this.result = this.currencyTransform(row[column.name]);
    }  else if (column.type === 'meters') {
      this.result = parseInt(row[column.name], 0) > 0 ? Math.round(row[column.name] / 1000).toLocaleString() : 'sin info';
    } else if (column.type === 'statusScore') {
        const valueRnd = row[column.name];
        if (valueRnd === 1) {
          this.result = '<span class="up-score">Subió</span>';
        } else if (valueRnd === -1) {
          this.result = '<span class="down-score">Bajó</span>';
        } else if (valueRnd === 0) {
          this.result = '<span class="normal-score">Sin cambios</span>';
        }

        
      this.result = this.sanitizer.bypassSecurityTrustHtml(this.result);
    } else if (column.type === 'plateGlobal') {
      const plate  = row[column.name].split('|');
      this.result = `<a class="globalActivateSearch" data-id="${plate[1]}|${plate[0]}|${plate[2]}">${plate[2]}</a>`;
      this.result = this.sanitizer.bypassSecurityTrustHtml(this.result);
    } else if (column.type === 'location') {
      
      const date  = row[column.name].replace('.000Z', '');
      let classGps = 'red';
      if (date != '-1') {
        const date1 = new Date(date);
        let diff = Math.abs(date1.getTime() - new Date().getTime()) / 3600000;
        if (diff < 5) {
          classGps = 'green';
        } else if (diff < 7) {
          classGps = 'yellow';
        }
        this.result = `<div class='gps-icon gps-icon-${classGps}'><img src="assets/icons_name/satellite.svg"></div>`;
        this.result = this.sanitizer.bypassSecurityTrustHtml(this.result);
      } else {
        this.result = `<div class='gps-icon gps-icon-${classGps}'><img src="assets/icons_name/satellite.svg"></div>`;
        this.result = this.sanitizer.bypassSecurityTrustHtml(this.result);
      }
      
    } else if (column.type === 'refinanced') {
      if (row[column.name] === true) {
        this.result = 'Si';
      } else {
        this.result = 'No';
      }
    } else {
      this.result = row[column.name];
    }
    return this.result;
  }

  public currencyTransform(input: any, decimal?: any, thousand?: any, symbol?: any): any {
    const token = sessionStorage.getItem('token');

    if (token !== null && typeof token !== 'undefined') {
      const currency = JwtHelper.getCurrency(token);
      symbol = currency['symbol'];
      thousand = currency['thousands_separator'];
      decimal = currency['decimal_separator'];
    }
    if (typeof symbol === 'undefined') {
      symbol = '';
    } else {
      symbol = symbol + ' ';
    }
    return symbol + this.formatNumber(input, decimal, thousand);


  }

  private formatNumber(num, decimal, thousands) {
    num = num.toString().replace(/\.0$/,'');
    if (decimal === ',') {
      num = num.replace('.', ',');
    }

    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${thousands}`)
  }

}
