import { Pipe, PipeTransform } from '@angular/core';
import { JwtHelper } from 'src/app/helpers/JwtHelper';

@Pipe({
  name: 'suffPipe'
})
export class suffixesPipe implements PipeTransform {

  transform(input: any, decimal?: any, thousand?: any, symbol?: any): any {
    const token = sessionStorage.getItem('token');

    if (token !== null && typeof token !== 'undefined') {
      const currency = JwtHelper.getCurrency(token);
      symbol = currency['symbol'];
      thousand = currency['thousands_separator'];
      decimal = currency['decimal_separator'];
    }
    if (typeof symbol === 'undefined') {
      symbol = '';
    } else {
      symbol = symbol + ' ';
    }
    return symbol + this.formatNumber(input, decimal, thousand);


  }



  private formatNumber(num, decimal, thousands) {
    num = parseFloat(num);
    num = num.toFixed(2);
    num = num.toString().replace(/\.0$/,'');
    if (decimal === ',') {
      num = num.replace('.', ',');
    }
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${thousands}`)
  }
}