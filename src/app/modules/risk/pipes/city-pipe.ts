import {Pipe, PipeTransform} from '@angular/core';

import {DriverService} from '../../shared/services/driver.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({name: 'getCellValue'})
export class GetCellValue implements PipeTransform {
  private result;

  constructor(private _driverService: DriverService, protected sanitizer: DomSanitizer) {
  }

  transform(value: any, row, column): any {
    if (column.type === 'date') {
      const date = new Date(row[column.name]);
      const year = date.getFullYear();
      const day = date.getDate();
      let month = date.getMonth();
      month = month + 1;

      this.result = year + '-' + month + '-' + day;
    } else if (column.type === 'number') {
      this.result = Math.round(row[column.name]);
    } else {
      this.result = row[column.name];
    }
    return this.sanitizer.bypassSecurityTrustHtml(this.result);
  }
}
