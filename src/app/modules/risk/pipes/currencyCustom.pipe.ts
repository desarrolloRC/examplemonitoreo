import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';
import { JwtHelper } from 'src/app/helpers/JwtHelper';
@Pipe({
  name: 'dateFormat'
})
export class ThousandSuffixesPipe implements PipeTransform {

  transform(input: any, header?: string): any {
    const token = sessionStorage.getItem('token');
    if (header.indexOf('Monto') !== -1 || header.indexOf('Deuda') !== -1 || header.indexOf('Valor') !== -1 || header.indexOf('Estima') !== -1) {
      if (token !== null && typeof token !== 'undefined') {
        const currency = JwtHelper.getCurrency(token);
        const symbol = currency['symbol'];
        const thousand = currency['thousands_separator'];
        const decimal = currency['decimal_separator'];
        input = symbol + this.formatNumber(input, decimal, thousand);
      }
    }
    if (typeof input === 'string') {
      input = input.replace('T00:00:00.000Z', '');
    }
    return input;
  }

  private formatNumber(num, decimal, thousands) {
    num = parseFloat(num);
    num = num.toFixed(2);
    num = num.toString().replace(/\.0$/,'');
    if (decimal === ',') {
      num = num.replace('.', ',');
    }
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${thousands}`)
  }

  DateToJSDate(dateSent) {
    dateSent = dateSent.replace('T00:00:00.000Z', '');
    const date = moment(dateSent,
      ['YYYY-MM-DD'], true);
    if (date.isValid()) {
      return date.format('YYYY-MM-DD');
    } else {
      return dateSent;
    }
  }

}
