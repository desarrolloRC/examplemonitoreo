import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {MatTabsModule} from '@angular/material/tabs';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';

import {RiskRouting} from './risk.routing';
import {SharedModule} from '../shared/shared.module';
import {RiskDashboardMainComponent} from './components/risk-dashboard-main/risk-dashboard-main.component';
import {StorageService} from '../shared/services/storage.service';
import {I18nHelper} from '../../helpers/I18nHelper';

@NgModule({
  declarations: [RiskDashboardMainComponent],
  imports: [
    RiskRouting,
    CommonModule,
    SharedModule,
    MatTabsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      isolate: false
    }),
  ],
  exports: [
    TranslateModule
  ]
})

export class RiskModule {
  constructor(private translate: TranslateService, storage: StorageService) {
    I18nHelper.setUp(translate, storage);
  }
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
