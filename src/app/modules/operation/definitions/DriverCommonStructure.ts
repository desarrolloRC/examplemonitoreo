export class DriverCommonStructure {
  public static speedAttributeLabels: object[] = [
    {
      name: 'plate_id',
      displayName: 'Placa',
      type: 'location'
    },
    {
      name: 'speed',
      displayName: 'N° de veces que se excedio el limite de velocidad',
      type: 'number'
    },
  ];

  public static travelAttributeLabels: object[] = [
    {
      name: 'plate_id',
      displayName: 'Placa',
      type: 'plateGlobal'
    },
    {
      name: 'odometer',
      displayName: 'Kms recorridos',
      type: 'meters'
    },
  ];

  public static scoreAttributeLabels: object[] = [
    {
      name: 'owner_contract_id',
      displayName: 'Crédito',
      type: 'plateGlobal'
    },
    {
      name: 'score',
      displayName: 'Score',
      type: 'percentScore'
    },
    {
      name: 'status',
      displayName: 'Tendencia',
      type: 'statusScore'
    },
  ];

  public static DriverRankingAttributeLabels: object[] = [
    {
      name: 'information', // display icon information
      displayName: 'Actividad',
      nameExport: '',
      type: 'information',
      hover: false,
    },
    {
      name: 'contract_number',
      displayName: 'home.credito',
      nameExport: 's.contract_number as Credito,',
      type: 'string',
      hover: true,
    },
    {
      name: 'driver',
      displayName: 'home.name',
      nameExport: 's.driver as Nombre,',
      hover: true,
      type: 'string'
    },
    {
      name: 'harvest',
      displayName: 'home.harvest',
      nameExport: 'coalesce(lastvalue.harvest, 0) as Cosecha, ',
      hover: true,
      type: 'string'
    },
    {
      name: 'amount_tickets_pending',
      displayName: 'home.amount_tickets',
      nameExport: 'coalesce(lastvalue.amount_tickets_pending, 0) as Monto_Papeletas,',
      hover: true,
      type: 'currency'
    },
    {
      name: 'tickets_pending',
      displayName: 'home.tickets_pending',
      nameExport: 'coalesce(lastvalue.tickets_pending, 0) as Numero_Papeletas,',
      hover: true,
      type: 'string'
    },
    {
      name: 'documents_expired',
      displayName: 'home.documents_expired',
      nameExport: 'coalesce(lastvalue.documents_expired, 0) as Documentos_vencidos,',
      hover: true,
      type: 'string'
    },
    {
      name: 'documents_on_risk',
      displayName: 'home.documents_on_risk',
      nameExport: 'coalesce(lastvalue.documents_on_risk, 0) as Documentos_por_vencer,',
      hover: true,
      type: 'string'
    },
    {
      name: 'debt_amount',
      displayName: 'home.debt_amount',
      nameExport: 'coalesce(lastvalue.debt_amount, 0) as Deuda_Monto,',
      hover: true,
      type: 'currency'
    },
    {
      name: 'debt_days',
      displayName: 'home.days',
      nameExport: 'coalesce(lastvalue.debt_days, 0) as Deuda_Dias,',
      hover: true,
      type: 'string'
    },
    {
      name: 'last_check_score',
      displayName: 'home.last_check_score',
      nameExport: 'coalesce(lastvalue.last_check_score, 0) as Ult_Insp_Puntaje,',
      hover: true,
      type: 'percentage'
    },
    {
      name: 'last_check_date',
      displayName: 'home.last_check_date',
      nameExport: 'coalesce(lastvalue.last_check_date, \'Sin info\') as Ult_Insp_Fecha,',
      hover: true,
      type: 'string'
    },
    {
      name: 'ranking',
      displayName: 'home.ranking',
      nameExport: 'coalesce(lastvalue.ranking, 0) as Puntaje',
      hover: true,
      type: 'string'
    },
    {
      name: 'actions',
      displayName: 'home.actions',
      hover: true,
      type: 'string'
    },
  ];

  public static DrivingRankingFinsusAttributeLabels: object[] = [
    {
      name: 'information', // display icon information
      displayName: 'Actividad',
      nameExport: '',
      type: 'information',
      hover: false,
    },
    {
      name: 'contract_number',
      displayName: 'home.credito',
      nameExport: 's.contract_number as Credito,',
      type: 'string',
      hover: true,
    },
    {
      name: 'driver',
      displayName: 'home.name',
      nameExport: 's.driver as Nombre,',
      hover: true,
      type: 'string'
    },
    {
      name: 'harvest',
      displayName: 'home.harvest',
      nameExport: 'coalesce(lastvalue.harvest, 0) as Cosecha, ',
      hover: true,
      type: 'string'
    },
    {
      name: 'amount_tickets_pending',
      displayName: 'home.amount_tickets',
      nameExport: 'coalesce(lastvalue.amount_tickets_pending, 0) as Monto_Papeletas,',
      hover: true,
      type: 'currency'
    },
    {
      name: 'ranking',
      displayName: 'home.ranking',
      nameExport: 'coalesce(lastvalue.ranking, 0) as Puntaje',
      hover: true,
      type: 'string'
    },
    {
      name: 's.initial_debt',
      displayName: 'Capital inicial',
      nameExport: 'coalesce(s.initial_debt, 0) as initial_inversion',
      hover: true,
      type: 'currency'
    },
    {
      name: 's.bucket',
      displayName: 'Bucket',
      nameExport: `coalesce(s.bucket, '') as bucket`,
      hover: true,
      type: 'string'
    },
    {
      name: 'debt_days',
      displayName: 'home.days',
      nameExport: 'coalesce(lastvalue.debt_days, 0) as Deuda_Dias,',
      hover: true,
      type: 'string'
    },
    {
      name: 'debt_amount',
      displayName: 'Deuda actual',
      nameExport: 'coalesce(lastvalue.debt_amount, 0) as Deuda_Monto,',
      hover: true,
      type: 'currency'
    },
    {
      name: 's.accrued_interest',
      displayName: 'Interes Devengado',
      nameExport: `coalesce(s.accrued_interest, 0) as accrued_interest`,
      hover: true,
      type: 'currency'
    },
    {
      name: 's.past_due_balance',
      displayName: 'Deuda anterior',
      nameExport: `coalesce(s.past_due_balance, 0) as past_due_balance`,
      hover: true,
      type: 'currency'
    },
    {
      name: 's.reserve',
      displayName: 'Estimación',
      nameExport: `coalesce(s.reserve, 0)`,
      hover: true,
      type: 'currency'
    },
    {
      name: 'actions',
      displayName: 'home.actions',
      hover: true,
      type: 'string'
    },
  ];

  public static PaymentHomeAttributeLabels: object[] = [
    {
      name: 'information', // display icon information
      displayName: 'Actividad',
      type: 'information',
      hover: false,
    },
    {
      name: 'contract_number',
      displayName: 'home.credito',
      nameExport: 's.contract_number as Credito,',
      type: 'string',
      hover: true,
    },
    {
      name: 'driver',
      displayName: 'home.name',
      nameExport: 's.driver as Nombre,',
      hover: true,
      type: 'string'
    },
    {
      name: 'harvest',
      displayName: 'home.harvest',
      nameExport: 'coalesce(lastvalue.harvest, 0) as Cosecha, ',
      hover: true,
      type: 'string'
    },
    {
      name: 'payment_freuency',
      displayName: 'payment.frecuency',
      nameExport: 'coalesce(lastvalue.payment_freuency, 0) as Frecuencia_de_pago,',
      hover: true,
      type: 'string'
    },
    {
      name: 'payment_percentage',
      displayName: 'payment.percentage',
      nameExport: 'coalesce(lastvalue.payment_percentage, 0) as Porcentaje_pago_Actual,',
      hover: true,
      type: 'string'
    },
    {
      name: 'ideal_payment_percentage',
      displayName: 'payment.ideal_percentage',
      nameExport: 'coalesce(lastvalue.ideal_payment_percentage, 0) as Porcentaje_pago_Ideal,',
      hover: true,
      type: 'string'
    },
    {
      name: 'debt_amount',
      displayName: 'home.debt_amount',
      nameExport: 'coalesce(lastvalue.debt_amount, 0) as Deuda_Monto,',
      hover: true,
      type: 'currency'
    },
    {
      name: 'debt_days',
      displayName: 'home.days',
      nameExport: 'coalesce(lastvalue.debt_days, 0) as Deuda_dias,',
      hover: true,
      type: 'string'
    },
    {
      name: 'kilometers',
      displayName: 'payment.kilometers',
      nameExport: 'agg.kilometers as Kilometros,',
      hover: true,
      type: 'km'
    },
    {
      name: 'kilometers_ideal',
      displayName: 'payment.kilometers_ideal',
      nameExport: 'agg.kilometers_ideal as Kilometraje_Ideal,',
      hover: true,
      type: 'km'
    },
    {
      name: 'ranking',
      displayName: 'home.ranking',
      nameExport: 'coalesce(lastvalue.ranking, 0) as Puntaje',
      hover: true,
      type: 'string'
    },
    {
      name: 'actions',
      displayName: 'home.actions',
      hover: true,
      type: 'string'
    },
  ];

  public static VehiclesHomeAttributeLabels: object[] = [
    {
      name: 'information', // display icon information
      displayName: 'Actividad',
      type: 'information',
      hover: false,
    },
    {
      name: 'contract_number',
      displayName: 'home.credito',
      nameExport: 's.contract_number as Credito,',
      type: 'string',
      hover: true,
    },
    {
      name: 'driver',
      displayName: 'home.name',
      nameExport: 's.driver as Nombre,',
      hover: true,
      type: 'string'
    },
    {
      name: 'harvest',
      displayName: 'home.harvest',
      nameExport: 'coalesce(lastvalue.harvest, 0) as Cosecha, ',
      hover: true,
      type: 'string'
    },
    {
      name: 'last_check_score',
      displayName: 'Calificación',
      nameExport: 'coalesce(lastvalue.last_check_score, 0) as Calificación,',
      hover: true,
      type: 'percentage'
    },
    {
      name: 'last_check_date',
      displayName: 'home.last_check_date',
      nameExport: 'coalesce(lastvalue.last_check_date, \'Sin info\') as Ult_Insp_Fecha,',
      hover: true,
      type: 'string'
    },
    {
      name: 'avg_check_score',
      displayName: 'vehicle.avg_check_score',
      nameExport: 'agg.avg_check_score as Promedio_calificación,',
      hover: true,
      type: 'percentage'
    },
    {
      name: 'kilometers',
      displayName: 'payment.kilometers',
      nameExport: 'agg.kilometers as kilometros,',
      hover: true,
      type: 'km'
    },
    {
      name: 'kilometers_ideal',
      displayName: 'payment.kilometers_ideal',
      nameExport: 'agg.kilometers_ideal as Kilometros_Ideal,',
      hover: true,
      type: 'km'
    },
    {
      name: 'ranking',
      displayName: 'home.ranking',
      nameExport: 'coalesce(lastvalue.ranking, 0) as Puntaje',
      hover: true,
      type: 'string'
    },
    {
      name: 'actions',
      displayName: 'home.actions',
      hover: true,
      type: 'string'
    },
  ];

  public static BehaviourHomeAttributeLabels: object[] = [
    {
      name: 'information', // display icon information
      displayName: 'Actividad',
      type: 'information',
      hover: false,
    },
    {
      name: 'contract_number',
      displayName: 'home.credito',
      nameExport: 's.contract_number as Credito,',
      type: 'string',
      hover: true,
    },
    {
      name: 'driver',
      displayName: 'home.name',
      nameExport: 's.driver as Nombre,',
      hover: true,
      type: 'string'
    },
    {
      name: 'harvest',
      displayName: 'home.harvest',
      nameExport: 'coalesce(lastvalue.harvest, 0) as Cosecha, ',
      hover: true,
      type: 'string'
    },
    {
      name: 'amount_tickets_pending',
      displayName: 'home.amount_tickets',
      nameExport: 'coalesce(lastvalue.amount_tickets_pending, 0) as Monto_Papeletas,',
      hover: true,
      type: 'currency'
    },
    {
      name: 'tickets_pending',
      displayName: 'home.tickets_pending',
      nameExport: 'coalesce(lastvalue.tickets_pending, 0) as Numero_Papeletas,',
      hover: true,
      type: 'string'
    },
    {
      name: 'documents_expired',
      displayName: 'home.documents_expired',
      nameExport: 'coalesce(lastvalue.documents_expired, 0) as Documentos_vencidos,',
      hover: true,
      type: 'string'
    },
    {
      name: 'documents_on_risk',
      displayName: 'home.documents_on_risk',
      nameExport: 'coalesce(lastvalue.documents_on_risk, 0) as Documntos_por_vencer,',
      hover: true,
      type: 'string'
    },
    {
      name: 'kilometers',
      displayName: 'payment.kilometers',
      nameExport: 'agg.kilometers as Kilometros,',
      hover: true,
      type: 'km'
    },
    {
      name: 'top_speed',
      displayName: 'vehicle.speed',
      nameExport: 'agg.top_speed as Kilometros_Ideal,',
      hover: true,
      type: 'km'
    },
    {
      name: 'engine_hours',
      displayName: 'vehicle.engine_hours',
      nameExport: 'lastvalue.engine_hours as Hora_motor,',
      hover: true,
      type: 'string'
    },
    {
      name: 'ranking',
      displayName: 'home.ranking',
      nameExport: 'coalesce(lastvalue.ranking, 0) as Puntaje',
      hover: true,
      type: 'string'
    },
    {
      name: 'actions',
      displayName: 'home.actions',
      hover: true,
      type: 'string'
    },
  ];

  public static AlertsAttributeLabels: object[] = [
    {
      name: 'contract_number',
      displayName: 'Crédito',
      type: 'string',
      hover: true,
    },
    {
      name: 'tickets_pending',
      displayName: 'Plata',
      hover: true,
      type: 'string'
    },
    {
      name: 'actions',
      displayName: 'Actions',
      hover: true,
      type: 'string'
    },
  ];

  public static scoreFullAttributeLabels: object[] = [
    {
      name: 'owner_contract_id',
      displayName: 'Crédito',
      type: 'plateGlobal'
    },
    {
      name: 'score',
      displayName: 'Score',
      type: 'percent'
    },
    {
      name: 'status',
      displayName: 'Estado',
      type: 'statusScore'
    },
    {
      name: 'description',
      displayName: 'Descripción',
      type: 'string'
    },
  ];

  public static scoreMiniAttributeLabels: object[] = [
    {
      name: 'owner_contract_id',
      displayName: 'Crédito',
      type: 'plateGlobal'
    },
    {
      name: 'score',
      displayName: 'Score',
      type: 'percent'
    },
    {
      name: 'status',
      displayName: 'Estado',
      type: 'statusScore'
    },
  ];
}
