export class OperationNavStructure {
  public static OperationNavStructure: object[] = [
    {title: 'Kilometros', link: '/driver/kilometers'},
    {title: 'Velocidad', link: '/driver/speed'},
    {title: 'Score', link: '/driver/score'},
    {title: 'Ubicación', link: '/driver/location'},
  ];
}
