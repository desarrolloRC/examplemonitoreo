import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {JwtHelper} from '../../../helpers/JwtHelper';
import {environment} from '../../../../environments/environment';
import {StorageService} from '../../shared/services/storage.service';
import {SearchGlobalParams} from '../../shared/interfaces/SharedInterface';

@Injectable({
  providedIn: 'root'
})
export class OperationDashboardService {
  constructor(private http: HttpClient,
              private storageService: StorageService) {
  }

  public getDataCollect(params: Object): Observable<object> {
    return this.http.post(environment.urlCollect + 'get-collect-data', params);
  }

  public getPreventivesOptions(): Observable<object> {
    return this.http.get(environment.urlMaintenance + 'get-preventives-options');
  }

  public getPreventivesByType(params: Object): Observable<object> {
    return this.http.post(environment.urlMaintenance + 'get-preventives-data', params);
  }

  public getPlateById(params: Object): Observable<object> {
    return this.http.post(environment.urlDriver + 'get-plate-by-vehicle-id', params);
  }

  public getVehiclesOn(): Observable<object> {
    const data: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    let globalSearch;
    const startDate = data['startDate'];
    const endDate = data['endDate'];

    if (typeof data['search'] === 'string' && data['search'] !== '') {
      globalSearch = data['search'];
    }

    let requestUrl = `${environment.urlDriver}vehicles-on?startDate=${startDate}&endDate=${endDate}`;

    if (typeof globalSearch !== 'undefined') {
      requestUrl = requestUrl + `&globalSearch=${globalSearch}`;
    }

    if (typeof data.harvest !== 'undefined' && data.harvest !== null && data.harvest !== '') {
      requestUrl = requestUrl + `&harvest=${data.harvest}`;
    }

    return this.http.get(requestUrl);
  }

  public getVehicleSpeedLimitAvg(): Observable<object> {
    const data: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    let globalSearch;
    const startDate = data['startDate'];
    const endDate = data['endDate'];

    if (typeof data['search'] === 'string' && data['search'] !== '') {
      globalSearch = data['search'];
    }

    let requestUrl = `
    ${environment.urlDriver}vehicles-speed-limit-avg?startDate=${startDate}&endDate=${endDate}
    `;

    if (typeof globalSearch !== 'undefined') {
      requestUrl = requestUrl + `&globalSearch=${globalSearch}`;
    }

    if (typeof data.harvest !== 'undefined' && data.harvest !== null && data.harvest !== '') {
      requestUrl = requestUrl + `&harvest=${data.harvest}`;
    }

    return this.http.get(requestUrl);
  }

  public getVehicleDistanceAvg(): Observable<object> {
    const data: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    let globalSearch;
    const startDate = data['startDate'];
    const endDate = data['endDate'];

    if (typeof data['search'] === 'string' && data['search'] !== '') {
      globalSearch = data['search'];
    }

    let requestUrl = `${environment.urlDriver}vehicles-distance-avg?startDate=${startDate}&endDate=${endDate}`;


    if (typeof globalSearch !== 'undefined') {
      requestUrl = requestUrl + `&globalSearch=${globalSearch}`;
    }

    if (typeof data.harvest !== 'undefined' && data.harvest !== null && data.harvest !== '') {
      requestUrl = requestUrl + `&harvest=${data.harvest}`;
    }

    return this.http.get(requestUrl);
  }

  public getVehiclesCount() {
    const enterprise = JwtHelper.getEnterprise(sessionStorage.getItem('token'));
    const data: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    return this.http.post(`${environment.urlDriver}get-plates-by-enterprise`, {
      params: {
        enterpriseId: enterprise,
        data
      }
    });
  }
}
