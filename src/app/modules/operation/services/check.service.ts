import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';
import {SearchGlobalParams} from '../../shared/interfaces/SharedInterface';
import { StorageService } from '../../shared/services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class CheckService {

  constructor(private http: HttpClient, private storageService: StorageService) { }

  public getCheckInfo(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    return this.http.post(environment.urlChecking + 'get-inspection-info', params);
  }

  public getCheckInfoReport(): Observable<object> {

    const params = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
      score: 90
    };

    return this.http.post(environment.urlChecking + 'get-inspection-info-report', params);
  }

  public getVariableInfo(variable): Observable<object> {

    const params = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
      variable: variable
    };

    return this.http.post(environment.urlChecking + 'get-variable-avg', params);
  }
}
