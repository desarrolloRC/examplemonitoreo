import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../../../environments/environment';
import {SearchGlobalParams} from '../../shared/interfaces/SharedInterface';
import { StorageService } from '../../shared/services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class CollectService {

  constructor(private http: HttpClient, private storageService: StorageService) { }

  public getOwnershipVehicle(): Observable<object> {
    return this.http.get(environment.urlCollect + 'get-ownership-vehicle');
  }
  public getInstallmentDataTable(): Observable<object> {
    const url = `${environment.urlCollect}get-installments-all?page=1&size=5&indexed=false`;

    const data: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    let globalSearch;
    const startDate = data['startDate'];
    const endDate = data['endDate'];

    if (typeof data['search'] === 'string' && data['search'] !== '') {
      globalSearch = data['search'];
    }

    let requestUrl = `
    ${url}&startDate=${startDate}&endDate=${endDate}
    `;

    if (typeof globalSearch !== 'undefined') {
      requestUrl = requestUrl + `&globalSearch=${globalSearch}`;
    }

    if (typeof data.harvest !== 'undefined' && data.harvest !== null && data.harvest !== '') {
      requestUrl = requestUrl + `&harvest=${data.harvest}`;
    }

    return this.http.get(requestUrl);
  }
  public getInstallmentData(params: SearchGlobalParams): Observable<object> {
    return this.http.post(environment.urlCollect + 'get-installment-data-dashboard', params);
  }

  public savePaymentsExcel(params): Observable<object> {
    return this.http.post(environment.urlCollect + 'save-payments-excel', params);
  }

  public getDebtInfo(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    return this.http.post(environment.urlCollect + 'get-collect-data-widget', params);
  }

  public getPaymentData(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    return this.http.post(environment.urlCollect + 'get-payment-data-widget', params);
  }

  public getPaymentIdeal(): Observable<object> {

    const params: SearchGlobalParams = {
      startDate: this.storageService.getItem('globalSearchStartDate'),
      endDate: this.storageService.getItem('globalSearchEndDate'),
      search: this.storageService.getItem('globalSearch'),
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };

    return this.http.post(environment.urlCollect + 'get-payment-ideal-widget', params);
  }
}
