import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {DriverSpeedComponent} from './components/driver-speed/driver-speed.component';
import {DriverScoreComponent} from './components/driver-score/driver-score.component';
import {DriverLocationComponent} from './components/driver-location/driver-location.component';
import {DriverDashboardComponent} from './components/driver-dashboard/driver-dashboard.component';
import {DriverKilometersComponent} from './components/driver-kilometers/driver-kilometers.component';
import {OperationDashboardMainComponent} from './components/operation-dashboard-main/operation-dashboard-main.component';
import {OperationBehaviourChildComponent} from './components/operation-behaviour-child/operation-behaviour-child.component';

const routes: Routes = [
  {path: 'driver-dashboard', component: DriverDashboardComponent, pathMatch: 'full'},
  {path: 'speed', component: DriverSpeedComponent, pathMatch: 'full'},
  {path: 'score', component: DriverScoreComponent, pathMatch: 'full'},
  {path: 'location', component: DriverLocationComponent, pathMatch: 'full'},
  {path: 'kilometers', component: DriverKilometersComponent, pathMatch: 'full'},
  {path: 'operation-dashboard', component: OperationDashboardMainComponent, pathMatch: 'prefix', data: {id: ''} },
  {path: 'collect-dashboard', component: OperationDashboardMainComponent, pathMatch: 'prefix', data: {id: 'collect'} },
  {path: 'check-dashboard', component: OperationDashboardMainComponent, pathMatch: 'prefix', data: {id: 'check'} },
  {path: 'maintenance-dashboard', component: OperationDashboardMainComponent, pathMatch: 'prefix', data: {id: 'maintenance'} },
  {path: 'behaviour-child', component: OperationBehaviourChildComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class OperationRouting {

}
