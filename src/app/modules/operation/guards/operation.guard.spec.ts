import { TestBed, async, inject } from '@angular/core/testing';

import { OperationGuard } from './operation.guard';

describe('OperationGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OperationGuard]
    });
  });

  it('should ...', inject([OperationGuard], (guard: OperationGuard) => {
    expect(guard).toBeTruthy();
  }));
});
