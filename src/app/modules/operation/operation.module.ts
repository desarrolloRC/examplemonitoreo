import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
} from '@angular/material';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {NgxGraphModule} from '@swimlane/ngx-graph';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';

import {SharedModule} from '../shared/shared.module';
import {OperationRouting} from './operation.routing';
import {DriverSpeedComponent} from './components/driver-speed/driver-speed.component';
import {DriverScoreComponent} from './components/driver-score/driver-score.component';
import {DriverLocationComponent} from './components/driver-location/driver-location.component';
import {DriverDashboardComponent} from './components/driver-dashboard/driver-dashboard.component';
import {DriverKilometersComponent} from './components/driver-kilometers/driver-kilometers.component';
import { OperationDashboardMainComponent } from './components/operation-dashboard-main/operation-dashboard-main.component';

import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { OperationDashboardPreventiveComponent } from './components/operation-dashboard-preventive/operation-dashboard-preventive.component';
import { OperationBehaviourChildComponent } from './components/operation-behaviour-child/operation-behaviour-child.component';
import {ChartsModule} from 'ng2-charts';
import {OperationBehaviourPaymentsComponent} from './components/operation-behaviour-payments/operation-behaviour-payments.component';
import {StorageService} from '../shared/services/storage.service';
import {I18nHelper} from '../../helpers/I18nHelper';

@NgModule({
  declarations: [
    DriverSpeedComponent,
    DriverScoreComponent,
    DriverLocationComponent,
    DriverDashboardComponent,
    DriverKilometersComponent,
    OperationDashboardMainComponent,
    OperationDashboardPreventiveComponent,
    OperationBehaviourChildComponent,
    OperationBehaviourPaymentsComponent

  ],
  imports: [
    CommonModule,
    OperationRouting,
    SharedModule,
    MatCardModule,
    MatTableModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSortModule,
    MatPaginatorModule,
    NgxChartsModule,
    NgxGraphModule,
    MatButtonToggleModule,
    MatTabsModule,
    ChartsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      isolate: false
    }),
],
  exports: [
    TranslateModule
  ]
})
export class OperationModule {
  constructor(private translate: TranslateService, storage: StorageService) {
    I18nHelper.setUp(translate, storage);
  }
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
