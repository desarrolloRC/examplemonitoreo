import {Component, Input, OnInit} from '@angular/core';
import * as shape from 'd3-shape';

@Component({
  selector: 'app-operation-dashboard-check',
  templateUrl: './operation-dashboard-check.component.html',
  styleUrls: ['./operation-dashboard-check.component.css']
})
export class OperationDashboardCheckComponent implements OnInit {

  multi_vertical: any[];

  parentOptions: any[] = [
    {value: 'interior', viewValue: 'Interior'},
    {value: 'exterior', viewValue: 'Exterior'},
    {value: 'bulbs', viewValue: 'Bombillos'},
    {value: 'fluids', viewValue: 'Niveles Fluidos'},
    {value: 'trunk', viewValue: 'Baúl'},
    {value: 'engine', viewValue: 'Motor'},
  ];

  childOptions: any[] = [

    {parent: 'fluids', value: 'liquido_bateria', viewValue: 'Liquido Bateria'},
    {parent: 'fluids', value: 'aceite', viewValue: 'Aceite'},
    {parent: 'fluids', value: 'liquido_refrigerante', viewValue: 'Liquido Refrigerante'},
    {parent: 'fluids', value: 'agua_radiador', viewValue: 'Agua Radiador'},

    {parent: 'interior', value: 'millare', viewValue: 'Millare'},
    {parent: 'interior', value: 'rejillas_aire', viewValue: 'Rejillas Aire'},
    {parent: 'interior', value: 'aire_acondicionado', viewValue: 'Aire Acondicionado'},
    {parent: 'interior', value: 'pantalla_radio', viewValue: 'Pantalla Radio'},

    {parent: 'exterior', value: 'fabricante', viewValue: 'Emblemas de Fabricante'},
    {parent: 'exterior', value: 'antena', viewValue: 'Antena(s)'},

    {parent: 'bulbs', value: 'luces_medias_altas', viewValue: 'Luces | Medias - Altas'},
    {parent: 'bulbs', value: 'luces_exploradoras', viewValue: 'Luces Exploradoras'},
    {parent: 'bulbs', value: 'luces_estacionarias', viewValue: 'Luces Estacionarias'},
    {parent: 'bulbs', value: 'luces_direccionales', viewValue: 'Luces Direccionales'},

    {parent: 'trunk', value: 'linterna', viewValue: 'Linterna'},
    {parent: 'trunk', value: 'llaves_fijas', viewValue: 'Llaves Fijas'},
    {parent: 'trunk', value: 'llave_expansion', viewValue: 'Llave de Expansión'},
    {parent: 'trunk', value: 'destornillador', viewValue: 'Destornillador'},

    {parent: 'engine', value: 'marco_bateria', viewValue: 'Marco Bateria'},
    {parent: 'engine', value: 'frontal', viewValue: 'Frontal'},
    {parent: 'engine', value: 'radiador', viewValue: 'Radiador'},
    {parent: 'engine', value: 'varilla', viewValue: 'Varilla'},
  ];

  childOptionsUsed: any[] = [
    {parent: 'fluids', value: 'liquido_bateria', viewValue: 'Liquido Bateria'},
    {parent: 'fluids', value: 'aceite', viewValue: 'Aceite'},
    {parent: 'fluids', value: 'liquido_refrigerante', viewValue: 'Liquido Refrigerante'},
    {parent: 'fluids', value: 'agua_radiador', viewValue: 'Agua Radiador'},
  ];

  childOption = 'aceite';
  showChart = false;
  public type = 'month';
  @Input() titleShown = true;
  view: any[];
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';
  timeline = true;
  curve: any = shape.curveCardinal;

  colorScheme = {
    domain: ['#1875F0', '#F2994A', '#eb5757', '#11cb87']
  };


  millare: any[] = [
    {name: 'Millare', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 3},
        {name: 'Malo', value: 5}, {name: 'No Aplica', value: 1},
    ]}];
  rejillas: any[] = [
    {name: 'Rejillas Aire', series: [
        {name: 'Bueno', value: 5}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 1}, {name: 'No Aplica', value: 3},
      ]}];
  aire_acondicionado: any[] = [
    {name: 'Aire Acondicionado', series: [
        {name: 'Bueno', value: 1}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 6}, {name: 'No Aplica', value: 1},
      ]}];
  pantalla_radio: any[] = [
    {name: 'Pantalla Radio', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 4},
        {name: 'Malo', value: 1}, {name: 'No Aplica', value: 3},
      ]}];


  emblema: any[] = [
    {name: 'Emblema de Fabricante', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 5},
        {name: 'Malo', value: 2}, {name: 'No Aplica', value: 1},
      ]}];
  antena: any[] = [
    {name: 'Antena(s)', series: [
        {name: 'Bueno', value: 6}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 2}, {name: 'No Aplica', value: 1},
      ]}];


  luces_medias_altas: any[] = [
    {name: 'Luces | Medias - Altas', series: [
        {name: 'Bueno', value: 6}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 2}, {name: 'No Aplica', value: 1},
      ]}];
  luces_exploradoras: any[] = [
    {name: 'Luces Exploradoras', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 4},
        {name: 'Malo', value: 1}, {name: 'No Aplica', value: 3},
      ]}];
  luces_estacionarias: any[] = [
    {name: 'Luces Estacionarias', series: [
        {name: 'Bueno', value: 6}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 2}, {name: 'No Aplica', value: 1},
      ]}];
  luces_direccionales: any[] = [
    {name: 'Luces Direccionales', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 3},
        {name: 'Malo', value: 5}, {name: 'No Aplica', value: 1},
      ]}];


  liquido_bateria: any[] = [
    {name: 'Liquido Bateria', series: [
        {name: 'Bueno', value: 6}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 2}, {name: 'No Aplica', value: 1},
      ]}];
  aceite: any[] = [
    {name: 'Aceite', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 4},
        {name: 'Malo', value: 1}, {name: 'No Aplica', value: 3},
      ]}];
  liquido_refrigerante: any[] = [
    {name: 'Liquido Refrigerante', series: [
        {name: 'Bueno', value: 6}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 2}, {name: 'No Aplica', value: 1},
      ]}];
  agua_radiador: any[] = [
    {name: 'Agua Radiador', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 3},
        {name: 'Malo', value: 5}, {name: 'No Aplica', value: 1},
      ]}];


  linterna: any[] = [
    {name: 'Linterna', series: [
        {name: 'Bueno', value: 6}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 2}, {name: 'No Aplica', value: 1},
      ]}];
  llaves_fijas: any[] = [
    {name: 'Llaves Fijas', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 4},
        {name: 'Malo', value: 1}, {name: 'No Aplica', value: 3},
      ]}];
  llave_expansion: any[] = [
    {name: 'Llave de Expansion', series: [
        {name: 'Bueno', value: 6}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 2}, {name: 'No Aplica', value: 1},
      ]}];
  destornillador: any[] = [
    {name: 'Destornillador', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 3},
        {name: 'Malo', value: 5}, {name: 'No Aplica', value: 1},
      ]}];


  marco_bateria: any[] = [
    {name: 'Marco Bateria', series: [
        {name: 'Bueno', value: 6}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 2}, {name: 'No Aplica', value: 1},
      ]}];
  frontal: any[] = [
    {name: 'Frontal', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 4},
        {name: 'Malo', value: 1}, {name: 'No Aplica', value: 3},
      ]}];
  radiador: any[] = [
    {name: 'Radiador', series: [
        {name: 'Bueno', value: 6}, {name: 'Regular', value: 1},
        {name: 'Malo', value: 2}, {name: 'No Aplica', value: 1},
      ]}];
  varilla: any[] = [
    {name: 'Varilla', series: [
        {name: 'Bueno', value: 2}, {name: 'Regular', value: 4},
        {name: 'Malo', value: 1}, {name: 'No Aplica', value: 3},
      ]}];

  ngOnInit() {
    this.showChart = true;
    setTimeout(() => {
      this.changeChild('aceite');
    }, 2000);
  }

  changeParent(valueSelected) {
    this.childOptionsUsed = [];
    this.childOptions.map((value) => {
      if (value.parent === valueSelected) {
        this.childOptionsUsed.push(value);
        this.childOption = value.value;
        this.changeChild(value.value);
      }
    });
  }

  changeChild(valueSelected) {
    switch (valueSelected) {
      case 'millare': this.multi_vertical = this.millare; break;
      case 'rejillas_aire': this.multi_vertical = this.rejillas; break;
      case 'aire_acondicionado': this.multi_vertical = this.aire_acondicionado; break;
      case 'pantalla_radio': this.multi_vertical = this.pantalla_radio; break;

      case 'fabricante': this.multi_vertical = this.emblema; break;
      case 'antena': this.multi_vertical = this.antena; break;

      case 'luces_medias_altas': this.multi_vertical = this.luces_medias_altas; break;
      case 'luces_exploradoras': this.multi_vertical = this.luces_exploradoras; break;
      case 'luces_estacionarias': this.multi_vertical = this.luces_estacionarias; break;
      case 'luces_direccionales': this.multi_vertical = this.luces_direccionales; break;

      case 'liquido_bateria': this.multi_vertical = this.liquido_bateria; break;
      case 'aceite': this.multi_vertical = this.aceite; break;
      case 'liquido_refrigerante': this.multi_vertical = this.liquido_refrigerante; break;
      case 'agua_radiador': this.multi_vertical = this.agua_radiador; break;

      case 'linterna': this.multi_vertical = this.linterna; break;
      case 'llaves_fijas': this.multi_vertical = this.llaves_fijas; break;
      case 'llave_expansion': this.multi_vertical = this.llave_expansion; break;
      case 'destornillador': this.multi_vertical = this.destornillador; break;

      case 'marco_bateria': this.multi_vertical = this.marco_bateria; break;
      case 'frontal': this.multi_vertical = this.frontal; break;
      case 'radiador': this.multi_vertical = this.radiador; break;
      case 'varilla': this.multi_vertical = this.varilla; break;
    }
  }

}
