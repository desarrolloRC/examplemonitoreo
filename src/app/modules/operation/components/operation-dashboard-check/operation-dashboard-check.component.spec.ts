import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationDashboardCheckComponent } from './operation-dashboard-check.component';

describe('OperationDashboardCheckComponent', () => {
  let component: OperationDashboardCheckComponent;
  let fixture: ComponentFixture<OperationDashboardCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationDashboardCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationDashboardCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
