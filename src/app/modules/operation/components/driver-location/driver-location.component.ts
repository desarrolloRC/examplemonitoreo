import { Component, OnInit } from '@angular/core';

import {OperationNavStructure} from '../../definitions/OperationNavStructure';

@Component({
  selector: 'app-driver-location',
  templateUrl: './driver-location.component.html',
  styleUrls: ['./driver-location.component.css']
})
export class DriverLocationComponent implements OnInit {
  public navLinks: object[];

  constructor() { }

  ngOnInit() {
    this.navLinks = OperationNavStructure.OperationNavStructure;

  }

}
