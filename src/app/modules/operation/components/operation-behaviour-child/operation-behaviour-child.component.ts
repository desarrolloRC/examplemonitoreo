import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {RiskCommonStructure} from '../../../risk/definitions/RiskCommonStructure';

@Component({
  selector: 'app-operation-behaviour-child',
  templateUrl: './operation-behaviour-child.component.html',
  styleUrls: ['./operation-behaviour-child.component.css']
})
export class OperationBehaviourChildComponent implements OnInit {


  public attributeLabelsInstallments: object[];

  public collectMicroService = environment.urlCollect;

  ngOnInit(): void {
    this.attributeLabelsInstallments = RiskCommonStructure.attributeLabelsInstallments;
  }

}
