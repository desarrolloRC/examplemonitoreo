import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationBehaviourChildComponent } from './operation-behaviour-child.component';

describe('OperationBehaviourPaymentsComponent', () => {
  let component: OperationBehaviourChildComponent;
  let fixture: ComponentFixture<OperationBehaviourChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationBehaviourChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationBehaviourChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
