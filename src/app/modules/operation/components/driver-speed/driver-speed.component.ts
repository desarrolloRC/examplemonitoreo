import { Component, OnInit } from '@angular/core';

import {environment} from '../../../../../environments/environment';
import {DriverCommonStructure} from '../../definitions/DriverCommonStructure';
import {OperationNavStructure} from '../../definitions/OperationNavStructure';

@Component({
  selector: 'app-driver-speed',
  templateUrl: './driver-speed.component.html',
  styleUrls: ['./driver-speed.component.css']
})
export class DriverSpeedComponent implements OnInit {
  public navLinks: object[];
  public attributeLabels: object[];
  public vehicleMicroService = environment.urlDriver;

  constructor() { }

  ngOnInit() {
    this.attributeLabels = DriverCommonStructure.speedAttributeLabels;
    this.navLinks = OperationNavStructure.OperationNavStructure;
  }

}
