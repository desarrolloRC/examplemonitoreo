import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationBehaviourPaymentsComponent } from './operation-behaviour-payments.component';

describe('OperationBehaviourPaymentsComponent', () => {
  let component: OperationBehaviourPaymentsComponent;
  let fixture: ComponentFixture<OperationBehaviourPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationBehaviourPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationBehaviourPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
