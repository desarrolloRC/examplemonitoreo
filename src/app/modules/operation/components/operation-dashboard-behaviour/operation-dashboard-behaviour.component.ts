import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as shape from 'd3-shape';
import {OperationDashboardService} from '../../services/operation-dashboard.service';
import {BasicType, GlobalSearchParams, RiskData, SearchGlobalParams} from '../../../shared/interfaces/SharedInterface';
import {StorageService} from '../../../shared/services/storage.service';
import {CollectService} from '../../services/collect.service';
import {DriverService} from '../../../shared/services/driver.service';
import {FormControl} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {TranslateService} from '@ngx-translate/core';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';

@Component({
  selector: 'app-operation-dashboard-behaviour',
  templateUrl: './operation-dashboard-behaviour.component.html',
  styleUrls: ['./operation-dashboard-behaviour.component.css']
})
export class OperationDashboardBehaviourComponent implements OnInit {

  constructor(
    private operationDashboard: OperationDashboardService,
    private storageService: StorageService,
    private _CollectService: CollectService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private _DriverService: DriverService) {


  }
  showChart = false;
  public type = 'month';
  @Input() titleShown = true;
  @Input() titleText = 'recaudo';
  @Input() showBarBottom = true;


  @Output()
  changeToIndexTab = new EventEmitter<string>();

  public installmentGenerated = 0;
  public installmentVoided = 0;
  public installmentPayed = 0;
  public installmentPartials = 0;
  public installmentExtraPayment = 0;
  public targetAmount = 0;
  public collectAmount = 0;
  public collectPercentage = 0;
  public extraPaymentCount = 0;
  public expectedAmount = 0;
  public expectedAmountType = 0;
  public selectedTypeInstallment = 'all';
  public endDate;

  backupData: any[];
  view: any[];
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';
  timeline = true;
  curve: any = shape.curveCardinal;

  selectedOptions = ['Todos'];
  optionsDisplayList: string[] = ['Todos', 'Esperado', 'Recaudado', 'Abono'];
  showAll = true;
  showCollect = false;
  showExpected = false;
  showExtraCollect = false;

  colorScheme = {
    domain: ['#b1c9fc', '#e1e9fd', '#EB5757']
  };
  multi: any[] = [];
  multiGoogle: any[] = [];

  public pieChart: GoogleChartInterface = {
    chartType: 'LineChart',
    dataTable: this.multiGoogle,
    /*formatters: [
      {
        columns: [1],
        type: 'DateFormat',
      }
    ],*/
    options: {'title': 'Tasks'},
  };
  public selectedVal: string;

  ngOnInit() {

    localStorage.setItem('typeChart', 'day');
    this.type = localStorage.getItem('typeChart');
    this.showChart = true;
    this.getData();

    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[2] === 'globalSearch'
      || parts[2] === 'globalSearchStartDate'
      || parts[2] === 'globalSearchEndDate'
      || parts[2] === 'chartScatterHarvest'
      ) {
        this.getData();
      } else if (parts[2] === 'typeInstallment') {
        this.selectedTypeInstallment = this.storageService.getItem('typeInstallment');
      }
    });
    this.storageService.setItem('typeInstallment', this.selectedTypeInstallment);
  }

  formatJustMonth(value) {
    value = value.replace('.000Z', '');
    let month = value.getMonth();
    month = month + 1;
    const year = value.getFullYear();
    let textMonth = '';
    switch (month) {
      case 1:
        textMonth = 'Enero';
        break;
      case 2:
        textMonth = 'Febrero';
        break;
      case 3:
        textMonth = 'Marzo';
        break;
      case 4:
        textMonth = 'Abril';
        break;
      case 5:
        textMonth = 'Mayo';
        break;
      case 6:
        textMonth = 'Junio';
        break;
      case 7:
        textMonth = 'Julio';
        break;
      case 8:
        textMonth = 'Agosto';
        break;
      case 9:
        textMonth = 'Septiembre';
        break;
      case 10:
        textMonth = 'Octubre';
        break;
      case 11:
        textMonth = 'Noviembre';
        break;
      case 12:
        textMonth = 'Diciembre';
        break;
    }

    return textMonth;
  }

  formatDateValue(value) {
    const day = value.getDate();
    let month = value.getMonth();
    month = month + 1;
    const year = value.getFullYear();

    let text = '';
    this.type = localStorage.getItem('typeChart');
    if (this.type === 'month') {
      switch (month) {
        case 1:
          text = 'Enero';
          break;
        case 2:
          text = 'Febrero';
          break;
        case 3:
          text = 'Marzo';
          break;
        case 4:
          text = 'Abril';
          break;
        case 5:
          text = 'Mayo';
          break;
        case 6:
          text = 'Junio';
          break;
        case 7:
          text = 'Julio';
          break;
        case 8:
          text = 'Agosto';
          break;
        case 9:
          text = 'Septiembre';
          break;
        case 10:
          text = 'Octubre';
          break;
        case 11:
          text = 'Noviembre';
          break;
        case 12:
          text = 'Diciembre';
          break;
      }
      text = year + ' - ' + text;
    } else if (this.type === 'day') {
      text = day + '/' + month + '/' + year;
    } else if (this.type === 'year') {
      text = year;
    } else if (this.type === 'week') {
      value = new Date(Date.UTC(value.getFullYear(), value.getMonth(), value.getDate()));
      value.setUTCDate(value.getUTCDate() + 4 - (value.getUTCDay() || 7));
      const yearStart = +new Date(Date.UTC(value.getUTCFullYear(), 0, 1));
      const valueCalc = value - yearStart;
      const weekNo = (Math.ceil(( ( valueCalc / 86400000) + 1) / 7));
      text = value.getFullYear() + ' - W' + weekNo.toString();
    }
    return text;
  }

  changeDistribution(type) {
    this.type = type;
  }

  formatJustMonthDate(value) {
    let month = value.getMonth();
    month = month + 1;
    const year = value.getFullYear();

    return year + '-' + month + '-1';
  }

  formatJustDayDate(value) {
    const day = value.getDate();
    let month = value.getMonth();
    month = month + 1;
    const year = value.getFullYear();

    return year + '-' + month + '-' + day;
  }

  formatJustYearDate(value) {
    const year = value.getFullYear();

    return year + '-1-1';
  }

  onValChange(val: string) {
    localStorage.setItem('typeChart', val);
    this.type = localStorage.getItem('typeChart');
    this.createArrayGroupFromDb(this.backupData);
  }

  formatJustYear(value) {
    value = value.replace('.000Z', '');
    const year = value.getFullYear();
    return year;
  }

  getData() {
    this.spinner.show();
    const startDate = this.storageService.getItem('globalSearchStartDate');
    const endDate = this.storageService.getItem('globalSearchEndDate');
    const search = this.storageService.getItem('globalSearch');

    const params: SearchGlobalParams = {
      startDate: startDate,
      endDate: endDate,
      search: search,
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };
    this.operationDashboard.getDataCollect(params).subscribe((response: any) => {
      this._DriverService.getExpectedAmount(params).subscribe((responseCollect: any) => {
        this.expectedAmount = responseCollect[0]['amount'];
        this.backupData = response;
        this.createArrayGroupFromDb(response);
        this.spinner.hide();
      });
    });
    this.endDate = new Date(endDate);
    this.getInstallemntsData();
  }


  dateToObjectDate(date) {
    date = date.replace('.000Z', '');
    const dateSplit = date.split('-');
    const day = dateSplit[2];
    let month = dateSplit[1];
    month = month - 1;
    const year = dateSplit[0];

    return new Date(year, month, day);
  }

  weekNumber(d) {
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    const yearStart = +new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    const weekNo = (Math.ceil(( ( (d - yearStart) / 86400000) + 1) / 7));
    return weekNo.toString();
  }

  getDateOfWeek(week, y) {
    const d = new Date('Jan 01, ' + y + ' 01:00:00');
    const dayMs = (24 * 60 * 60 * 1000);
    const offSetTimeStart = dayMs * (d.getDay() - 1);
    const w = d.getTime() + 604800000 * (week - 1) - offSetTimeStart; // reducing the offset here

    const date = new Date(w);
    let month = date.getMonth();
    month = month + 1;
    const year = date.getFullYear();
    const day = date.getDate();
    return year + '-' + month + '-' + day;
  }

  createArrayGroup(data) {
    const array: any = {};
    const arrayExtra: any = {};
    const arrayExpected: any = {};
    const dataInput = data[0].series;

    dataInput.forEach((input) => {
      input['provider_date'] = input['provider_date'].replace('.000Z', '');
      let index = '';
      if (this.type === 'month') {
        this.expectedAmountType = this.expectedAmount;
        index = this.formatJustMonthDate(input.name);
      } else if (this.type === 'year') {
        this.expectedAmountType = (this.expectedAmount * 12);
        index = this.formatJustYearDate(input.name);
      } else if (this.type === 'day') {
        this.expectedAmountType = (this.expectedAmount / 10);
        index = this.formatJustDayDate(input.name);
      } else if (this.type === 'week') {
        this.expectedAmountType = (this.expectedAmount / 4);
        index = this.getDateOfWeek(this.weekNumber(input.name) , input.name.getFullYear());
      }
      const amount = input.value;
      const amountTotal = input.total;
      const installmentDate = new Date(input['provider_date']);

      if (parseInt(amount, 0) > 0 && this.endDate < installmentDate) {
        if (typeof arrayExtra[index]  === 'undefined') {
          arrayExtra[index] = parseFloat(amount);
        } else {
          arrayExtra[index] = arrayExtra[index] + parseFloat(amount);
        }
      } else {
        if (typeof array[index]  === 'undefined') {
          array[index] = parseFloat(amount);
          arrayExpected[index] = parseFloat(amountTotal);
        } else {
          array[index] = array[index] + parseFloat(amount);
          arrayExpected[index] = arrayExpected[index] + parseFloat(amountTotal);
        }
      }
    });

    this.multiGoogle = [];
    this.multiGoogle.push(['Fecha', 'Esperado', 'Recaudado']);
    const dataObjectArray: any[] = [
      {
        name: 'Esperado',
        series: [
        ]
      },
      {
        name: 'Recaudo',
        series: [
        ]
      },
      {
        name: 'Abono',
        series: [
        ]
      },
    ];

    for (const propertyName in array) {
      this.multiGoogle.push([this.dateToObjectDate(propertyName), arrayExpected[propertyName], array[propertyName]]);

      const serieObject: any = {
        name: this.dateToObjectDate(propertyName),
        value: array[propertyName]
      };
      const serieObjectExpected: any = {
        name: this.dateToObjectDate(propertyName),
        value: arrayExpected[propertyName]
      };
      if (this.showAll || this.showCollect) {
        dataObjectArray[1].series.push(serieObject);
      }
      if (this.showAll || this.showExpected) {
        dataObjectArray[0].series.push(serieObjectExpected);
      }

    }

    for (const propertyNameExtra in arrayExtra) {
      let serieObjectExtra: any = {};

      serieObjectExtra = {
        name: this.dateToObjectDate(propertyNameExtra),
        value: arrayExtra[propertyNameExtra]
      };
      if (this.showAll || this.showExtraCollect) {
        dataObjectArray[2].series.push(serieObjectExtra);
      }
    }
    this.multiGoogle = this.multiGoogle.sort(this.Comparator);
    this.pieChart.dataTable = this.multiGoogle;
    this.multi = dataObjectArray;
  }

  createArrayGroupFromDb(data) {
    const array: any = {};
    const arrayExtra: any = {};
    const arrayExpected: any = {};
    const dataInput = data;
    const dataArrayGoogle = [];
    dataInput.forEach((input, indexMap) => {
      dataInput[indexMap]['provider_date'] = dataInput[indexMap]['provider_date'].replace('.000Z', '');
      input['provider_date'] = input['provider_date'].replace('.000Z', '');
      let index = '';
      const date = new Date(input['provider_date'].replace('.000Z', ''));
      const amount = input['sum'];
      const amountTotal = input['total'];
      if (this.type === 'month') {
        this.expectedAmountType = this.expectedAmount;
        index = this.formatJustMonthDate(date);
      } else if (this.type === 'year') {
        this.expectedAmountType = (this.expectedAmount * 12);
        index = this.formatJustYearDate(date);
      } else if (this.type === 'day') {
        this.expectedAmountType = (this.expectedAmount / 10);
        index = this.formatJustDayDate(date);
      } else if (this.type === 'week') {
        this.expectedAmountType = (this.expectedAmount / 4);
        index = this.getDateOfWeek(this.weekNumber(date) , date.getFullYear());
      }

      const installmentDate = new Date(input['provider_date']);

      if (parseInt(amount, 0) > 0 && this.endDate < installmentDate) {
        if (typeof arrayExtra[index]  === 'undefined') {
          arrayExtra[index] = parseFloat(amount);
        } else {
          arrayExtra[index] = arrayExtra[index] + parseFloat(amount);
        }
      } else {
        if (typeof array[index]  === 'undefined') {
          array[index] = parseFloat(amount);
          arrayExpected[index] = parseFloat(amountTotal);
        } else {
          array[index] = array[index] + parseFloat(amount);
          arrayExpected[index] = arrayExpected[index] + parseFloat(amountTotal);
        }
      }

    });

    const dataObjectArray: any[] = [
      {
        name: 'Esperado',
        series: [
        ]
      },
      {
        name: 'Recaudo',
        series: [
        ]
      },
      {
        name: 'Abono',
        series: [
        ]
      },
    ];

    const dataset = [{
        'data': []}];

    const labels = [];
    this.multiGoogle = [];
    this.multiGoogle.push(['Fecha', 'Esperado', 'Recaudado']);
    for (const propertyName in array) {
      this.multiGoogle.push([this.dateToObjectDate(propertyName), arrayExpected[propertyName], array[propertyName]]);
      let serieObject: any = {};
      let serieObjectExpected: any = {};

      serieObject = {
        name: this.dateToObjectDate(propertyName),
        value: array[propertyName]
      };
      serieObjectExpected = {
        name: this.dateToObjectDate(propertyName),
        value: arrayExpected[propertyName]
      };
      if (this.showAll || this.showCollect) {
        dataObjectArray[1].series.push(serieObject);
      }
      if (this.showAll || this.showExpected) {
        dataObjectArray[0].series.push(serieObjectExpected);
      }
      let ccComponent = this.pieChart.component;
      if (typeof ccComponent !== 'undefined') {
        ccComponent.draw();
      }
    }

    for (const propertyNameExtra in arrayExtra) {
      let serieObjectExtra: any = {};

      serieObjectExtra = {
        name: this.dateToObjectDate(propertyNameExtra),
        value: arrayExtra[propertyNameExtra]
      };

      if (this.showAll || this.showExtraCollect) {
        dataObjectArray[2].series.push(serieObjectExtra);
      }

    }

    this.multiGoogle = this.multiGoogle.sort(this.Comparator);
    this.pieChart.dataTable = this.multiGoogle;
    this.multi = dataObjectArray;
  }

  Comparator(a, b) {
    if (a[0] < b[0]) return -1;
    if (a[0] > b[0]) return 1;
    return 0;
  }

  getInstallemntsData() {
    const startDate = this.storageService.getItem('globalSearchStartDate');
    const endDate = this.storageService.getItem('globalSearchEndDate');
    const search = this.storageService.getItem('globalSearch');

    const params: SearchGlobalParams = {
      startDate: startDate,
      endDate: endDate,
      search: search,
      harvest: this.storageService.getItem('chartScatterHarvest'),
    };
    this._CollectService.getInstallmentDataTable().subscribe((response: any) => {
      this.targetAmount = 0;
      this.collectAmount = 0;
      this.extraPaymentCount = 0;

      this.installmentGenerated = 0;
      this.installmentVoided = 0;
      this.installmentPayed = 0;
      this.installmentPartials = 0;
      this.installmentExtraPayment = 0;

      response.map((value) => {
        const payed = value['Pagado'] === null ? 0 : parseFloat(value['Pagado']);
        this.targetAmount += parseFloat(value['Monto']);
        this.collectAmount += payed;

        this.installmentGenerated ++;

        if (value['Estado'] === 'PAGADA') {
          this.installmentPayed ++;
        } else if (value['Estado'] === 'VENCIDA') {
          this.installmentVoided ++;
        } else if (value['Estado'] === 'ACTUAL' || value['Estado'] === 'PENDIENTE') {
          this.installmentPartials ++;
        }
      });

      this.targetAmount = Math.round(this.targetAmount);
      this.collectAmount = Math.round(this.collectAmount);

      this.collectPercentage = Math.ceil((this.collectAmount * 100) / this.targetAmount);
      this.collectPercentage = isNaN(this.collectPercentage) ? 0 : this.collectPercentage;
    });
  }

  changeOptions(event) {
    const indexTodos = this.selectedOptions.indexOf('Todos');
    if (this.selectedOptions.length > 1 || indexTodos === -1) {
      this.showAll = false;
      if (indexTodos > -1) {
        this.selectedOptions.splice(indexTodos, 1);
      }
      if (this.selectedOptions.indexOf('Recaudado') !== -1) {
        this.showCollect = true;
      } else {
        this.showCollect = false;
      }

      if (this.selectedOptions.indexOf('Esperado') !== -1) {
        this.showExpected = true;
      } else {
        this.showExpected = false;
      }

      if (this.selectedOptions.indexOf('Abono') !== -1) {
        this.showExtraCollect = true;
      } else {
        this.showExtraCollect = false;
      }
    } else {
      if (this.selectedOptions.indexOf('Todos') !== -1) {
        this.showAll = true;
        this.showCollect = false;
        this.showExpected = false;
        this.showExtraCollect = false;
        this.selectedOptions = ['Todos'];
      } else {
        this.showAll = false;
      }
    }

    this.createArrayGroupFromDb(this.backupData);
  }

  selectTypeInstallment(type) {
    this.changeTab();
    this.selectedTypeInstallment = type;
    this.storageService.setItem('typeInstallment', type);
  }

  changeTab() {
    this.changeToIndexTab.emit('1');
  }

}
