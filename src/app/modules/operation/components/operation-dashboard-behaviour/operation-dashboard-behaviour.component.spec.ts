import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationDashboardBehaviourComponent } from './operation-dashboard-behaviour.component';

describe('OperationDashboardBehaviourComponent', () => {
  let component: OperationDashboardBehaviourComponent;
  let fixture: ComponentFixture<OperationDashboardBehaviourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationDashboardBehaviourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationDashboardBehaviourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
