import { Component, OnInit } from '@angular/core';

import {environment} from '../../../../../environments/environment';
import {DriverCommonStructure} from '../../definitions/DriverCommonStructure';
import {OperationNavStructure} from '../../definitions/OperationNavStructure';

@Component({
  selector: 'app-driver-score',
  templateUrl: './driver-score.component.html',
  styleUrls: ['./driver-score.component.css']
})
export class DriverScoreComponent implements OnInit {
  public navLinks: object[];
  public attributeLabels: object[];
  public scoreMicroService = environment.urlScore;

  constructor() { }

  ngOnInit() {
    this.attributeLabels = DriverCommonStructure.scoreFullAttributeLabels;
    this.navLinks = OperationNavStructure.OperationNavStructure;
  }

}
