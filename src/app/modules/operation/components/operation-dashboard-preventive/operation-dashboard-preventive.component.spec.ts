import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationDashboardPreventiveComponent } from './operation-dashboard-preventive.component';

describe('OperationDashboardPreventiveComponent', () => {
  let component: OperationDashboardPreventiveComponent;
  let fixture: ComponentFixture<OperationDashboardPreventiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationDashboardPreventiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationDashboardPreventiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
