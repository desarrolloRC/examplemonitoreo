import { Component, OnInit } from '@angular/core';
import * as shape from 'd3-shape';
import {OperationDashboardService} from '../../services/operation-dashboard.service';
import {GetById, ParamsAndId} from '../../../shared/interfaces/SharedInterface';
import {StorageService} from '../../../shared/services/storage.service';

@Component({
  selector: 'app-operation-dashboard-preventive',
  templateUrl: './operation-dashboard-preventive.component.html',
  styleUrls: ['./operation-dashboard-preventive.component.css']
})
export class OperationDashboardPreventiveComponent implements OnInit {
  constructor(private operationDashboard: OperationDashboardService, private storageService: StorageService) {

  }
  public preventives: any[];
  showChart = false;
  public type = 'month';
  public preventiveType;

  view: any[];
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';
  timeline = true;
  curve: any = shape.curveCardinal;

  colorScheme = {
    domain: ['#eb5757', '#f2994a', '#1875f0', '#11cb87']
  };

  multi_vertical: any[];

  ngOnInit() {
    this.operationDashboard.getPreventivesOptions().subscribe((response: any) => {
      this.preventives = [];
      if (response.length > 0) {
        response.map((value) => {
          const object: object = {
            value: value['Job_id'],
            viewValue: value['Job_label']
          };
          this.preventives.push(object);
        });
      }
    });
    this.storageService.watchStorage().subscribe((data: string) => {
      const parts = data.split('|');
      if (parts[2] === 'globalSearch' 
      || parts[2] === 'globalSearchStartDate' 
      || parts[2] === 'globalSearchEndDate'
      || parts[2] === 'chartScatterHarvest'
      ) {
        this.getData();
      }
    });
    this.preventiveType = 1;

    this.showChart = true;

    this.getData();
  }
    changeType(value) {
      this.preventiveType = value;
      this.getData();
    }

    getData() {
      const startDate = this.storageService.getItem('globalSearchStartDate');
      const endDate = this.storageService.getItem('globalSearchEndDate');
      const search = this.storageService.getItem('globalSearch');

      const no_info = 0;
      const on_day = 0;
      const no_go = 0;

      const params: ParamsAndId = {
        id: parseInt(this.preventiveType),
        startDate: startDate,
        endDate: endDate,
        search: search,
        harvest: this.storageService.getItem('chartScatterHarvest'),
        
      };
      this.operationDashboard.getPreventivesByType(params).subscribe((response: any) => {
        const dataObjectArray: any[] = [
          {
            name: 'Recaudo',
            series: [
            ]
          }
        ];
        if (response.length > 0) {

          response.map((value) => {
            dataObjectArray[0].name = value['label'];
            const serieObject: any = {
              name: value['type'],
              value: value['count']
            };
            dataObjectArray[0].series.push(serieObject);
          });
          this.multi_vertical = dataObjectArray;
        } else {
          dataObjectArray[0].name = 'Sin Info';
          const serieObject: any = {
            name: 'Sin Info',
            value: 0
          };
          dataObjectArray[0].series.push(serieObject);
          this.multi_vertical = dataObjectArray;
        }
      });
    }
}
