import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverKilometersComponent } from './driver-kilometers.component';

describe('DriverKilometersComponent', () => {
  let component: DriverKilometersComponent;
  let fixture: ComponentFixture<DriverKilometersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverKilometersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverKilometersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
