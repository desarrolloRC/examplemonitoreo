import { Component, OnInit } from '@angular/core';

import {environment} from '../../../../../environments/environment';
import {DriverCommonStructure} from '../../definitions/DriverCommonStructure';
import {OperationNavStructure} from '../../definitions/OperationNavStructure';

@Component({
  selector: 'app-driver-kilometers',
  templateUrl: './driver-kilometers.component.html',
  styleUrls: ['./driver-kilometers.component.css']
})
export class DriverKilometersComponent implements OnInit {
  public navLinks: object[];
  public attributeLabels: object[];
  public vehicleMicroService = environment.urlDriver;

  constructor() { }

  ngOnInit() {
    this.attributeLabels = DriverCommonStructure.travelAttributeLabels;
    this.navLinks = OperationNavStructure.OperationNavStructure;
  }

}
