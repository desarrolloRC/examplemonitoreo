import {Component, OnInit} from '@angular/core';

import {environment} from '../../../../../environments/environment';
import {StorageService} from '../../../shared/services/storage.service';
import {DriverCommonStructure} from '../../definitions/DriverCommonStructure';
import {OperationDashboardService} from '../../services/operation-dashboard.service';
import {ScoreService} from '../../../shared/services/score.service';

@Component({
  selector: 'app-driver-dashboard',
  templateUrl: './driver-dashboard.component.html',
  styleUrls: ['./driver-dashboard.component.css']
})
export class DriverDashboardComponent implements OnInit {

  public pageIndex;
  public speedAttributeLabels: object[];
  public travelAttributeLabels: object[];
  public scoreAttributeLabels: object[];
  public vehicleMicroService = environment.urlDriver;

  public scoreMicroService = environment.urlScore;

  public score = 0;
  public speed = 0;
  public kilometer = 0;

  constructor(
          private operationDashboardService: OperationDashboardService,
          private scoreService: ScoreService,
          private storageService: StorageService
  ) {
  }

  ngOnInit() {
    this.speedAttributeLabels = DriverCommonStructure.speedAttributeLabels;
    this.travelAttributeLabels = DriverCommonStructure.travelAttributeLabels;
    this.scoreAttributeLabels = DriverCommonStructure.scoreMiniAttributeLabels;

    this.getSpeed();
    this.getDistance();
    this.getScore();

    this.storageService.watchStorage().subscribe(() => {
      this.getSpeed();
      this.getDistance();
      this.getScore();
    });
  }

  public getSpeed() {
    this.operationDashboardService.getVehicleSpeedLimitAvg().subscribe((response) => {
      if (typeof response['data'] !== 'undefined') {
        this.speed = Math.round(response['data']['average'][0]['percent']);
      }
    });
  }

  public getDistance() {
    this.operationDashboardService.getVehicleDistanceAvg().subscribe((response) => {
      if (typeof response['data'] !== 'undefined') {
        this.kilometer = Math.round(response['data']['average'][0]['avg'] / 1000);
      }
    });
  }

  public getScore() {
    this.scoreService.getVehiclesInRiskAll().subscribe((response: any) => {
      if (typeof response !== 'undefined') {
        response.map((value, index) => {
          this.score += value['score_value'];
          if (response.length === (index + 1)) {
            this.score = this.score / response.length;
          }
        });
      }
    });
  }
}
