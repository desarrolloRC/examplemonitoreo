import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationDashboardMainComponent } from './operation-dashboard-main.component';

describe('OperationDashboardMainComponent', () => {
  let component: OperationDashboardMainComponent;
  let fixture: ComponentFixture<OperationDashboardMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationDashboardMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationDashboardMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
