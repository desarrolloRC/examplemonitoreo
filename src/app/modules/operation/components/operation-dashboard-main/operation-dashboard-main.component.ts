import {ActivatedRoute} from '@angular/router';
import {AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {DriverCommonStructure} from '../../definitions/DriverCommonStructure';
import {OperationNavStructure} from '../../definitions/OperationNavStructure';

@Component({
  selector: 'app-operation-dashboard-main',
  templateUrl: './operation-dashboard-main.component.html',
  styleUrls: ['./operation-dashboard-main.component.css']
})
export class OperationDashboardMainComponent implements OnInit, AfterViewInit {

  public navLinks: object[];
  public attributeLabels: object[];
  public scoreMicroService = environment.urlScore;

  constructor( private route: ActivatedRoute) { }

  @ViewChild('tabGroup') tabGroup;
  ngOnInit() {
    this.attributeLabels = DriverCommonStructure.scoreFullAttributeLabels;
    this.navLinks = OperationNavStructure.OperationNavStructure;
  }

  ngAfterViewInit() {
    this.route
      .data
      .subscribe((data) => {
        window.location.hash = data.id;
      });
  }

  changeToIndexTab(event) {
    this.tabGroup.selectedIndex = parseInt(event);
  }

}
