import {Observable} from 'rxjs';
import * as JwtDecode from 'jwt-decode';
import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';

import {JwtHelper} from '../helpers/JwtHelper';
import {LoginService} from '../modules/system/services/login.service';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private _location: Router,
    private translate: TranslateService,
    private _loginService: LoginService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const token = sessionStorage.getItem('token');
    if (token) {
      this.translate.setDefaultLang(localStorage.getItem('language'));
      this.translate.use(localStorage.getItem('language'));

      if (JwtHelper.verifyExpiration(token)) {
        return true;
      } else {
        const person_id = {person_id: JwtDecode(token)['person']};
        this._loginService.regenerateToken(person_id).subscribe((response: any) => {
            sessionStorage.setItem('token', response.token);

            return true;
        }, () => {
          this._location.navigate(['login']).then();

          return false;
        });
      }
    } else {
      this._location.navigate(['login']).then();
      return false;
    }
  }
}
