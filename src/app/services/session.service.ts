import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';
import {SessionInterface} from '../interfaces/SessionInterface';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private http: HttpClient) { }

  public saveSession(data: SessionInterface) {
    return this.http.post(environment.url + 'save-session', data);
  }
}
