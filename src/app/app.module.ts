import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatToolbarModule,
  MatIconModule,
  MatListModule,
} from '@angular/material';
import {AngularFireModule} from '@angular/fire';
import {GestureConfig} from '@angular/material';
import {LOCALE_ID, NgModule} from '@angular/core';
import {registerLocaleData} from '@angular/common';
import localeEs from '@angular/common/locales/es-CO';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatNativeDateModule, MatSnackBarModule} from '@angular/material';
import {Angulartics2RouterlessModule} from 'angulartics2/routerlessmodule';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';

import {AppComponent} from './app.component';
import {Interceptor} from './auth/Interceptor';
import {AppRoutingModule} from './app-routing.module';
import {environment} from '../environments/environment';
import {SharedModule} from './modules/shared/shared.module';
import {HeaderComponent} from './modules/shared/components/header/header.component';
import {PageOfflineComponent} from './components/page-offline/page-offline.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';

import {I18nHelper} from './helpers/I18nHelper';
import {StorageService} from './modules/shared/services/storage.service';

registerLocaleData(localeEs, 'es-CO');

@NgModule({

  declarations: [
    AppComponent,
    PageOfflineComponent,
    PageNotFoundComponent,
    HeaderComponent,
  ],
  imports: [
    FormsModule,
    SharedModule, // load material dissing components and common components
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatToolbarModule,
    FlexLayoutModule,
    AngularFontAwesomeModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatIconModule,
    MatListModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AngularFireModule.initializeApp(environment.firebase),
    Angulartics2RouterlessModule.forRoot({
      gst: {
        trackingIds: [environment.analyticId],
        anonymizeIp: false
      },
      pageTracking: {}
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient],
      },
      isolate: false,
    }),
    AngularFireDatabaseModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
    {provide: LOCALE_ID, useValue: 'es-CO'},
    {provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig}

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private translate: TranslateService, storage: StorageService) {
    I18nHelper.setUp(translate, storage);
  }
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
